<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
  protected $table = 'barang';
  protected $fillable = ['kategori_id','nama'];
  public $timestamps = true;

  public function kategori()
    {
        return $this->belongsTo('App\Kategori', 'kategori_id', 'id');
    }
}
