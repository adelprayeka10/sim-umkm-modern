<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/login';

    protected function authenticated(Request $request, $user)
    {
        if(Auth::user()){
          if($user->level=='Admin'){
            return redirect('dashboard');
            }elseif ($user->level=='Penjual') {
                $user->status_id='1';
                $user->save();
                return redirect('dashboard_konven');
            }elseif ($user->level=='Kasir') {
                return redirect('dashboard_konven_kasir');
            }elseif ($user->level=='Pembeli') {
                Auth::logout();
                return redirect('/login');
            }
        }else{
          return redirect('/login');
        }
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
