<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bank;
Use DB;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
        //  $this->middleware('auth');
     }
       /**
        * Display a listing of the resource.
        *
        * @return \Illuminate\Http\Response
        */
       public function index()
       {
           $list = Bank::all()->sortBy('nama');
           return view('bank.list', compact('list'));
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return \Illuminate\Http\Response
        */
       public function create()
       {
           return view('bank.create');
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param  \Illuminate\Http\Request  $request
        * @return \Illuminate\Http\Response
        */
       public function store(Request $request)
       {
           $this->validate(request(),
             [
               'nama' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)|max:30',
               'deskripsi' => 'required',
             ],
             [
               'nama.required' => 'Bank harus diisi!',
               'nama.regex' => 'Bank tidak boleh menggunakan angka!',
               'nama.max' => 'Nama bank terlalu panjang!',
               'deskripsi.required' => 'Deskripsi harus diisi!',
             ]
           );

           Bank::create([
             'nama'=>request('nama'),
             'deskripsi'=>request('deskripsi')
           ]);

         return redirect('master/bank')->with('success','Data berhasil ditambahkan!');
       }

       /**
        * Display the specified resource.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
       public function show($id)
       {
           //
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
       public function edit($id)
       {
         $data = Bank::findOrFail($id);
         return view('bank.edit',compact('data'));
       }

       /**
        * Update the specified resource in storage.
        *
        * @param  \Illuminate\Http\Request  $request
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
       public function update(Request $request, $id)
       {
         $data = Bank::find($id);
         $data->nama=$request->get('nama');
         $data->deskripsi=$request->get('deskripsi');

         $this->validate(request(),
         [
           'nama' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)|max:30',
           'deskripsi' => 'required',
         ],
         [
           'nama.required' => 'Bank harus diisi!',
           'nama.regex' => 'Bank tidak boleh menggunakan angka!',
           'nama.max' => 'Nama bank terlalu panjang!',
           'deskripsi.required' => 'Deskripsi harus diisi!',
         ]
         );

         $data->save();

         return redirect('master/bank')->with('success', 'Data berhasil diperbarui!');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
       public function destroy($id)
       {
         $cek2 = DB::table('pencairan')
              ->join('bank', 'pencairan.bank_id', '=', 'bank.id')
              ->where('bank.id',$id)
              ->get();

        // remove id from KATEGORI
        if($cek2->isNotEmpty()){
              return redirect()->back()->with('error', 'Bank yang dipilih masih memiliki data di pencairan.');
        }else{
           $data = Bank::where('id',$id)->delete();
           return redirect('master/bank')->with('Success', 'Data berhasil dihapus!');
           // return response()->json($data);
         }
       }
}
