<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DetailTransaksiKonven;
use App\TransaksiKonven;
use App\BarangKonven;
use App\Pembeli;
use App\User;
use App\PenjualKonven;
use App\JenisPembayaran;
use Cart;
use DB;
use Auth;
use PDF;
use Response;

class DetailTransaksiKonvenKasirController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $produk=BarangKonven::get()->sortBy('name');
        $jenis_pembayaran = JenisPembayaran::all();
        $penjual = DB::table('users')
                ->join('penjual_konven', 'users.id', '=', 'penjual_konven.user_id')
                ->where('level','Penjual')->select('penjual_konven.id as id_penjual','users.name')->get()->sortBy('name');
        $requested = $request;
        return view('kasir_utama.index',compact('jenis_pembayaran','produk','penjual','requested'));
    }

    public function pilih_toko()
    {

        $penjual = User::where('level','Penjual')->get()->sortBy('name');
        return view('kasir_utama.index2',compact('penjual'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $path = $request->get('jenis_pembayaran');
        $path2 = $request->get('id_pembeli');
        // $barang = BarangKonven::select('stok')->where('id', $request->id_produk)->get();


        if($path == '1' && $path2 != null ){

        $transaksi['penjual_konven_id']='28';
        $transaksi['pembeli_id']=$request->id_pembeli;
        $transaksi['jenis_pembayaran_id']=$request->jenis_pembayaran;
        $transaksi['status']='Lunas';
        $transaksi['diskon']=$request->diskon;
        $transaksi['total']=$request->total;
        $transaksi['created_at']=date('Y-m-d H:i:s');
        $transaksi['updated_at']=date('Y-m-d H:i:s');
        $getId=TransaksiKonven::insertGetId($transaksi);

        // $a = $request->id_produk;

        foreach ($request->id_produk as $key => $value) {

            $transaksi_konven_id=$getId;
            $harga=$request->harga[$key];
            $potongan=$request->potongan[$key];
            $jumlah=$request->jumlah[$key];
            $subtotal=$request->subtotal[$key];
            DetailTransaksiKonven::Create(['transaksi_konven_id'=>$transaksi_konven_id,'barang_konven_id'=>$value,'harga'=>$harga, 'diskon'=>$potongan,'jumlah'=>$jumlah,'total'=>$subtotal]);
            // BarangKonven::where('id', $request->id_produk[$key])->decrement('stok', $jumlah);
            }   
            
        // return redirect('transaksi_konven')->with('success','Transaksi berhasil.');
        return redirect('invoice_kasir/'.$getId)->with('success','Transaksi berhasil disimpan.');
        }

        elseif($path == '1' && $path2 == null ){

        $data = new Pembeli;
        $data->user_id='14';
        $data->nama=$request->nama_pembeli;
        $data->member='Tidak';
        $data->save();

        $path3 = $request->get('nama_pembeli');

        $transaksi['penjual_konven_id']=$request->id_penjual;
        $transaksi['pembeli_id']=$data->id;
        $transaksi['jenis_pembayaran_id']=$request->jenis_pembayaran;
        $transaksi['status']='Lunas';
        $transaksi['diskon']=$request->diskon;
        $transaksi['total']=$request->total;
        $transaksi['created_at']=date('Y-m-d H:i:s');
        $transaksi['updated_at']=date('Y-m-d H:i:s');
        $getId=TransaksiKonven::insertGetId($transaksi);

        // $a = $request->id_produk;

        foreach ($request->id_produk as $key => $value) {

            $transaksi_konven_id=$getId;
            $harga=$request->harga[$key];
            $potongan=$request->potongan[$key];
            $jumlah=$request->jumlah[$key];
            $subtotal=$request->subtotal[$key];
            DetailTransaksiKonven::Create(['transaksi_konven_id'=>$transaksi_konven_id,'barang_konven_id'=>$value,'harga'=>$harga, 'diskon'=>$potongan,'jumlah'=>$jumlah,'total'=>$subtotal]);
            // BarangKonven::where('id', $request->id_produk[$key])->decrement('stok', $jumlah);
            }     
        
        return redirect('invoice_kasir/'.$getId)->with('success','Transaksi berhasil disimpan.');
        }

        elseif($path == '2' && $path2 != null ){

          if($request->saldo_pembeli < $request->total){

            return redirect()->back()->with('error','Saldo pembeli tidak cukup. Lakukan transaksi dengan tunai!');
          }

          else{

            $transaksi['penjual_konven_id']='28';
            $transaksi['pembeli_id']=$request->id_pembeli;
            $transaksi['jenis_pembayaran_id']=$request->jenis_pembayaran;
            $transaksi['status']='Menunggu';
            $transaksi['diskon']=$request->diskon;
            $transaksi['total']=$request->total;
            $transaksi['created_at']=date('Y-m-d H:i:s');
            $transaksi['updated_at']=date('Y-m-d H:i:s');
            $getId=TransaksiKonven::insertGetId($transaksi);

            // $a = $request->id_produk;

            foreach ($request->id_produk as $key => $value) {

                $transaksi_konven_id=$getId;
                $harga=$request->harga[$key];
                $potongan=$request->potongan[$key];
                $jumlah=$request->jumlah[$key];
                $subtotal=$request->subtotal[$key];
                DetailTransaksiKonven::Create(['transaksi_konven_id'=>$transaksi_konven_id,'barang_konven_id'=>$value,'harga'=>$harga, 'diskon'=>$potongan,'jumlah'=>$jumlah,'total'=>$subtotal]);
                // BarangKonven::where('id', $request->id_produk[$key])->decrement('stok', $jumlah);
                }   

            $total = $request->get('total');
            $id_pembeli = $request->get('id_pembeli');
            
            return view('kasir_utama.gamapay', compact('total','path4','getId'));

          }
        
        }

        else{
        
        return redirect('transaksi_konven')->with('error','Transaksi Gagal. Masukan data dengan benar!');
        }


    }

    //print nota
    public function getPdf($id)
    {
        date_default_timezone_set('Asia/Jakarta');       

        $value = TransaksiKonven::with('pembeli','penjual_konven')->find($id);
        $data = DetailTransaksiKonven::with('transaksiKonven','barang_konven')->where('transaksi_konven_id',$id)->get()->toArray();

        return view('kasir_utama.nota',compact('value','data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('kasir_utama.list');
    }

    function fetch(Request $request)
    {
     if($request->get('query'))
     {
      $query = $request->get('query');
      $data = DB::table('pembeli')
        ->where('nama', 'LIKE', "%{$query}%")
        ->get();

        if($data->isEmpty()){
          $output = '<ul class="dropdown-menu col-md-12" style="display:block; position:relative"><li><a href="#">'.'Tidak ditemukan'.'</a></li></ul>';
          echo $output;
        }

        else{
          $output = '<ul class="dropdown-menu col-md-12" style="display:block; position:relative">';
          foreach($data as $row)
          {
           $output .= '
           <li><a href="#">'.$row->nama.'</a></li>
           ';
          }
          $output .= '</ul>';
          echo $output;

        }
     }
    }

    public function getData(Request $request, $id_produk)
    {
        $user = DB::table('users')
                ->join('penjual_konven', 'users.id', '=', 'penjual_konven.user_id')
                ->where('users.id',$request->penjual)
                ->pluck('penjual_konven.id')->first();
        $tampil = BarangKonven::where('penjual_konven_id','=',$user)->find($id_produk);
        return response()->json($tampil);
    }


    public function getDataProduk(Request $request)
    {
        $user = DB::table('users')
                ->join('penjual_konven', 'users.id', '=', 'penjual_konven.user_id')
                ->where('users.id',$request->penjual)
                ->pluck('penjual_konven.id')->first();
        $tampil = BarangKonven::where('nama',$request->nama)->first();
        return response()->json($tampil);
    }

    public function getDataPembeli($id_pembeli)
    {
      $tampil = DB::table('users')
                ->join('pembeli', 'users.id', '=', 'pembeli.user_id')
                ->where('pembeli.id',$id_pembeli)
                ->select('users.saldo', 'pembeli.member','pembeli.id','pembeli.nama')
                ->get()->first();
        // $tampil = Pembeli::find($id_pembeli);
        return response()->json($tampil);
    }

    public function getNamaPembeli(Request $request)
    {

      $tampil = DB::table('users')
                ->join('pembeli', 'users.id', '=', 'pembeli.user_id')
                ->where('pembeli.nama',$request->nama_pembeli)
                ->select('users.saldo', 'pembeli.member','pembeli.id','pembeli.nama')
                ->get()->first();
        // $tampil = Pembeli::where('nama',$request->nama_pembeli)->first();
        return response()->json($tampil);
    }

    public function autoComplete(Request $request) {

        $query = $request->get('term','');

        $products=BarangKonven::where('nama','LIKE','%'.$query.'%')->get();
        
        $data=array();
        foreach ($products as $product) {
            $data[]=array('value'=>$product->nama,'id'=>$product->id);
        }
        if(count($data))
            return $data;
        else
            return ['value'=>'Hasil tidak ditemukan','id'=>''];
    }
    

    public function generateQrcode(Request $request)
    {
        $total = $request->get('total_belanja');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
