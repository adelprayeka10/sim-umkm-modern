<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

class PembeliController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = User::where('level','Pembeli')->get();
        return view('pembeli.list', compact('list'));
    }

    public function index2()
    {
        $list = DB::table('users')
                ->join('pembeli', 'users.id', '=', 'pembeli.user_id')
                ->where('users.level','Pembeli')
                ->where('pembeli.member','Ya')
                ->select('users.saldo', 'users.email','users.no_telepon', 'pembeli.member', 'pembeli.nama', 'pembeli.id')
                ->get();
                
        return view('pembeli.list_pos', compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pembeli.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      User::create([
            'name' => request('username'),
            'level' => 'Pembeli',
            'email' => request('email'),
            'no_telepon' => request('no_hp'),
            'password' => request('password'),
            'tanggal_lahir' => request('tanggal_lahir')
      ]);
      return redirect('pembeli')->with('success','pembeli berhasil disimpan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::find($id);
        return view('pembeli.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data= User::find($id);
      $data->username=$request->username;
      $data->email=$request->email;
      $data->no_telephone=$request->no_hp;
      $data->password=$request->password;
      $data->tanggal_lahir=$request->tanggal_lahir;
      $data->save();
      return redirect('pembeli');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $data = User::find($id)->delete();
      return redirect('pembeli');
    }
}
