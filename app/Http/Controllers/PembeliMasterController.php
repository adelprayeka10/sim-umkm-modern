<?php

namespace App\Http\Controllers;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use File;
use Illuminate\Http\Request;
use App\User;
use App\PembeliMaster;
use App\Pembeli;
use Carbon\Carbon;
Use Alert;
Use Auth;
use DB;
Use Excel;

class PembeliMasterController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = User::leftJoin('pembeli', 'users.id', '=', 'pembeli.user_id')
                ->select('pembeli.id','pembeli.user_id', 'users.name', 'users.email','users.no_telepon','users.status_id')
                ->where('users.level','Pembeli')->where('users.name','!=','Pembeli')
                ->get();

        return view('pembeli-master.list', compact('list'));
    }

    public function index2()
    {

      $list = Pembeli::select('id', 'nama')->where('member','=','Tidak')->get();
        // $list = User::leftJoin('pembeli', 'users.id', '=', 'pembeli.user_id')->select('pembeli.id', 'users.name', 'users.email','users.no_telepon','users.status_id')->where('users.level','Pembeli')->where('users.name','!=','Pembeli')->where('pembeli.member','=','Ya')->get();
        return view('pembeli-master.list_bukan_member', compact('list'));
    }
    public function aktif()
    {
        $list = Pembeli::leftJoin('users', 'pembeli.user_id', '=', 'users.id')
                        ->select('pembeli.id', 'pembeli.user_id', 'users.name', 'users.email','users.no_telepon','users.status_id')
                        ->where('status_id', 1)
                        ->get();
        return view('pembeli-master.list', compact('list'));
    }
    public function nonAktif()
    {
        $list = Pembeli::leftJoin('users', 'pembeli.user_id', '=', 'users.id')
                        ->select('pembeli.id', 'pembeli.user_id', 'users.name', 'users.email','users.no_telepon','users.status_id')
                        ->where('status_id', 2)
                        ->get();
        return view('pembeli-master.list', compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pembeli-master.create');
    }


    public function create2($id)
    {
        $data = Pembeli::where('id', $id)->first();
        $nama = Pembeli::find($id)->pluck('nama')->first();
        // $status = Status::all();
        return view('pembeli-master.create_nonmember', compact('data','nama'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

  public function store(Request $request)
  {
    date_default_timezone_set('Asia/Jakarta');
    $this->validate(request(),
      [
        'name' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)|unique:users|max:30',
        'password' => 'required|min:8',
        'email' => 'required|email|unique:users',
        'no_telepon' => 'required|unique:users|max:15',
        'tanggal_lahir' => 'required',
        'saldo' => 'required',
      ],
      [
        'name.required' => 'Nama harus diisi!',
        'name.regex' => 'Nama tidak boleh menggunakan angka!',
        'name.unique' => 'Nama sudah pernah terdaftar, coba dengan nama lain!',
        'name.max' => 'Nama terlalu panjang, maksimal 30 karakter!',
        'password.required' => 'Password harus diisi!',
        'password.min' => ' Password minimal 8 karetker!',
        'email.required' => 'Email harus diisi!',
        'email.email' => 'Format email salah!',
        'email.unique' => 'Email sudah pernah terdaftar!',
        'no_telepon.required' => 'Nomer telepon harus diisi!',
        'no_telepon.unique' => 'Nomer telepon sudah terdaftar!',
        'no_telepon.max' => 'Nomer telepon terlalu panjang!',
        'tanggal_lahir.required' => 'Tanggal lahir harus diisi!',
        'saldo.required' => 'Saldo harus diisi!'
      ]);

      if(!empty($request->foto)){
         $file = $request->file('foto');
         $extension = strtolower($file->getClientOriginalExtension());
         $filename = $request->name . '.' . $extension;
         Storage::put('images/' . $filename, File::get($file));
         $file_server = Storage::get('images/' . $filename);
         $img = Image::make($file_server)->resize(141, 141);
         $img->save(base_path('public/images/' . $filename));
       }

      $user = User::create([
        'level'=>'Pembeli',
        'name'=>$request->name,
        'password'=>bcrypt($request->password),
        'email'=>$request->email,
        'no_telepon'=>$request->no_telepon,
        'tanggal_lahir'=>$request->tanggal_lahir = Carbon::parse($request->tanggal_lahir),
        'status_id'=>2,
        'saldo'=>$request->saldo,
      ])
      ->pembeli()->create([
        'nama'=>$request->name,

      ]);
      if (!empty($request->foto)) {
        $user->user->foto=$filename;
        // dd($user->foto);
        $user->user->save();
      }else {
        $user->user->foto='avatar.png';
        $user->user->save();
      }

      return redirect('master/pembeli')->with('success','Member berhasil ditambahkan.');
  }

  public function store2($id, Request $request)
  {
      date_default_timezone_set('Asia/Jakarta');
      $pembeli = Pembeli::where('id', $id)->first();
      $data= User::where('id',$pembeli->user_id)->first();
      $this->validate(request(),
      [
        'password' => 'required|min:8',
        'email' => 'required|email|unique:users',
        'no_telepon' => 'required|unique:users|max:15',
        'tanggal_lahir' => 'required',
        // 'status_id' => 'required',
        'saldo' => 'required',
      ],
      [
        'password.required' => 'Password harus diisi!',
        'password.min' => ' Password minimal 8 karetker!',
        'email.required' => 'Email harus diisi!',
        'email.email' => 'Format email salah!',
        'email.unique' => 'Email sudah pernah terdaftar!',
        'no_telepon.required' => 'Nomer telepon harus diisi!',
        'no_telepon.unique' => 'Nomer telepon sudah terdaftar!',
        'no_telepon.max' => 'Nomer telepon terlalu panjang!',
        'tanggal_lahir.required' => 'Tanggal lahir harus diisi!',
        'saldo.required' => 'Saldo harus diisi!'
      ]);

      // $jajal = $request->get('name');
      // dd($jajal);

      if(!empty($request->foto)){
         $file = $request->file('foto');
         $extension = strtolower($file->getClientOriginalExtension());
         $filename = $request->get('name') . '.' . $extension;
         Storage::put('images/' . $filename, File::get($file));
         $file_server = Storage::get('images/' . $filename);
         $img = Image::make($file_server)->resize(141, 141);
         $img->save(base_path('public/images/' . $filename));
       }

      $user = User::create([
        'level'=>'Pembeli',
        'name'=>$request->name,
        'password'=>bcrypt($request->password),
        'email'=>$request->email,
        'no_telepon'=>$request->no_telepon,
        'tanggal_lahir'=>$request->tanggal_lahir = Carbon::parse($request->tanggal_lahir),
        'status_id'=>2,
        'saldo'=>$request->saldo,
      ]);

      $pembeli->user_id=DB::getPdo()->lastInsertId();
      $pembeli->member='Ya';
      $pembeli->save();

      if (!empty($request->foto)) {
        $user->foto=$filename;
        // dd($user->foto);
        $user->save();
      }else {
        $user->foto='avatar.png';
        $user->save();
      }

      return redirect('master/pembeli_bukan_member')->with('success','Member berhasil ditambahkan.');
  }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
         $data = Pembeli::where('id', $id)->first();
         // $status = Status::all();
         return view('pembeli-master.edit', compact('data'));
        //  dd($data);
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      date_default_timezone_set('Asia/Jakarta');
      $pembeli = Pembeli::where('id', $id)->first();
      $pembeli->nama=$request->name;
      $data= User::where('id',$pembeli->user_id)->first();
      $this->validate($request,
      [
        'name' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)|max:30',
        'no_telepon' => 'required|max:15',
        'tanggal_lahir' => 'required',
        // 'saldo' => 'required',
      ],
      [
        'name.required' => 'Nama harus diisi!',
        'name.regex' => 'Nama tidak boleh menggunakan angka!',
        'name.max' => 'Nama terlalu panjang, maksimal 30 karakter!',
        'no_telepon.required' => 'Nomer telepon harus diisi!',
        'no_telepon.unique' => 'Nomer telepon sudah terdaftar!',
        'no_telepon.max' => 'Nomer telepon terlalu panjang!',
        'tanggal_lahir.required' => 'Tanggal lahir harus diisi!'
      ]);
      if ($data->name != $request->name) {
        $this->validate($request,
        [
          'name' => 'unique:users',
        ],
        [
          'name.unique' => 'Nama sudah terdaftar gunakan nama yang lain!',
        ]);
      }
      if ($data->no_telepon != $request->no_telepon) {
        $this->validate($request,
        [
          'no_telepon' => 'unique:users',
        ],
        [
          'no_telepon.unique' => 'Nomer telepon sudah terdaftar!',
        ]);
      }
      $data->name=$request->name;
      $data->email=$request->email;
      $data->no_telepon=$request->no_telepon;
      $data->tanggal_lahir=$request->tanggal_lahir = Carbon::parse($request->tanggal_lahir);

      // $check_name = User::where('name', $request->name)->get()->count();
      // if($check_name == 1)

      $data->save();
      $pembeli->save();
      
      return redirect('master/pembeli/'.$pembeli->id.'')->with('success','Data berhasil diperbarui!');;
    }

    public function editPassword($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $data = Pembeli::where('id', $id)->first();
        return view('pembeli-master.edit-password', compact('data'));
    }
    public function updatePassword(Request $request, $id)
    {
      date_default_timezone_set('Asia/Jakarta');
      $pembeli = Pembeli::where('id', $id)->first();
      $data= User::where('id',$pembeli->user_id)->first();
      $data->password=bcrypt($request->password);

      $this->validate($request,
        [
          'password' => 'required|min:8',
        ],
        [
          'password.required' => 'Password harus diisi',
          'password.min' => ' Password minimal 8 karetker!',
        ]);

      $data->save();

      return redirect('master/pembeli/'.$pembeli->id.'')->with('success','Data berhasil diperbarui!');
    }

    public function editAvatar($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $data = Pembeli::find($id);
        return view('pembeli-master.edit-ava', compact('data'));
    }
    public function updateAvatar(Request $request, $id)
    {
      date_default_timezone_set('Asia/Jakarta');
      $pembeli = Pembeli::find($id);
      $data= User::where('id',$pembeli->user_id)->first();

      $this->validate($request,
      [
        'foto' => 'required',
      ],
      [
        'foto.required' => 'Foto harus diisi!',
      ]);

      $file = $request->file('foto');
      $extension = strtolower($file->getClientOriginalExtension());
      $filename = $data->name . '.' . $extension;
      Storage::put('images/' . $filename, File::get($file));
      $file_server = Storage::get('images/' . $filename);
      $img = Image::make($file_server)->resize(141, 141);
      $img->save(base_path('public/images/' . $filename));

      $data->foto=$filename;
      $data->save();
      
      return redirect('master/pembeli/'.$pembeli->id.'')->with('success','Data berhasil diperbarui!');;
    }

    public function status(Request $request, $user_id)
    {

      $data= User::find($user_id);
      if($data['status_id']==1){
        $data->status_id=2;
      }
      else{
        $data->status_id=1;
      }
      $data->save();
      
      return redirect()->back();
      // ->with('alert', 'Status has been changed!');
    }
    public function detail($id)
    {
        $data = Pembeli::find($id);
        return view('pembeli-master.detail', compact('data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
      $cek2 = DB::table('transaksi_konven')
              ->leftJoin('pembeli', 'transaksi_konven.pembeli_id', '=', 'transaksi_konven.id')
              ->leftJoin('users', 'pembeli.user_id', '=', 'users.id')
              ->where('transaksi_konven.pembeli_id',$id)
              ->get();

      $getIdUser = DB::table('pembeli')
              ->leftJoin('users', 'pembeli.user_id', '=', 'users.id')
              ->where('pembeli.id',$id)
              ->pluck('user_id');


      // remove id from KATEGORI
      if($cek2->isNotEmpty()){
            return redirect()->back()->with('error', 'Pembeli yang dipilih masih memiliki data transaksi.');
      }else{
      $data = Pembeli::find($id)->delete();
      $data2 = DB::table('users')->where('id',$getIdUser)->delete();
      // return response()->json($data);
      return redirect()->back()->with('success', 'Data pembeli berhasil dihapus!');
      }
    }

    public function downloadExcel(Request $request)
    {
        $data = DB::table('pembeli')
            ->leftJoin('users', 'pembeli.user_id', '=', 'users.id')
            ->select('pembeli.id','pembeli.nama','users.email','users.no_telepon','users.tanggal_lahir')
            ->where('pembeli.member','Ya')
            ->get();

        $data= json_decode( json_encode($data), true);
        Excel::create('Data Member', function($excel) use($data){
        $excel->sheet('Data Member', function ($sheet) use ($data) {
            $sheet->fromArray($data);
            });
        })->download("xlsx");
    }

    public function downloadTemplateExcel(Request $request)
    {
        $data = User::where('id','0')->get()->toArray();
        return Excel::create('Template Import Pembeli', function($excel) use ($data) {
            $excel->sheet('import_pembeli', function($sheet) use ($data)
            {
                $sheet->cell('A1', function($cell) {$cell->setValue('id_user');   });
                $sheet->cell('B1', function($cell) {$cell->setValue('nama_pembeli');   });
                $sheet->cell('C1', function($cell) {$cell->setValue('email');   });
                $sheet->cell('D1', function($cell) {$cell->setValue('password');   });
                $sheet->cell('E1', function($cell) {$cell->setValue('no_telepon');   });
                $sheet->cell('F1', function($cell) {$cell->setValue('tanggal_lahir');   });
                $sheet->cell('G1', function($cell) {$cell->setValue('saldo');   });

            });
        })->download("xlsx");
    }

    public function import(Request $request)
    {
      $this->validate($request,
      [
        'import' => 'required',
      ],
      [
        'import.required' => 'File belum dimasukan!',
      ]);

        

        if ($request->hasFile('import')) {
            $path = $request->file('import')->getRealPath();

            $data = Excel::load($path, function($reader){})->get();
            $a = collect($data);            

            $k=$a->first()->keys()->toArray();
            $diff = count(array_diff($k, array('id_user','nama_pembeli','email','password','no_telepon','tanggal_lahir','saldo')));

            if($diff>0){
                return back()->with('error', 'Format Salah!');
            }

            if (!empty($a) && $a->count()) {
                foreach ($a as $key => $value) {

            $cek1 = DB::table('users')->where('id', $value->id_user)->get();
            
            if($cek1->isNotEmpty()){
                return back()->with('error', 'Id user'.' '.  $value->id_user .' '. 'yang dimasukan pada sudah terdaftar. Silahkan cek ulang juga data yang berada dibawahnya kemudian lakukan import ulang!');
            }else{

                $insert[] = [
                            'id' => $value->id_user, 
                            'level' => 'Pembeli', 
                            'name' => $value->nama_pembeli, 
                            'email' => $value->email,
                            'password' => bcrypt($value->password),
                            'no_telepon' => $value->no_telepon,
                            'tanggal_lahir' => $value->tanggal_lahir,
                            'saldo' => $value->saldo,
                            'status_id' => '2',
                            ];

                        User::create($insert[$key]);

                $insert2[] = [
                            'user_id' => $value->id_user, 
                            'nama' => $value->nama_pembeli, 
                            'member' => 'Ya', 
                            ];                          
                        
                        Pembeli::create($insert2[$key]);
                      }
                    }
                    return redirect('master/pembeli')->with('success', 'Data berhasil di import.');
            };            
        }
    }
}
