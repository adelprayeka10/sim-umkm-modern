<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Pembeli;
use App\Status;
use App\DetailTransaksiKonven;
use Carbon\Carbon;
use PDF;
use App\Exports\PembeliExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
Use Alert;
use Auth;
use DB;

class PembeliReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $pembeli = User::where('level','Pembeli')->get()->sortBy('name');
      return view('pembeli-report.find',compact('pembeli'));
    }

    public function laporan(Request $request)
    {
      if (Carbon::parse($request->from) > Carbon::parse($request->until)) {
        return redirect()->back()->with('error', 'Tanggal tidak sesuai!');
      }
      $this->validate(request(),
        [
          'pembeli' => 'required',
          'from' => 'required',
          'until' => 'required',
        ],
        [
          'pembeli.required' => 'Silahkan pilih pembeli terlebih dulu!',
          'from.required' => 'Tanggal awal tidak boleh kosong!',
          'until.required' => 'Tanggal selesai tidak boleh kosong!',
        ]
      );

      $list = DetailTransaksiKonven::leftJoin('transaksi_konven', 'detail_transaksi_konven.transaksi_konven_id', '=', 'transaksi_konven.id')
                              ->leftJoin('jenis_pembayaran', 'transaksi_konven.jenis_pembayaran_id', '=', 'jenis_pembayaran.id')
                              ->leftJoin('pembeli', 'transaksi_konven.pembeli_id', '=', 'pembeli.id')
                              ->leftJoin('penjual_konven', 'transaksi_konven.penjual_konven_id', '=', 'penjual_konven.id')
                              ->leftJoin('barang_konven', 'detail_transaksi_konven.barang_konven_id', '=', 'barang_konven.id')
                              ->leftJoin('users', 'pembeli.user_id', '=', 'users.id')
                              ->where('users.id', $request->pembeli)
                              ->where('transaksi_konven.updated_at', '>=', $request->from = Carbon::parse($request->from))
                              ->where('transaksi_konven.updated_at', '<=', $request->until = Carbon::parse($request->until)->addDay(1))
                              ->select('transaksi_konven.updated_at', 'transaksi_konven.id as transaksi_konven_id', 'barang_konven.nama', 'detail_transaksi_konven.harga', 'detail_transaksi_konven.diskon', 'penjual_konven.nama_toko', 'detail_transaksi_konven.jumlah', 'detail_transaksi_konven.total', 'jenis_pembayaran.jenis_pembayaran', 'transaksi_konven.status')
                              ->get();

        $solds = DetailTransaksiKonven::leftJoin('transaksi_konven', 'detail_transaksi_konven.transaksi_konven_id', '=', 'transaksi_konven.id')
                              ->leftJoin('pembeli', 'transaksi_konven.pembeli_id', '=', 'pembeli.id')
                              ->leftJoin('barang_konven', 'detail_transaksi_konven.barang_konven_id', '=', 'barang_konven.id')
                              ->groupBy('barang_konven.id', 'barang_konven.id', 'barang_konven.harga')
                              ->where('pembeli.user_id', $request->pembeli)
                              ->where('transaksi_konven.status','=','Lunas')
                              ->where('transaksi_konven.updated_at', '>=', $request->from = Carbon::parse($request->from))
                              ->where('transaksi_konven.updated_at', '<=', $request->until = Carbon::parse($request->until)->addDay(1))
                              ->selectRaw('sum(jumlah)*barang_konven.harga as outcome, sum(jumlah) as item')
                              ->get();

      $outcomeTotal=0;
      $itemTotal=0;
      foreach ($solds as $key => $value) {
          $outcomeTotal += $value['outcome'];
          $itemTotal += $value['item'];
      }

      $pembeli = User::where('id',$request->pembeli)->first();
      $pembeliAll = User::where('level','Pembeli')->get()->sortBy('name');
      $requested = $request;
      
      return view('pembeli-report.laporan',compact('pembeli','list','requested', 'pembeliAll', 'outcomeTotal', 'itemTotal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function downloadPDF(request $request){

      date_default_timezone_set('Asia/Jakarta');

     $list = DetailTransaksiKonven::leftJoin('transaksi_konven', 'detail_transaksi_konven.transaksi_konven_id', '=', 'transaksi_konven.id')
                              ->leftJoin('jenis_pembayaran', 'transaksi_konven.jenis_pembayaran_id', '=', 'jenis_pembayaran.id')
                              ->leftJoin('pembeli', 'transaksi_konven.pembeli_id', '=', 'pembeli.id')
                              ->leftJoin('penjual_konven', 'transaksi_konven.penjual_konven_id', '=', 'penjual_konven.id')
                              ->leftJoin('barang_konven', 'detail_transaksi_konven.barang_konven_id', '=', 'barang_konven.id')
                              ->leftJoin('users', 'pembeli.user_id', '=', 'users.id')
                              ->where('users.id', $request->pembeli)
                              ->where('transaksi_konven.updated_at', '>=', $request->from = Carbon::parse($request->from))
                              ->where('transaksi_konven.updated_at', '<=', $request->until = Carbon::parse($request->until)->addDay(1))
                              ->select('transaksi_konven.updated_at', 'transaksi_konven.id as transaksi_konven_id', 'barang_konven.nama', 'detail_transaksi_konven.harga', 'detail_transaksi_konven.diskon', 'penjual_konven.nama_toko', 'detail_transaksi_konven.jumlah', 'detail_transaksi_konven.total', 'jenis_pembayaran.jenis_pembayaran', 'transaksi_konven.status')
                              ->get();

        $solds = DetailTransaksiKonven::leftJoin('transaksi_konven', 'detail_transaksi_konven.transaksi_konven_id', '=', 'transaksi_konven.id')
                              ->leftJoin('pembeli', 'transaksi_konven.pembeli_id', '=', 'pembeli.id')
                              ->leftJoin('barang_konven', 'detail_transaksi_konven.barang_konven_id', '=', 'barang_konven.id')
                              ->groupBy('barang_konven.id', 'barang_konven.id', 'barang_konven.harga')
                              ->where('pembeli.user_id', $request->pembeli)
                              ->where('transaksi_konven.status','=','Lunas')
                              ->where('transaksi_konven.updated_at', '>=', $request->from = Carbon::parse($request->from))
                              ->where('transaksi_konven.updated_at', '<=', $request->until = Carbon::parse($request->until)->addDay(1))
                              ->selectRaw('sum(jumlah)*barang_konven.harga as outcome, sum(jumlah) as item')
                              ->get();

      $outcomeTotal=0;
      $itemTotal=0;
      foreach ($solds as $key => $value) {
          $outcomeTotal += $value['outcome'];
          $itemTotal += $value['item'];
      }

      $pembeli = User::where('id',$request->pembeli)->first();

       $pdf = PDF::loadView('pembeli-report.pdf', compact('list', 'request', 'pembeli', 'outcomeTotal', 'itemTotal'));
       $request->until = Carbon::parse($request->until)->addDay(-1)->format('dmY');
       $request->from = Carbon::parse($request->from)->format('dmY');
       return $pdf->download('Laporan-'.$pembeli->name.'-'.$request->from.'-'.$request->until.'.pdf');

     }
     public function downloadExcel(Request $request)
     {

        $path1 = $request->get('from');
        $path = $request->get('until');

        if(!empty($path1) && !empty($path)){

            $user = Auth::user()->penjual_konven()->pluck('id')->first();

            $data = DB::table('detail_transaksi_konven')
                ->leftJoin('transaksi_konven', 'detail_transaksi_konven.transaksi_konven_id', '=', 'transaksi_konven.id')
                ->leftJoin('jenis_pembayaran', 'transaksi_konven.jenis_pembayaran_id', '=', 'jenis_pembayaran.id')
                ->leftJoin('penjual_konven', 'transaksi_konven.penjual_konven_id', '=', 'penjual_konven.id')
                ->leftJoin('pembeli', 'transaksi_konven.pembeli_id', '=', 'pembeli.id')
                ->leftJoin('barang_konven', 'detail_transaksi_konven.barang_konven_id', '=', 'barang_konven.id')
                ->leftJoin('users', 'pembeli.user_id', '=', 'users.id')
                ->where('users.id', $request->pembeli)
                ->select('transaksi_konven.id','penjual_konven.nama_toko','barang_konven.nama','detail_transaksi_konven.harga','detail_transaksi_konven.diskon','detail_transaksi_konven.jumlah','detail_transaksi_konven.total','jenis_pembayaran.jenis_pembayaran','detail_transaksi_konven.created_at')
                ->whereBetween('transaksi_konven.updated_at',[$path1,$path])
                ->get();

            $data= json_decode( json_encode($data), true);
            Excel::create('Data Transaksi', function($excel) use($data){
            $excel->sheet('Data Transaksi', function ($sheet) use ($data) {
                $sheet->fromArray($data);
                });
            })->download("xlsx");
        }
     }
}
