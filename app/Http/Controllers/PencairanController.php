<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pencairan;
use App\DetailTransaksi;
use App\Transaksi;
use App\User;
use App\PenjualKonven;
use App\LogSaldo;
use App\Bank;
use Alert;
use DB;

use Auth;


class PencairanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function find()
    {
        $list = User::where('level','Penjual')->get()->sortBy('name');
        return view('pencairan.find',compact('list'));
    }
    public function index(Request $request)
    {
        if ($request->status == 'all') {
          $data = Pencairan::all()->sortBy('created_at');
        }
        else
        {
          $data = Pencairan::where('status', $request->status)->get()->sortBy('created_at');
        }
        return view('pencairan.list',compact('data','request'));
    }

    public function detail($id)
    {
        $data = User::find($id);
        $data2 = PenjualKonven::join('bank', 'penjual_konven.bank_id', '=', 'bank.id')
                ->where('penjual_konven.user_id','=',$id)
                ->select('bank.id','bank.nama as nama')
                ->get()->first();
        // dd($data2);
        $history = LogSaldo::where('user_id', $id)->get();
        $bank = Bank::get()->sortBy('nama');
       
        return view('pencairan.detail',compact('data','bank','history','data2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      date_default_timezone_set('Asia/Jakarta');
      DB::beginTransaction();
      $this->validate($request,
      [
        'total' => 'required|min:5000',
        'bank' => 'required',
      ],
      [
        'total.required' => 'Nominal harus diisi!',
        'total.min' => 'Nominal harus diatas 5000 rupiah!',
        'bank.required' => 'Bank harus diisi!',
      ]);
      $penjual = User::where('id', $request->user_id)->first();
      $pencairan = Pencairan::create([
        'user_id'=>$penjual->id,
        'total'=>$request->total,
        'bank_id'=>$request->bank,

      ]);
      if (!$pencairan) {
        DB::rollback();
        
        return redirect('payment')->with('Error', 'Ada yang salah!');
      }
      $pencairan->logSaldo()->create([
          'user_id'=>$penjual->id,
          'saldo_awal'=>$penjual->saldo,
          'type'=>'pencairan',
          'total'=>$request->total,
          'grand_total'=>$penjual->saldo - $request->total
        ]);
        if (!$pencairan) {
          DB::rollback();
          return redirect('payment')->with('Error', 'Ada yang salah!');
        }
      $penjual->saldo = $pencairan->logSaldo->grand_total;
      $penjual->save();
      DB::commit();
      
      return redirect()->back()->with('success', 'Saldo berhasil di updated!');
    }

    public function update(Request $request, $id)
    {
        date_default_timezone_set('Asia/Jakarta');
        DB::beginTransaction();
        // $data = Pencairan::find($id)->first();
        $data = Pencairan::where('id', $id)->first();
        // dd($data);
        if ($request->status == "failed") {
          $data->logSaldo()->create([
              'user_id'=>$data->user->id,
              'saldo_awal'=>$data->user->saldo,
              'type'=>'withdraw',
              'total'=>$data->total,
              'grand_total'=>$data->user->saldo + $data->total
            ]);
            if (!$data) {
              DB::rollback();
              Alert::success('Warning', 'Something wrong!');
              return redirect('payment');
            }
            $penjual = User::where('id', $data->user->id)->first();
            $penjual->saldo += $data->total;
            $penjual->save();
            $data->status = $request->status;
            $data->save();
            if (!$penjual) {
              DB::rollback();
              Alert::success('Warning', 'Something wrong!');
              return redirect('payment');
            }
          }
        else {
          $data->status = $request->status;
          $data->save();
        }
        DB::commit();
        Alert::success('Success', 'Withdrawing has been updated!');
        return redirect('pencairan/'.$id.'');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

// START OF API CONTROLLER

public function api_store(Request $request)
    {
      DB::beginTransaction();
      $penjual = User::find(Auth::user()->id);
      // dd($penjual);
      $data = Pencairan::create([
        'user_id'=>$penjual->id,
        'total'=>$request->total,
        'status'=>'waiting',
      ]);
      if (!$data) {
        DB::rollback();
        return response()->json([
          'status'=>'failed',
          'error'=>'Something wrong!',
          'message'=>'Something wrong!',
        ]);
      }
      $data->logSaldo()->create([
        'user_id'=>$penjual->id,
        'saldo_awal'=>$penjual->saldo,
        'type'=>'withdraw',
        'total'=>$request->total,
        'grand_total'=> $penjual->saldo - $request->total
      ]);
      if (!$data) {
        DB::rollback();
        return response()->json([
          'status'=>'failed',
          'error'=>'Something wrong!',
          'message'=>'Something wrong!',
        ]);
      }
      $penjual->saldo = $data->logSaldo->grand_total;
      $penjual->save();
      DB::commit();
      return response()->json([
        'status'=>'success',
        'result'=>$data
      ]);
    }
}
