<?php

namespace App\Http\Controllers;
use app\Exceptions\Handler;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use File;
use Illuminate\Http\Request;
use App\User;
use App\PenjualKonven;
use App\PenjualKonvenMaster;
use App\Bank;
use Carbon\Carbon;
Use DB;
Use Auth;

class PenjualKonvenController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = PenjualKonven::leftJoin('users', 'penjual_konven.user_id', '=', 'users.id')
                        ->select('penjual_konven.id', 'penjual_konven.user_id', 'users.name', 'users.email','users.no_telepon','users.status_id')
                        // ->where('status_id', 1)
                        ->get();
        return view('penjual-master.list', compact('list'));
    }
    public function aktif()
    {
        $list = PenjualKonven::leftJoin('users', 'penjual_konven.user_id', '=', 'users.id')
                        ->select('penjual_konven.id', 'penjual_konven.user_id', 'users.name', 'users.email','users.no_telepon','users.status_id')
                        ->where('status_id', 1)
                        ->get();
        return view('penjual-master.list', compact('list'));
    }
    public function nonAktif()
    {
        $list = PenjualKonven::leftJoin('users', 'penjual_konven.user_id', '=', 'users.id')
                        ->select('penjual_konven.id', 'penjual_konven.user_id', 'users.name', 'users.email','users.no_telepon','users.status_id')
                        ->where('status_id', 2)
                        ->get();
        return view('penjual-master.list', compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bank = Bank::all();
        return view('penjual-master.create', compact('bank'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
  {
    date_default_timezone_set('Asia/Jakarta');
    $this->validate(request(),
      [
        'name' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)|unique:users|max:30',
        'password' => 'required|min:8',
        'email' => 'required|email|unique:users',
        'no_telepon' => 'required|unique:users|max:15',
        'no_rek' => 'required|unique:penjual_konven|max:30',
        'tanggal_lahir' => 'required',
        'bank' => 'required',
        'saldo' => 'required',
      ],
      [

        'name.required' => 'Nama harus diisi!',
        'name.regex' => 'Nama tidak boleh menggunakan angka!',
        'name.unique' => 'Nama sudah pernah terdaftar, coba dengan nama lain!',
        'name.max' => 'Nama terlalu panjang, maksimal 30 karakter!',
        'password.required' => 'Password harus diisi!',
        'password.min' => ' Password minimal 8 karetker!',
        'email.required' => 'Email harus diisi!',
        'email.email' => 'Format email salah!',
        'email.unique' => 'Email sudah pernah terdaftar!',
        'no_telepon.required' => 'Nomer telepon harus diisi!',
        'no_telepon.unique' => 'Nomer telepon sudah terdaftar!',
        'no_telepon.max' => 'Nomer telepon terlalu panjang!',
        'no_rek.required' => 'Nomer rekening harus diisi!',
        'no_rek.unique' => 'Nomer rekening sudah terdaftar!',
        'no_rek.max' => 'Nomer rekening terlalu panjang!',
        'tanggal_lahir.required' => 'Tanggal lahir harus diisi!',
        'saldo.required' => 'Saldo harus diisi!',
        'bank.required' => 'Bank harus dipilih!'
      ]);

      if(!empty($request->foto)){
         $file = $request->file('foto');
         $extension = strtolower($file->getClientOriginalExtension());
         $filename = $request->name . '.' . $extension;
         Storage::put('images/' . $filename, File::get($file));
         $file_server = Storage::get('images/' . $filename);
         $img = Image::make($file_server)->resize(141, 141);
         $img->save(base_path('public/images/' . $filename));
       }

      $user = User::create([
        'level'=>'Penjual',
        'name'=>$request->name,
        'password'=>bcrypt($request->password),
        'email'=>$request->email,
        'no_telepon'=>$request->no_telepon,
        'tanggal_lahir'=>$request->tanggal_lahir = Carbon::parse($request->tanggal_lahir),
        'status_id'=>2,
        'saldo'=>$request->saldo,
      ])
      ->penjual_konven()->create([
        'nama_toko'=>$request->name,
        'no_rek'=>$request->no_rek,
        'bank_id'=>$request->bank,
      ]);
      if (!empty($request->foto)) {
        $user->user->foto=$filename;
        // dd($user->foto);
        $user->user->save();
      }else {
        $user->user->foto='avatar.png';
        $user->user->save();
      }

      
      return redirect('master/penjual_konven')->with('success','Penjual berhasil ditambahkan!');
  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
         $data = PenjualKonven::where('id', $id)->first();
         // $status = Status::all();
         return view('penjual-master.edit', compact('data'));
        //  dd($data);
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      date_default_timezone_set('Asia/Jakarta');
      $penjual = PenjualKonven::where('id', $id)->first();
      // $penjual_konven->nama=$request->name;
      $data= User::where('id',$penjual->user_id)->first();
      $this->validate($request,
      [
        'name' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)|max:30',
        'no_telepon' => 'required',
        'no_rek' => 'required',
        'tanggal_lahir' => 'required',
        // 'saldo' => 'required',
      ],
      [
        'name.required' => 'Nama harus diisi!',
        'name.regex' => 'Nama tidak boleh menggunakan angka!',
        'name.max' => 'Nama terlalu panjang, maksimal 30 karakter!',
        'no_telepon.required' => 'Nomer telepon harus diisi!',
        'no_telepon.unique' => 'Nomer telepon sudah terdaftar!',
        'no_telepon.max' => 'Nomer telepon terlalu panjang!',
        'no_rek.required' => 'Nomer rekening harus diisi!',
        'tanggal_lahir.required' => 'Tanggal lahir harus diisi!',
      ]);
      if ($data->name != $request->name) {
        $this->validate($request,
        [
          'name' => 'unique:users',
        ]);
      }
      if ($data->no_telepon != $request->no_telepon) {
        $this->validate($request,
        [
          'no_telepon' => 'unique:users',
        ],
        [
          'no_telepon.unique' => 'Phone number has already been taken.',
        ]);
      }
      $data->name=$request->name;
      $data->email=$request->email;
      $data->no_telepon=$request->no_telepon;
      $data->tanggal_lahir=$request->tanggal_lahir = Carbon::parse($request->tanggal_lahir);
      $penjual->no_rek=$request->no_rek;

      // $check_name = User::where('name', $request->name)->get()->count();
      // if($check_name == 1)

      $data->save();
      $penjual->save();
      
      return redirect('master/penjual_konven/'.$penjual->id.'')->with('success','Data berhasil diperbarui!');
    }

    public function editPassword($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $data = PenjualKonven::where('id', $id)->first();
        return view('penjual-master.edit-password', compact('data'));
       //  dd($data);
    }
    public function updatePassword(Request $request, $id)
    {
      date_default_timezone_set('Asia/Jakarta');
      $pembeli = PenjualKonven::where('id', $id)->first();
      $data= User::where('id',$pembeli->user_id)->first();
      $data->password=bcrypt($request->password);

      $this->validate($request,
        [
          'password' => 'required|min:8',
        ],
        [
          'password.required' => 'Password harus diisi!',
          'password.min' => ' Password minimal 8 karetker!',
        ]);

      $data->save();

      return redirect('master/penjual_konven/'.$pembeli->id.'')->with('success','Data berhasil diperbarui!');
      // ->with('alert', 'Password has been changed!');
      // dd($data);
    }
    public function topUp($id)
    {
        $data = PenjualKonven::where('id', $id)->first();
        return view('pembeli-master.tambah-saldo', compact('data'));
       //  dd($data);
    }
    public function topUpStore(Request $request, $id)
    {
      $pembeli = Pembeli::where('id', $id)->first();
      $pembeli->saldo = $pembeli->saldo + $request->topup;

      $this->validate($request,
        [
          'topup' => 'required|',
        ],
        [
          'topup.required' => 'Top up field can not be empty!',
        ]);

      $pembeli->save();
      Alert::success('Success', 'Saldo has been updated!');
      return redirect('master/pembeli/'.$pembeli->id.'');
      // ->with('alert', 'Password has been changed!');
      // dd($data);
    }
    public function editAvatar($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $data = PenjualKonven::find($id);
        return view('penjual-master.edit-ava', compact('data'));
    }
    public function updateAvatar(Request $request, $id)
    {
      date_default_timezone_set('Asia/Jakarta');
      $penjual = PenjualKonven::find($id);
      $data= User::where('id',$penjual->user_id)->first();

      $this->validate($request,
      [
        'foto' => 'required',
      ],
      [
        'foto.required' => 'Foto harus diisi!',
      ]);

      $file = $request->file('foto');
      $extension = strtolower($file->getClientOriginalExtension());
      $filename = $data->name . '.' . $extension;
      Storage::put('images/' . $filename, File::get($file));
      $file_server = Storage::get('images/' . $filename);
      $img = Image::make($file_server)->resize(141, 141);
      $img->save(base_path('public/images/' . $filename));

      $data->foto=$filename;
      $data->save();
   
      return redirect('master/penjual_konven/'.$penjual->id.'')->with('success','Data berhasil diperbarui!');
    }

    public function status(Request $request, $user_id)
    {

      $data= User::find($user_id);
      if($data['status_id']==1){
        $data->status_id=2;
      }
      else{
        $data->status_id=1;
      }
      $data->save();
      
      return redirect()->back();
      // ->with('alert', 'Status has been changed!');
    }
    public function detail($id)
    {
        $data = PenjualKonven::find($id);
        $bank = PenjualKonven::join('bank','bank.id','penjual_konven.bank_id')
                ->select('bank.nama')->where('penjual_konven.id',$id)->pluck('nama')->first();
        return view('penjual-master.detail', compact('data','bank'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
      $cek2 = DB::table('transaksi_konven')
              ->leftJoin('penjual_konven', 'transaksi_konven.penjual_konven_id', '=', 'penjual_konven.id')
              ->leftJoin('users', 'penjual_konven.user_id', '=', 'users.id')
              ->where('transaksi_konven.penjual_konven_id',$id)
              ->get();

      $cek3 = DB::table('barang_konven')
              ->leftJoin('penjual_konven', 'barang_konven.penjual_konven_id', '=', 'penjual_konven.id')
              ->leftJoin('users', 'penjual_konven.user_id', '=', 'users.id')
              ->where('barang_konven.penjual_konven_id',$id)
              ->get();

      $getIdUser = DB::table('penjual_konven')
              ->leftJoin('users', 'penjual_konven.user_id', '=', 'users.id')
              ->where('penjual_konven.id',$id)
              ->pluck('user_id');


      // remove id from KATEGORI
      if($cek2->isNotEmpty()){
            return redirect()->back()->with('error', 'Penjual yang dipilih masih memiliki data transaksi.');
      }elseif($cek3->isNotEmpty()){
            return redirect()->back()->with('error', 'Penjual yang dipilih masih terintegrasi dengan data produk.');
      }else{
      $data = PenjualKonven::find($id)->delete();
      $data2 = DB::table('users')->where('id',$getIdUser)->delete();
      // return response()->json($data);
      return redirect()->back()->with('success', 'Data penjual berhasil dihapus!');
    }
  }


    public function api_index_barang()
    {
    
       $barang = Auth::user()->BarangKonven()->get();
         //dd($barang);
        return response()->json([
          // 'error'=>false,
          'status'=>'success',
          // 'token' => $success['token'],
          'barang_konvensional' => $barang
      ]);
        
      

    }


    // public function api_transaksi(Request $request){
    //   $validatedData = $request->validate([
    //     'id' => 'required',
    //     'pembeli_id' => 'required',
    //     'jenis_pembayaran_id' => 'required',
    //     'status' => 'required',
    //     'diskon' => 'total',
    // ]);


    // }


    public function api_cek_pin(Request $request)
    {
        $request->validate(['id_pembeli'=>'required','pin'=>'required']);
        $pin=User::find($request->id_pembeli)->pin;
        if($pin == $request->pin){
          return response()->json([
            // 'error'=>false,
            'status'=>'success',
            // 'token' => $success['token'],
            'pin' => 'Verified'
        ]);
        }
        
        else
        {
          return response()->json([
            'status'=>'fail',
            'pin' => 'Rejected'
            ]);
        }
        

        
    }
}
