<?php

namespace App\Http\Controllers;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use File;
use Illuminate\Http\Request;
use App\User;
use App\Bank;
use App\PenjualKonven;
use Alert;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;


class ProfilPosController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index($id)
  {
      $id_penjual = Auth::user()->penjual_konven()->pluck('id')->first();  
      $user = User::find($id);
      $data2 = PenjualKonven::where('penjual_konven.user_id','=',$id)->get()->first();
      $bank = PenjualKonven::join('bank','bank.id','penjual_konven.bank_id')
                ->select('bank.nama')->where('penjual_konven.id',$id_penjual)->pluck('nama')->first();
      return view('profil-pos.index', compact('user','data2','bank'));
  }
  public function edit($id)
  {
    $data = User::find($id);
    $data2 = PenjualKonven::where('penjual_konven.user_id','=',$id)->get()->first();

    return view('profil-pos.edit',compact('data','data2'));
  }
  public function editPassword($id)
  {
    $data = User::find($id);
    return view('profil-pos.edit-password',compact('data'));
  }
  public function update(Request $request, $id)
  {
    date_default_timezone_set('Asia/Jakarta');
    $data= User::find($id);
    $data2 = PenjualKonven::where('user_id',$id)->get()->first();

    $data->email=$request->email;
    $data->name=$request->name;
    $data->no_telepon=$request->no_hp;
    $data->tanggal_lahir=$request->tanggal_lahir = Carbon::parse($request->tanggal_lahir);
    $data2->no_rek=$request->no_rek;
    // $data->password=bcrypt($request->password);
    $this->validate($request,
      [
        // 'name' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)',
        'no_hp' => 'required',
        'no_rek' => 'required',
        'tanggal_lahir' => 'required',
        // 'saldo' => 'required',
      ],
      [
        // 'name.required' => 'Name can not be empty!',
        // 'name.regex' => 'Name can not be numeric only!',
        'no_hp.required' => 'Nomer HP harus diisi!',
        'no_rek.required' => 'Nomer Rekening harus diisi!',
        'tanggal_lahir.required' => 'Tanggal lahir harus diisi!',
        // 'no_telepon.unique' => 'Phone number has already been taken!',
        // 'saldo.required' => 'Saldo can not be empty!'
      ]);
    $data->save();
    $data2->save();
    
    return redirect('profil_pos/'.$id.'')->with('success', 'Data berhasil di perbarui!');
  }
  public function updatePassword(Request $request, $id)
  {
    date_default_timezone_set('Asia/Jakarta');
    $data= User::find($id);
    if(Hash::check($request->password_current,$data->password)){
      $data->password = Hash::make($request->password);
      $this->validate($request,
        [
          'password_current' => 'required',
          'password' => 'required|string|min:8|confirmed',
          'password_confirmation' => 'required',
          // 'password_confirmation' => 'required|string|min:8|confirmed',
        ],
        [
          'password_current.required' => 'Nomer HP harus diisi!',
          'password_confirmation.required' => 'Password harus diisi!',
          'password.min' => 'Password minimal 8 karakter!',
          'password.required' => 'Password harus diisi!',
          'password.confirmed' => 'Konfirmasi password Anda!',
        ]);
      $data->save();

      return redirect('profil_pos/'.$id.'')->with('success', 'Data berhasil di perbarui!');;
    }else{
        // Alert::warning('Warning!','Something went wrong!');
        // return redirect()->back();
        return redirect()->back()->with('error', 'Password tidak cocok!');
    }
  }
  public function editAvatar($id)
  {
      $data = User::find($id);
      return view('profil-pos.edit-ava', compact('data'));
  }
  public function updateAvatar(Request $request, $id)
  {
    date_default_timezone_set('Asia/Jakarta');
    $data= User::where('id',$id)->first();

    $this->validate($request,
    [
      'foto' => 'required',
    ],
    [
      'foto.required' => 'Foto harus diisi!!',
    ]);

    $file = $request->file('foto');
    $extension = strtolower($file->getClientOriginalExtension());
    $filename = $data->name . '.' . $extension;
    Storage::put('images/' . $filename, File::get($file));
    $file_server = Storage::get('images/' . $filename);
    $img = Image::make($file_server)->resize(141, 141);
    $img->save(base_path('public/images/' . $filename));

    $data->foto=$filename;
    $data->save();

    return redirect('profil_pos/'.$data->id.'')->with('success', 'Data berhasil di perbarui!');;
  }

  public function mutasiSaldo($id)
    {
        $user = User::find(Auth::user()->id);
        $data = LogSaldo::where('user_id', Auth::user()->id)->get();

        return view('profil-pos.mutasi-saldo', compact('data'));
    }
}
