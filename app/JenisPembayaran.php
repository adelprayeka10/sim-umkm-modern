<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisPembayaran extends Model
{
  protected $table = 'jenis_pembayaran';
  protected $fillable = ['jenis_pembayaran','deskripsi'];
  public $timestamps = true;

  public function transaksiKonven()
  {
     return $this->hasMany('App\TransaksiKonven', 'jenis_pembayaran_id'. 'id');
  }
}
