<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
  protected $table = 'kategori';
  protected $fillable = ['kategori','deskripsi'];
  public $timestamps = true;

  public function barang_konven()
      {
          return $this->hasMany('App\BarangKonven', 'id', 'id');
      }
}
