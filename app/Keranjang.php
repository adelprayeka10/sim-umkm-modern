<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Keranjang extends Model
{
  protected $table = 'keranjang';
  // protected $fillable = ['penjual_id','stok_barang_id','kuantitas'];
  protected $guarded = ['created_at', 'updated_at'];
  public $timestamps = true;

  public function barang_konven()
  {
    return $this->belongsTo('App\BarangKonven', 'barang_konven_id', 'id');
  }
  public function penjual_konven()
  {
    return $this->belongsTo('App\PenjualKonven', 'penjual_konven_id', 'id');
  }

}
