<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pencairan extends Model
{
  protected $table = 'pencairan';
  protected $guarded = ['created_at', 'updated_at', 'deleted_at'];
  public $timestamps = true;

  public function user()
      {
          return $this->belongsTo('App\User', 'user_id', 'id');
      }
  public function logSaldo()
  {
      return $this->morphOne('App\LogSaldo', 'pencairan', 'source', 'reference_id');
  }
}
