<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topup extends Model
{
  protected $table = 'topup';
  protected $guarded = ['created_at', 'updated_at'];
  public $timestamps = true;

  public function user()
  {
      return $this->belongsTo('App\User', 'user_id', 'id');
  }
  public function logSaldo()
  {
      return $this->morphOne('App\LogSaldo', 'topup', 'source', 'reference_id');
  }
}
