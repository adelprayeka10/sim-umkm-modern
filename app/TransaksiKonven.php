<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiKonven extends Model
{
    protected $table = 'transaksi_konven';
  	protected $fillable = ['id','penjual_konven_id','pembeli_id','jenis_pembayaran_id','status','total_beli','total','diskon','updated_at'];
    public $timestamps = true;
    protected $primaryKey = 'id';

  public function detailTransaksiKonven()
  {
    return $this->hasMany('App\DetailTransaksiKonven', 'transaksi_konven_id', 'id');
  }
  public function pembeli()
  {
    return $this->belongsTo('App\Pembeli','pembeli_id','id');
  }
  public function penjual_konven()
  {
    return $this->belongsTo('App\PenjualKonven','penjual_konven_id','id');
  }
  public function jenisPembayaran()
  {
    return $this->belongsTo('App\JenisPembayaran','jenis_pembayaran_id','id');
  }
  public function logSaldo()
  {
      return $this->morphOne('App\LogSaldo', 'topup', 'source', 'reference_id');
  }
}
