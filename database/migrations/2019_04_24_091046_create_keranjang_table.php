<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateKeranjangTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('keranjang', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('penjual_konven_id')->unsigned()->index('keranjang_penjual_konven_id_foreign');
			$table->integer('barang_konven_id')->unsigned()->index('keranjang_barang_konven_id_foreign');
			$table->integer('kuantitas')->unsigned();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('keranjang');
	}

}
