<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBarangKonvenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('barang_konven', function(Blueprint $table)
		{
			$table->foreign('penjual_konven_id', 'barang_konven_ibfk_1')->references('id')->on('penjual_konven')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('kategori_id')->references('id')->on('kategori')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('penjual_konven_id')->references('id')->on('penjual_konven')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('barang_konven', function(Blueprint $table)
		{
			$table->dropForeign('barang_konven_ibfk_1');
			$table->dropForeign('barang_konven_kategori_id_foreign');
			$table->dropForeign('barang_konven_penjual_konven_id_foreign');
		});
	}

}
