<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTransaksiKonvenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transaksi_konven', function(Blueprint $table)
		{
			$table->foreign('penjual_konven_id', 'transaksi_konven_ibfk_1')->references('id')->on('penjual_konven')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('jenis_pembayaran_id')->references('id')->on('jenis_pembayaran')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('pembeli_id')->references('id')->on('pembeli')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transaksi_konven', function(Blueprint $table)
		{
			$table->dropForeign('transaksi_konven_ibfk_1');
			$table->dropForeign('transaksi_konven_jenis_pembayaran_id_foreign');
			$table->dropForeign('transaksi_konven_pembeli_id_foreign');
		});
	}

}
