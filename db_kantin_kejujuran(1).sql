-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 23, 2019 at 01:34 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kantin_kejujuran`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(75) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `nama`, `deskripsi`, `created_at`, `updated_at`) VALUES
(5, 'BCA', 'Bank Central Asia', '2019-04-07 02:03:41', '2019-04-07 02:03:41'),
(6, 'BRI', 'Bank Rakyat Indonesia', '2019-04-07 02:03:56', '2019-04-07 02:03:56');

-- --------------------------------------------------------

--
-- Table structure for table `barang_konven`
--

CREATE TABLE `barang_konven` (
  `id` int(10) UNSIGNED NOT NULL,
  `penjual_konven_id` int(10) UNSIGNED NOT NULL,
  `kategori_id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(75) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `harga` int(10) UNSIGNED NOT NULL,
  `status` enum('Tersedia','Habis') COLLATE utf8mb4_unicode_ci NOT NULL,
  `diskon` int(10) UNSIGNED DEFAULT '0',
  `foto` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'product.png',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `barang_konven`
--

INSERT INTO `barang_konven` (`id`, `penjual_konven_id`, `kategori_id`, `nama`, `deskripsi`, `harga`, `status`, `diskon`, `foto`, `created_at`, `updated_at`) VALUES
(1, 23, 1, 'Ayam Geprek Keju  Mbak Rum', 'Ayam geprek dengan parutan keju', 15000, 'Tersedia', 0, 'product.png', '2019-04-02 05:01:56', '2019-04-15 00:39:04'),
(2, 23, 1, 'Ayam Goreng Madu  Mbak Rum', 'Ayam yang digoreng dengan madu', 17000, 'Habis', 0, 'product.png', '2019-04-02 05:03:00', '2019-04-21 10:17:06'),
(3, 23, 1, 'Ca Kangkung  Mbak Rum', '-', 5000, 'Tersedia', 0, 'product.png', '2019-04-02 05:05:04', '2019-04-02 05:05:04'),
(4, 23, 1, 'Tempe Goreng  Mbak Rum', '-', 500, 'Tersedia', 0, 'product.png', '2019-04-02 05:06:14', '2019-04-09 23:31:28'),
(5, 23, 2, 'Es Teh  Mbak Rum', '-', 2000, 'Tersedia', 0, 'product.png', '2019-04-02 05:06:55', '2019-04-04 21:46:49'),
(6, 23, 2, 'Es Buah  Mbak Rum', 'Minuman dengan aneka buah-buahan segar', 8000, 'Tersedia', 0, 'product.png', '2019-04-02 05:07:31', '2019-04-11 08:20:27'),
(7, 23, 2, 'Es Jeruk  Mbak Rum', '-', 2500, 'Tersedia', 0, 'product.png', '2019-04-02 05:08:22', '2019-04-07 17:30:54'),
(8, 27, 1, 'Mie Ayam Pangsit  Mie Ayam San', 'Mie ayam dengan tambahan pangsit basah', 8000, 'Tersedia', 0, 'product.png', '2019-04-02 05:09:14', '2019-04-02 05:09:14'),
(9, 27, 1, 'Mie Ayam Goreng  Mie Ayam Sant', '-', 9000, 'Tersedia', 0, 'product.png', '2019-04-02 05:10:06', '2019-04-02 05:10:06'),
(10, 27, 1, 'Mie Ayam Jamur  Mie Ayam Santi', 'Mie ayam dengan tambahan jamur', 10000, 'Tersedia', 0, 'product.png', '2019-04-02 05:10:40', '2019-04-02 05:10:40'),
(11, 27, 2, 'Es Teh  Mie Ayam Santika', '-', 1500, 'Tersedia', 0, 'product.png', '2019-04-02 05:12:00', '2019-04-02 05:12:00'),
(12, 27, 2, 'Es Susu  Mie Ayam Santika', '-', 3000, 'Tersedia', 0, 'product.png', '2019-04-02 05:12:23', '2019-04-04 20:59:00'),
(13, 26, 1, 'Nasi Goreng Seafood  Kantin Bu Esti', 'Nasi goreng dengan toping seafood', 15000, 'Tersedia', 0, 'product.png', '2019-04-02 05:13:10', '2019-04-13 03:24:48'),
(14, 26, 1, 'Nasi Putih  Kantin Bu Esti', '-', 3000, 'Tersedia', 0, 'product.png', '2019-04-02 05:14:09', '2019-04-05 00:28:30'),
(15, 26, 1, 'Oseng-oseng  Kantin Bu Esti', '-', 2000, 'Tersedia', 0, 'product.png', '2019-04-02 05:15:47', '2019-04-02 05:15:47'),
(16, 26, 1, 'Ayam Goreng  Kantin Bu Esti', '-', 5000, 'Tersedia', 0, 'product.png', '2019-04-02 05:16:18', '2019-04-02 05:16:18'),
(17, 26, 2, 'Aqua Dingin  Kantin Bu Esti', '-', 2500, 'Tersedia', 0, 'product.png', '2019-04-02 05:16:46', '2019-04-13 03:24:48'),
(18, 26, 2, 'Es Teh  Kantin Bu Esti', '-', 2000, 'Tersedia', 0, 'product.png', '2019-04-02 05:17:06', '2019-04-02 05:24:28'),
(19, 26, 2, 'Es Goodday Coolin  Kantin Bu Esti', '-', 4000, 'Tersedia', 0, 'product.png', '2019-04-02 05:17:53', '2019-04-07 03:41:43'),
(20, 26, 1, 'Sate Bakso  Kantin Bu Esti', 'Sate bakso pedas', 1500, 'Tersedia', 0, 'product.png', '2019-04-02 05:18:29', '2019-04-09 23:31:28'),
(21, 21, 1, 'Tango Vanila  Toko Asli', 'Waffer renyah', 1000, 'Tersedia', 0, 'product.png', '2019-04-02 05:19:16', '2019-04-06 09:44:21'),
(22, 21, 1, 'Chitato Sapi Panggang 50gr  To', '-', 7500, 'Tersedia', 0, 'product.png', '2019-04-02 05:20:04', '2019-04-04 20:59:00'),
(23, 21, 2, 'Ultramilk Susu Vanila 600ml  T', '-', 8500, 'Tersedia', 0, 'product.png', '2019-04-02 05:20:52', '2019-04-02 05:50:00'),
(24, 26, 1, 'Nasi Kucing Bu Esti', 'Sambal teri', 2000, 'Tersedia', 0, 'product.png', '2019-04-04 17:24:54', '2019-04-04 17:24:54'),
(25, 26, 2, 'Es Pisang Ijo Bu Esti', '-', 6500, 'Tersedia', 0, 'product.png', '2019-04-04 17:24:54', '2019-04-05 00:28:13'),
(26, 21, 1, 'Silverquen Coklat Toko Asli', NULL, 8000, 'Tersedia', 0, 'product.png', '2019-04-04 17:38:15', '2019-04-04 17:38:15'),
(27, 21, 2, 'Kacang Pilus Garuda Toko Asli', NULL, 1500, 'Tersedia', 0, 'product.png', '2019-04-04 17:38:15', '2019-04-04 17:38:15'),
(28, 22, 1, 'Soto Lapangan  Tenda Biru', 'Soto enak khas tenda biru', 6000, 'Tersedia', 0, '', '2019-04-07 09:35:21', '2019-04-09 23:31:28'),
(33, 22, 1, 'Sate Usus Tenda Biru', 'Sambal teri', 2000, 'Tersedia', 0, 'product.png', '2019-04-07 07:32:42', '2019-04-07 07:32:42'),
(34, 22, 2, 'Jus Melon Tenda Biru', 'Dingin', 5500, 'Tersedia', 0, 'product.png', '2019-04-07 07:32:42', '2019-04-07 07:32:42'),
(35, 21, 1, 'Chocolatos 100 gr Toko Asli', 'Snack enak', 1500, 'Tersedia', 0, 'product.png', '2019-04-07 07:54:54', '2019-04-07 07:54:54'),
(36, 21, 2, 'Piatoz BBQ 50 gr Toko Asli', 'Krenyes', 2500, 'Tersedia', 0, 'product.png', '2019-04-07 07:54:54', '2019-04-07 07:54:54'),
(37, 31, 2, 'AQUA 1500ml  Kantin Guntur', '-', 5000, 'Tersedia', 0, 'product.png', '2019-04-08 04:02:48', '2019-04-08 04:08:45'),
(38, 31, 1, 'Kacang telur  Kantin Guntur', '-', 1000, 'Tersedia', 0, 'product.png', '2019-04-08 04:05:58', '2019-04-08 04:08:45');

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksi_konven`
--

CREATE TABLE `detail_transaksi_konven` (
  `id` int(10) UNSIGNED NOT NULL,
  `barang_konven_id` int(10) UNSIGNED NOT NULL,
  `transaksi_konven_id` int(10) UNSIGNED NOT NULL,
  `harga` int(10) UNSIGNED NOT NULL,
  `diskon` int(10) NOT NULL,
  `jumlah` int(10) UNSIGNED NOT NULL,
  `total` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_transaksi_konven`
--

INSERT INTO `detail_transaksi_konven` (`id`, `barang_konven_id`, `transaksi_konven_id`, `harga`, `diskon`, `jumlah`, `total`, `created_at`, `updated_at`) VALUES
(1, 13, 1, 15000, 0, 1, 15000, '2019-04-02 05:24:28', '2019-04-02 05:24:28'),
(2, 18, 1, 2000, 0, 1, 2000, '2019-04-02 05:24:28', '2019-04-02 05:24:28'),
(3, 2, 2, 17000, 0, 2, 34000, '2019-04-02 05:35:42', '2019-04-02 05:35:42'),
(4, 6, 2, 8000, 0, 1, 8000, '2019-04-02 05:35:43', '2019-04-02 05:35:43'),
(5, 7, 2, 2500, 0, 1, 2500, '2019-04-02 05:35:43', '2019-04-02 05:35:43'),
(6, 1, 3, 15000, 0, 1, 15000, '2019-04-02 05:36:52', '2019-04-02 05:36:52'),
(7, 6, 3, 8000, 0, 1, 8000, '2019-04-02 05:36:52', '2019-04-02 05:36:52'),
(8, 23, 4, 8500, 0, 2, 17000, '2019-04-02 05:49:59', '2019-04-02 05:49:59'),
(9, 2, 4, 17000, 0, 2, 34000, '2019-04-02 05:50:00', '2019-04-02 05:50:00'),
(10, 20, 4, 1500, 0, 1, 1500, '2019-04-02 05:50:00', '2019-04-02 05:50:00'),
(11, 4, 5, 500, 0, 5, 2500, '2019-04-04 20:58:59', '2019-04-04 20:58:59'),
(12, 12, 5, 3000, 0, 2, 6000, '2019-04-04 20:59:00', '2019-04-04 20:59:00'),
(13, 22, 5, 7500, 0, 3, 22500, '2019-04-04 20:59:00', '2019-04-04 20:59:00'),
(14, 1, 6, 15000, 0, 1, 15000, '2019-04-04 21:49:25', '2019-04-04 21:49:25'),
(15, 7, 6, 2500, 0, 1, 2500, '2019-04-04 21:49:25', '2019-04-04 21:49:25'),
(16, 2, 7, 17000, 0, 1, 17000, '2019-04-04 21:58:21', '2019-04-04 21:58:21'),
(17, 21, 8, 1000, 0, 2, 2000, '2019-04-06 09:44:21', '2019-04-06 09:44:21'),
(18, 6, 9, 8000, 0, 1, 8000, '2019-04-07 03:46:16', '2019-04-07 03:46:16'),
(19, 2, 10, 17000, 0, 2, 34000, '2019-04-07 17:19:10', '2019-04-07 17:19:10'),
(20, 7, 10, 2500, 0, 1, 2500, '2019-04-07 17:19:10', '2019-04-07 17:19:10'),
(21, 7, 11, 2500, 0, 1, 2500, '2019-04-07 17:30:53', '2019-04-07 17:30:53'),
(22, 37, 12, 5000, 0, 1, 5000, '2019-04-08 04:08:45', '2019-04-08 04:08:45'),
(23, 38, 12, 1000, 0, 1, 1000, '2019-04-08 04:08:45', '2019-04-08 04:08:45'),
(24, 20, 13, 1500, 0, 2, 3000, '2019-04-09 23:31:28', '2019-04-09 23:31:28'),
(25, 4, 13, 500, 0, 5, 2500, '2019-04-09 23:31:28', '2019-04-09 23:31:28'),
(26, 28, 13, 6000, 0, 1, 6000, '2019-04-09 23:31:28', '2019-04-09 23:31:28'),
(27, 1, 14, 15000, 0, 1, 15000, '2019-04-11 08:20:27', '2019-04-11 08:20:27'),
(28, 6, 14, 8000, 0, 1, 8000, '2019-04-11 08:20:27', '2019-04-11 08:20:27'),
(29, 13, 16, 15000, 0, 1, 15000, '2019-04-13 03:24:48', '2019-04-13 03:24:48'),
(30, 17, 16, 2500, 0, 1, 2500, '2019-04-13 03:24:48', '2019-04-13 03:24:48');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_pembayaran`
--

CREATE TABLE `jenis_pembayaran` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenis_pembayaran` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jenis_pembayaran`
--

INSERT INTO `jenis_pembayaran` (`id`, `jenis_pembayaran`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, 'Tunai', 'Pembayaran dengan uang tunai atau cash', '2019-01-16 17:00:00', '2019-03-12 21:56:36'),
(2, 'Gamapay', 'Pembayaran dengan scan QR Code android dari UGM', '2019-01-17 17:00:00', '2019-03-12 21:57:58');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(10) UNSIGNED NOT NULL,
  `kategori` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `kategori`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, 'Makanan', 'Semua jenis makanan enak', '2018-11-27 00:27:03', '2019-03-12 21:40:42'),
(2, 'Minuman', '', '2018-11-27 00:27:38', '2018-11-27 00:27:38');

-- --------------------------------------------------------

--
-- Table structure for table `keranjang`
--

CREATE TABLE `keranjang` (
  `id` int(10) UNSIGNED NOT NULL,
  `penjual_konven_id` int(10) UNSIGNED NOT NULL,
  `barang_konven_id` int(10) UNSIGNED NOT NULL,
  `kuantitas` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `log_saldo`
--

CREATE TABLE `log_saldo` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `reference_id` int(10) UNSIGNED NOT NULL,
  `source` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `saldo_awal` int(10) UNSIGNED NOT NULL,
  `type` enum('topup','pencairan','penjualan','pembelian') COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `grand_total` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `log_saldo`
--

INSERT INTO `log_saldo` (`id`, `user_id`, `reference_id`, `source`, `saldo_awal`, `type`, `total`, `grand_total`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 'App\\Topup', 0, 'topup', 10000, 10000, '2019-03-03 05:59:11', '2019-03-03 05:59:11'),
(2, 10, 2, 'App\\Topup', 0, 'topup', 25000, 25000, '2019-03-03 06:15:28', '2019-03-03 06:15:28'),
(5, 10, 5, 'App\\Topup', 0, 'topup', 10000, 10000, '2019-03-04 18:35:52', '2019-03-04 18:35:52'),
(6, 10, 6, 'App\\Topup', 10000, 'topup', 5000, 15000, '2019-03-04 18:37:11', '2019-03-04 18:37:11'),
(7, 10, 7, 'App\\Topup', 15000, 'topup', 1000, 16000, '2019-03-04 18:37:46', '2019-03-04 18:37:46'),
(8, 10, 8, 'App\\Topup', 16000, 'topup', 1000, 17000, '2019-03-09 08:12:55', '2019-03-09 08:12:55'),
(17, 10, 23, 'App\\TransaksiKonven', 17000, 'pembelian', 9500, 7500, '2019-03-18 00:10:48', '2019-03-18 00:10:48'),
(18, 4, 23, 'App\\TransaksiKonven', 0, 'penjualan', 9500, 9500, '2019-03-18 00:10:48', '2019-03-18 00:10:48'),
(19, 30, 1, 'App\\Pencairan', 1000000, 'pencairan', 50000, 950000, '2019-03-21 00:16:42', '2019-03-21 00:16:42'),
(20, 10, 9, 'App\\Topup', 7500, 'topup', 200000, 207500, '2019-03-24 06:56:43', '2019-03-24 06:56:43'),
(21, 13, 10, 'App\\Topup', 0, 'topup', 150000, 150000, '2019-03-24 07:01:59', '2019-03-24 07:01:59'),
(22, 13, 69, 'App\\TransaksiKonven', 150000, 'pembelian', 2000, 148000, '2019-03-24 07:07:16', '2019-03-24 07:07:16'),
(23, 12, 69, 'App\\TransaksiKonven', 0, 'penjualan', 2000, 2000, '2019-03-24 07:07:16', '2019-03-24 07:07:16'),
(24, 13, 68, 'App\\TransaksiKonven', 148000, 'pembelian', 2000, 146000, '2019-03-24 07:49:57', '2019-03-24 07:49:57'),
(25, 12, 68, 'App\\TransaksiKonven', 148000, 'penjualan', 2000, 150000, '2019-03-24 07:49:58', '2019-03-24 07:49:58'),
(26, 3, 11, 'App\\Topup', 0, 'topup', 100000, 100000, '2019-03-28 00:17:35', '2019-03-28 00:17:35'),
(27, 12, 2, 'App\\Pencairan', 150000, 'pencairan', 10000, 140000, '2019-04-07 09:06:44', '2019-04-07 09:06:44'),
(28, 10, 12, 'App\\Topup', 207500, 'topup', 50000, 257500, '2019-04-07 16:21:08', '2019-04-07 16:21:08'),
(29, 13, 14, 'App\\TransaksiKonven', 146000, 'pembelian', 23000, 123000, '2019-04-11 01:26:50', '2019-04-11 01:26:50'),
(30, 15, 14, 'App\\TransaksiKonven', 0, 'penjualan', 23000, 23000, '2019-04-11 01:26:50', '2019-04-11 01:26:50'),
(31, 10, 8, 'App\\TransaksiKonven', 257500, 'pembelian', 2000, 255500, '2019-04-12 19:26:06', '2019-04-12 19:26:06'),
(32, 4, 8, 'App\\TransaksiKonven', 7500, 'penjualan', 2000, 9500, '2019-04-12 19:26:06', '2019-04-12 19:26:06'),
(33, 10, 8, 'App\\TransaksiKonven', 255500, 'pembelian', 2000, 253500, '2019-04-12 19:30:13', '2019-04-12 19:30:13'),
(34, 4, 8, 'App\\TransaksiKonven', 9500, 'penjualan', 2000, 11500, '2019-04-12 19:30:13', '2019-04-12 19:30:13'),
(35, 10, 8, 'App\\TransaksiKonven', 255500, 'pembelian', 2000, 253500, '2019-04-12 19:34:21', '2019-04-12 19:34:21'),
(36, 4, 8, 'App\\TransaksiKonven', 11500, 'penjualan', 2000, 13500, '2019-04-12 19:34:21', '2019-04-12 19:34:21'),
(37, 10, 8, 'App\\TransaksiKonven', 255500, 'pembelian', 2000, 253500, '2019-04-12 19:35:39', '2019-04-12 19:35:39'),
(38, 4, 8, 'App\\TransaksiKonven', 13500, 'penjualan', 2000, 15500, '2019-04-12 19:35:39', '2019-04-12 19:35:39'),
(39, 10, 8, 'App\\TransaksiKonven', 255500, 'pembelian', 2000, 253500, '2019-04-12 19:37:32', '2019-04-12 19:37:32'),
(40, 4, 8, 'App\\TransaksiKonven', 15500, 'penjualan', 2000, 17500, '2019-04-12 19:37:32', '2019-04-12 19:37:32'),
(41, 10, 8, 'App\\TransaksiKonven', 255500, 'pembelian', 2000, 253500, '2019-04-12 19:40:51', '2019-04-12 19:40:51'),
(42, 4, 8, 'App\\TransaksiKonven', 17500, 'penjualan', 2000, 19500, '2019-04-12 19:40:51', '2019-04-12 19:40:51'),
(43, 10, 8, 'App\\TransaksiKonven', 255500, 'pembelian', 2000, 253500, '2019-04-12 19:44:47', '2019-04-12 19:44:47'),
(44, 4, 8, 'App\\TransaksiKonven', 19500, 'penjualan', 2000, 21500, '2019-04-12 19:44:47', '2019-04-12 19:44:47'),
(45, 10, 8, 'App\\TransaksiKonven', 255500, 'pembelian', 2000, 253500, '2019-04-12 19:52:45', '2019-04-12 19:52:45'),
(46, 4, 8, 'App\\TransaksiKonven', 21500, 'penjualan', 2000, 23500, '2019-04-12 19:52:45', '2019-04-12 19:52:45'),
(47, 10, 8, 'App\\TransaksiKonven', 255500, 'pembelian', 2000, 253500, '2019-04-12 19:54:25', '2019-04-12 19:54:25'),
(48, 4, 8, 'App\\TransaksiKonven', 23500, 'penjualan', 2000, 25500, '2019-04-12 19:54:25', '2019-04-12 19:54:25'),
(49, 10, 16, 'App\\TransaksiKonven', 255500, 'pembelian', 17500, 238000, '2019-04-12 20:34:43', '2019-04-12 20:34:43'),
(50, 33, 16, 'App\\TransaksiKonven', 0, 'penjualan', 17500, 17500, '2019-04-12 20:34:43', '2019-04-12 20:34:43');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_10_06_072913_create_penjual_table', 1),
(4, '2018_10_06_072927_create_pembeli_table', 1),
(5, '2018_10_28_064542_create_barang_jual_table', 1),
(6, '2018_10_28_065306_create_stok_barang_table', 1),
(7, '2018_10_28_065403_create_detail_transaksi_table', 1),
(8, '2018_10_28_071732_create_barang', 1),
(9, '2018_10_28_071802_create_status_barang', 1),
(10, '2018_10_28_071818_create_status_harian', 1),
(11, '2018_10_28_071845_create_kategori', 1),
(12, '2018_10_28_071901_create_jenis_pembayaran', 1),
(13, '2018_10_28_071928_create_transaksi', 1),
(14, '2018_10_28_072000_add_foreign_key_barang_jual', 1),
(15, '2018_10_28_072000_add_foreign_key_detail_transaksi', 1),
(16, '2018_10_28_072000_add_foreign_key_stok_barang', 1),
(17, '2018_10_28_073455_add_foreign_key_barang', 1),
(18, '2018_10_28_073541_add_foreign_key_transaksi', 1),
(19, '2018_11_06_073148_create_detail_transaksi_konven_table', 1),
(20, '2018_11_06_073206_create_transaksi_konven_table', 1),
(21, '2018_11_06_073225_create_barang_konven_table', 1),
(22, '2018_11_06_083954_add_foreign_key_penjual_table', 1),
(23, '2018_11_06_084035_add_foreign_key_pembeli_table', 1),
(24, '2018_11_06_084107_add_foreign_key_detail_transaksi_konven_table', 1),
(25, '2018_11_06_084124_add_foreign_key_transaksi_konven_table', 1),
(26, '2018_11_06_084138_add_foreign_key_barang_konven_table', 1),
(28, '2019_01_10_075422_create_penjual_konven_table', 2),
(34, '2019_01_10_083913_add_penjual_konven_id_field_to_barang_konven_table', 3),
(35, '2019_01_10_084618_add_penjual_konven_id_field_to_transaksi_konven_table', 3),
(36, '2019_03_03_073132_create_barang_table', 0),
(37, '2019_03_03_073132_create_barang_jual_table', 0),
(38, '2019_03_03_073132_create_barang_konven_table', 0),
(39, '2019_03_03_073132_create_detail_transaksi_table', 0),
(40, '2019_03_03_073132_create_detail_transaksi_konven_table', 0),
(41, '2019_03_03_073132_create_jenis_pembayaran_table', 0),
(42, '2019_03_03_073132_create_kategori_table', 0),
(43, '2019_03_03_073132_create_pembeli_table', 0),
(44, '2019_03_03_073132_create_penjual_table', 0),
(45, '2019_03_03_073132_create_penjual_konven_table', 0),
(46, '2019_03_03_073132_create_status_table', 0),
(47, '2019_03_03_073132_create_status_harian_table', 0),
(48, '2019_03_03_073132_create_stok_barang_table', 0),
(49, '2019_03_03_073132_create_transaksi_table', 0),
(50, '2019_03_03_073132_create_transaksi_konven_table', 0),
(51, '2019_03_03_073132_create_users_table', 0),
(52, '2019_03_03_073138_add_foreign_keys_to_barang_table', 0),
(53, '2019_03_03_073138_add_foreign_keys_to_barang_jual_table', 0),
(54, '2019_03_03_073138_add_foreign_keys_to_barang_konven_table', 0),
(55, '2019_03_03_073138_add_foreign_keys_to_detail_transaksi_table', 0),
(56, '2019_03_03_073138_add_foreign_keys_to_detail_transaksi_konven_table', 0),
(57, '2019_03_03_073138_add_foreign_keys_to_pembeli_table', 0),
(58, '2019_03_03_073138_add_foreign_keys_to_penjual_table', 0),
(59, '2019_03_03_073138_add_foreign_keys_to_penjual_konven_table', 0),
(60, '2019_03_03_073138_add_foreign_keys_to_stok_barang_table', 0),
(61, '2019_03_03_073138_add_foreign_keys_to_transaksi_table', 0),
(62, '2019_03_03_073138_add_foreign_keys_to_transaksi_konven_table', 0),
(63, '2019_02_27_011612_create_kasir_table', 4),
(64, '2019_02_27_012030_add_foreign_key_kasir_table', 4),
(65, '2019_03_03_074257_create_topup_table', 4),
(66, '2019_03_03_074324_create_log_saldo_table', 4),
(67, '2019_03_03_074435_add_foreign_key_topup', 4),
(68, '2019_03_03_074445_add_foreign_key_log_saldo', 4),
(69, '2016_06_01_000001_create_oauth_auth_codes_table', 5),
(70, '2016_06_01_000002_create_oauth_access_tokens_table', 5),
(71, '2016_06_01_000003_create_oauth_refresh_tokens_table', 5),
(72, '2016_06_01_000004_create_oauth_clients_table', 5),
(73, '2016_06_01_000005_create_oauth_personal_access_clients_table', 5),
(74, '2019_03_06_011213_create_bank_table', 6),
(75, '2019_03_12_003343_create_pencairan_table', 7),
(76, '2019_03_12_003421_add_foreign_key_pencairan_table', 7),
(77, '2019_03_13_051804_create_bank_table', 0),
(78, '2019_03_13_051804_create_barang_table', 0),
(79, '2019_03_13_051804_create_barang_jual_table', 0),
(80, '2019_03_13_051804_create_barang_konven_table', 0),
(81, '2019_03_13_051804_create_detail_transaksi_table', 0),
(82, '2019_03_13_051804_create_detail_transaksi_konven_table', 0),
(83, '2019_03_13_051804_create_jenis_pembayaran_table', 0),
(84, '2019_03_13_051804_create_kasir_table', 0),
(85, '2019_03_13_051804_create_kategori_table', 0),
(86, '2019_03_13_051804_create_log_saldo_table', 0),
(87, '2019_03_13_051804_create_oauth_access_tokens_table', 0),
(88, '2019_03_13_051804_create_oauth_auth_codes_table', 0),
(89, '2019_03_13_051804_create_oauth_clients_table', 0),
(90, '2019_03_13_051804_create_oauth_personal_access_clients_table', 0),
(91, '2019_03_13_051804_create_oauth_refresh_tokens_table', 0),
(92, '2019_03_13_051804_create_pembeli_table', 0),
(93, '2019_03_13_051804_create_pencairan_table', 0),
(94, '2019_03_13_051804_create_penjual_table', 0),
(95, '2019_03_13_051804_create_penjual_konven_table', 0),
(96, '2019_03_13_051804_create_status_table', 0),
(97, '2019_03_13_051804_create_status_harian_table', 0),
(98, '2019_03_13_051804_create_stok_barang_table', 0),
(99, '2019_03_13_051804_create_topup_table', 0),
(100, '2019_03_13_051804_create_transaksi_table', 0),
(101, '2019_03_13_051804_create_transaksi_konven_table', 0),
(102, '2019_03_13_051804_create_users_table', 0),
(103, '2019_03_13_051810_add_foreign_keys_to_barang_table', 0),
(104, '2019_03_13_051810_add_foreign_keys_to_barang_jual_table', 0),
(105, '2019_03_13_051810_add_foreign_keys_to_barang_konven_table', 0),
(106, '2019_03_13_051810_add_foreign_keys_to_detail_transaksi_table', 0),
(107, '2019_03_13_051810_add_foreign_keys_to_detail_transaksi_konven_table', 0),
(108, '2019_03_13_051810_add_foreign_keys_to_kasir_table', 0),
(109, '2019_03_13_051810_add_foreign_keys_to_log_saldo_table', 0),
(110, '2019_03_13_051810_add_foreign_keys_to_pembeli_table', 0),
(111, '2019_03_13_051810_add_foreign_keys_to_pencairan_table', 0),
(112, '2019_03_13_051810_add_foreign_keys_to_penjual_table', 0),
(113, '2019_03_13_051810_add_foreign_keys_to_penjual_konven_table', 0),
(114, '2019_03_13_051810_add_foreign_keys_to_stok_barang_table', 0),
(115, '2019_03_13_051810_add_foreign_keys_to_topup_table', 0),
(116, '2019_03_13_051810_add_foreign_keys_to_transaksi_table', 0),
(117, '2019_03_13_051810_add_foreign_keys_to_transaksi_konven_table', 0),
(118, '2019_03_13_052252_create_bank_table', 0),
(119, '2019_03_13_052252_create_barang_table', 0),
(120, '2019_03_13_052252_create_barang_jual_table', 0),
(121, '2019_03_13_052252_create_barang_konven_table', 0),
(122, '2019_03_13_052252_create_detail_transaksi_table', 0),
(123, '2019_03_13_052252_create_detail_transaksi_konven_table', 0),
(124, '2019_03_13_052252_create_jenis_pembayaran_table', 0),
(125, '2019_03_13_052252_create_kasir_table', 0),
(126, '2019_03_13_052252_create_kategori_table', 0),
(127, '2019_03_13_052252_create_log_saldo_table', 0),
(128, '2019_03_13_052252_create_oauth_access_tokens_table', 0),
(129, '2019_03_13_052252_create_oauth_auth_codes_table', 0),
(130, '2019_03_13_052252_create_oauth_clients_table', 0),
(131, '2019_03_13_052252_create_oauth_personal_access_clients_table', 0),
(132, '2019_03_13_052252_create_oauth_refresh_tokens_table', 0),
(133, '2019_03_13_052252_create_pembeli_table', 0),
(134, '2019_03_13_052252_create_pencairan_table', 0),
(135, '2019_03_13_052252_create_penjual_table', 0),
(136, '2019_03_13_052252_create_penjual_konven_table', 0),
(137, '2019_03_13_052252_create_status_table', 0),
(138, '2019_03_13_052252_create_status_harian_table', 0),
(139, '2019_03_13_052252_create_stok_barang_table', 0),
(140, '2019_03_13_052252_create_topup_table', 0),
(141, '2019_03_13_052252_create_transaksi_table', 0),
(142, '2019_03_13_052252_create_transaksi_konven_table', 0),
(143, '2019_03_13_052252_create_users_table', 0),
(144, '2019_03_13_052258_add_foreign_keys_to_barang_table', 0),
(145, '2019_03_13_052258_add_foreign_keys_to_barang_jual_table', 0),
(146, '2019_03_13_052258_add_foreign_keys_to_barang_konven_table', 0),
(147, '2019_03_13_052258_add_foreign_keys_to_detail_transaksi_table', 0),
(148, '2019_03_13_052258_add_foreign_keys_to_detail_transaksi_konven_table', 0),
(149, '2019_03_13_052258_add_foreign_keys_to_kasir_table', 0),
(150, '2019_03_13_052258_add_foreign_keys_to_log_saldo_table', 0),
(151, '2019_03_13_052258_add_foreign_keys_to_pembeli_table', 0),
(152, '2019_03_13_052258_add_foreign_keys_to_pencairan_table', 0),
(153, '2019_03_13_052258_add_foreign_keys_to_penjual_table', 0),
(154, '2019_03_13_052258_add_foreign_keys_to_penjual_konven_table', 0),
(155, '2019_03_13_052258_add_foreign_keys_to_stok_barang_table', 0),
(156, '2019_03_13_052258_add_foreign_keys_to_topup_table', 0),
(157, '2019_03_13_052258_add_foreign_keys_to_transaksi_table', 0),
(158, '2019_03_13_052258_add_foreign_keys_to_transaksi_konven_table', 0),
(159, '2019_03_20_025711_create_keranjang_table', 8),
(160, '2019_03_20_025720_add_foreign_key_keranjang_table', 8),
(161, '2019_03_25_015526_create_bank_table', 0),
(162, '2019_03_25_015526_create_barang_table', 0),
(163, '2019_03_25_015526_create_barang_jual_table', 0),
(164, '2019_03_25_015526_create_barang_konven_table', 0),
(165, '2019_03_25_015526_create_detail_transaksi_table', 0),
(166, '2019_03_25_015526_create_detail_transaksi_konven_table', 0),
(167, '2019_03_25_015526_create_jenis_pembayaran_table', 0),
(168, '2019_03_25_015526_create_kategori_table', 0),
(169, '2019_03_25_015526_create_keranjang_table', 0),
(170, '2019_03_25_015526_create_log_saldo_table', 0),
(171, '2019_03_25_015526_create_oauth_access_tokens_table', 0),
(172, '2019_03_25_015526_create_oauth_auth_codes_table', 0),
(173, '2019_03_25_015526_create_oauth_clients_table', 0),
(174, '2019_03_25_015526_create_oauth_personal_access_clients_table', 0),
(175, '2019_03_25_015526_create_oauth_refresh_tokens_table', 0),
(176, '2019_03_25_015526_create_pembeli_table', 0),
(177, '2019_03_25_015526_create_pencairan_table', 0),
(178, '2019_03_25_015526_create_penjual_table', 0),
(179, '2019_03_25_015526_create_penjual_konven_table', 0),
(180, '2019_03_25_015526_create_status_table', 0),
(181, '2019_03_25_015526_create_status_harian_table', 0),
(182, '2019_03_25_015526_create_stok_barang_table', 0),
(183, '2019_03_25_015526_create_topup_table', 0),
(184, '2019_03_25_015526_create_transaksi_table', 0),
(185, '2019_03_25_015526_create_transaksi_konven_table', 0),
(186, '2019_03_25_015526_create_users_table', 0),
(187, '2019_03_25_015551_add_foreign_keys_to_barang_table', 0),
(188, '2019_03_25_015551_add_foreign_keys_to_barang_jual_table', 0),
(189, '2019_03_25_015551_add_foreign_keys_to_barang_konven_table', 0),
(190, '2019_03_25_015551_add_foreign_keys_to_detail_transaksi_table', 0),
(191, '2019_03_25_015551_add_foreign_keys_to_detail_transaksi_konven_table', 0),
(192, '2019_03_25_015551_add_foreign_keys_to_keranjang_table', 0),
(193, '2019_03_25_015551_add_foreign_keys_to_log_saldo_table', 0),
(194, '2019_03_25_015551_add_foreign_keys_to_pembeli_table', 0),
(195, '2019_03_25_015551_add_foreign_keys_to_pencairan_table', 0),
(196, '2019_03_25_015551_add_foreign_keys_to_penjual_table', 0),
(197, '2019_03_25_015551_add_foreign_keys_to_penjual_konven_table', 0),
(198, '2019_03_25_015551_add_foreign_keys_to_stok_barang_table', 0),
(199, '2019_03_25_015551_add_foreign_keys_to_topup_table', 0),
(200, '2019_03_25_015551_add_foreign_keys_to_transaksi_table', 0),
(201, '2019_03_25_015551_add_foreign_keys_to_transaksi_konven_table', 0),
(202, '2019_04_02_203832_create_bank_table', 0),
(203, '2019_04_02_203832_create_barang_table', 0),
(204, '2019_04_02_203832_create_barang_jual_table', 0),
(205, '2019_04_02_203832_create_barang_konven_table', 0),
(206, '2019_04_02_203832_create_detail_transaksi_table', 0),
(207, '2019_04_02_203832_create_detail_transaksi_konven_table', 0),
(208, '2019_04_02_203832_create_jenis_pembayaran_table', 0),
(209, '2019_04_02_203832_create_kategori_table', 0),
(210, '2019_04_02_203832_create_keranjang_table', 0),
(211, '2019_04_02_203832_create_log_saldo_table', 0),
(212, '2019_04_02_203832_create_oauth_access_tokens_table', 0),
(213, '2019_04_02_203832_create_oauth_auth_codes_table', 0),
(214, '2019_04_02_203832_create_oauth_clients_table', 0),
(215, '2019_04_02_203832_create_oauth_personal_access_clients_table', 0),
(216, '2019_04_02_203832_create_oauth_refresh_tokens_table', 0),
(217, '2019_04_02_203832_create_pembeli_table', 0),
(218, '2019_04_02_203832_create_pencairan_table', 0),
(219, '2019_04_02_203832_create_penjual_table', 0),
(220, '2019_04_02_203832_create_penjual_konven_table', 0),
(221, '2019_04_02_203832_create_status_table', 0),
(222, '2019_04_02_203832_create_status_harian_table', 0),
(223, '2019_04_02_203832_create_stok_barang_table', 0),
(224, '2019_04_02_203832_create_topup_table', 0),
(225, '2019_04_02_203832_create_transaksi_table', 0),
(226, '2019_04_02_203832_create_transaksi_konven_table', 0),
(227, '2019_04_02_203832_create_users_table', 0),
(228, '2019_04_02_203840_add_foreign_keys_to_barang_table', 0),
(229, '2019_04_02_203840_add_foreign_keys_to_barang_jual_table', 0),
(230, '2019_04_02_203840_add_foreign_keys_to_barang_konven_table', 0),
(231, '2019_04_02_203840_add_foreign_keys_to_detail_transaksi_table', 0),
(232, '2019_04_02_203840_add_foreign_keys_to_detail_transaksi_konven_table', 0),
(233, '2019_04_02_203840_add_foreign_keys_to_keranjang_table', 0),
(234, '2019_04_02_203840_add_foreign_keys_to_log_saldo_table', 0),
(235, '2019_04_02_203840_add_foreign_keys_to_pembeli_table', 0),
(236, '2019_04_02_203840_add_foreign_keys_to_pencairan_table', 0),
(237, '2019_04_02_203840_add_foreign_keys_to_penjual_table', 0),
(238, '2019_04_02_203840_add_foreign_keys_to_penjual_konven_table', 0),
(239, '2019_04_02_203840_add_foreign_keys_to_stok_barang_table', 0),
(240, '2019_04_02_203840_add_foreign_keys_to_topup_table', 0),
(241, '2019_04_02_203840_add_foreign_keys_to_transaksi_table', 0),
(242, '2019_04_02_203840_add_foreign_keys_to_transaksi_konven_table', 0),
(243, '2019_04_02_204641_create_bank_table', 0),
(244, '2019_04_02_204641_create_barang_konven_table', 0),
(245, '2019_04_02_204641_create_detail_transaksi_konven_table', 0),
(246, '2019_04_02_204641_create_jenis_pembayaran_table', 0),
(247, '2019_04_02_204641_create_kategori_table', 0),
(248, '2019_04_02_204641_create_keranjang_table', 0),
(249, '2019_04_02_204641_create_log_saldo_table', 0),
(250, '2019_04_02_204641_create_oauth_access_tokens_table', 0),
(251, '2019_04_02_204641_create_oauth_auth_codes_table', 0),
(252, '2019_04_02_204641_create_oauth_clients_table', 0),
(253, '2019_04_02_204641_create_oauth_personal_access_clients_table', 0),
(254, '2019_04_02_204641_create_oauth_refresh_tokens_table', 0),
(255, '2019_04_02_204641_create_pembeli_table', 0),
(256, '2019_04_02_204641_create_pencairan_table', 0),
(257, '2019_04_02_204641_create_penjual_konven_table', 0),
(258, '2019_04_02_204641_create_topup_table', 0),
(259, '2019_04_02_204641_create_transaksi_konven_table', 0),
(260, '2019_04_02_204641_create_users_table', 0),
(261, '2019_04_02_204645_add_foreign_keys_to_barang_konven_table', 0),
(262, '2019_04_02_204645_add_foreign_keys_to_detail_transaksi_konven_table', 0),
(263, '2019_04_02_204645_add_foreign_keys_to_keranjang_table', 0),
(264, '2019_04_02_204645_add_foreign_keys_to_log_saldo_table', 0),
(265, '2019_04_02_204645_add_foreign_keys_to_pembeli_table', 0),
(266, '2019_04_02_204645_add_foreign_keys_to_pencairan_table', 0),
(267, '2019_04_02_204645_add_foreign_keys_to_penjual_konven_table', 0),
(268, '2019_04_02_204645_add_foreign_keys_to_topup_table', 0),
(269, '2019_04_02_204645_add_foreign_keys_to_transaksi_konven_table', 0),
(270, '2019_04_06_174500_create_bank_table', 0),
(271, '2019_04_06_174500_create_barang_konven_table', 0),
(272, '2019_04_06_174500_create_detail_transaksi_konven_table', 0),
(273, '2019_04_06_174500_create_jenis_pembayaran_table', 0),
(274, '2019_04_06_174500_create_kategori_table', 0),
(275, '2019_04_06_174500_create_keranjang_table', 0),
(276, '2019_04_06_174500_create_log_saldo_table', 0),
(277, '2019_04_06_174500_create_oauth_access_tokens_table', 0),
(278, '2019_04_06_174500_create_oauth_auth_codes_table', 0),
(279, '2019_04_06_174500_create_oauth_clients_table', 0),
(280, '2019_04_06_174500_create_oauth_personal_access_clients_table', 0),
(281, '2019_04_06_174500_create_oauth_refresh_tokens_table', 0),
(282, '2019_04_06_174500_create_pembeli_table', 0),
(283, '2019_04_06_174500_create_pencairan_table', 0),
(284, '2019_04_06_174500_create_penjual_konven_table', 0),
(285, '2019_04_06_174500_create_topup_table', 0),
(286, '2019_04_06_174500_create_transaksi_konven_table', 0),
(287, '2019_04_06_174500_create_users_table', 0),
(288, '2019_04_06_174506_add_foreign_keys_to_barang_konven_table', 0),
(289, '2019_04_06_174506_add_foreign_keys_to_detail_transaksi_konven_table', 0),
(290, '2019_04_06_174506_add_foreign_keys_to_keranjang_table', 0),
(291, '2019_04_06_174506_add_foreign_keys_to_log_saldo_table', 0),
(292, '2019_04_06_174506_add_foreign_keys_to_pembeli_table', 0),
(293, '2019_04_06_174506_add_foreign_keys_to_pencairan_table', 0),
(294, '2019_04_06_174506_add_foreign_keys_to_penjual_konven_table', 0),
(295, '2019_04_06_174506_add_foreign_keys_to_topup_table', 0),
(296, '2019_04_06_174506_add_foreign_keys_to_transaksi_konven_table', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('100dc778cf346a4d4bfcd3ed0d19ca53518a5d811f7b17870df70cc79167f6875a8a65ecf2d98976', 10, 1, 'MyApp', '[]', 0, '2019-04-12 19:22:53', '2019-04-12 19:22:53', '2020-04-13 02:22:53'),
('12994614e2e449648db194f217e375da64c34bb020fe0c189ae39e45a1b926eea1dc331778b819ae', 3, 1, 'MyApp', '[]', 0, '2019-04-14 19:53:33', '2019-04-14 19:53:33', '2020-04-15 02:53:33'),
('1d7dbaa067e24876c81a360c86981109963a68adc9e4e5ad27db47bbf26173d713a88dcf0a1c52f8', 3, 1, 'MyApp', '[]', 0, '2019-04-14 19:50:55', '2019-04-14 19:50:55', '2020-04-15 02:50:55'),
('482ca97aa50b1651db1d82585c7e536e9ffffae6e0f0c9cc6d8e0b2bcf0943e50423fb3907c8a689', 3, 1, 'MyApp', '[]', 0, '2019-04-14 20:00:32', '2019-04-14 20:00:32', '2020-04-15 03:00:32'),
('4bb21b54f662a6b3db700cf4f03139fd5b66a5b5445a16468f4ed3d5cff94385422f7ae52f2ed645', 3, 1, 'MyApp', '[]', 0, '2019-04-14 19:59:03', '2019-04-14 19:59:03', '2020-04-15 02:59:03'),
('51e732f5cc6363b7d302c6f53acbabc0a0c1038881259535db9cdbb257b1bd3c236c14211a962d32', 12, 1, 'MyApp', '[]', 0, '2019-03-19 18:56:10', '2019-03-19 18:56:10', '2020-03-20 01:56:10'),
('619f62637ee85574b6b614d4532d81ef6163c83f31ad325ac415ad59f2eafbfaf5e17a39c92bee85', 3, 1, 'MyApp', '[]', 0, '2019-04-14 19:50:04', '2019-04-14 19:50:04', '2020-04-15 02:50:04'),
('72d28771be26f1e0ef0569fa68887d8a57df1986055d803e101a5722cb6f94af46d0ebd75fd32bc6', 10, 1, 'MyApp', '[]', 0, '2019-03-17 23:48:55', '2019-03-17 23:48:55', '2020-03-18 06:48:55'),
('8695c3ce85c04cf8daeda22b85ba330acdc0c62b2ccadfe3945542a5729d28fe9b90add0f28e6b9e', 3, 1, 'MyApp', '[]', 0, '2019-04-14 19:56:33', '2019-04-14 19:56:33', '2020-04-15 02:56:33'),
('8709dbbcaf5b36a82d737bc9db73c622360b07cabc342ea8658d78b2fa3c8ce3c25207cb5864a93c', 13, 1, 'MyApp', '[]', 0, '2019-03-24 07:02:13', '2019-03-24 07:02:13', '2020-03-24 14:02:13'),
('8f9103fd7e31d03b3190246268be95b8a8badad7419e602208911af0ed961a45ec51b559647b0009', 12, 1, 'MyApp', '[]', 0, '2019-03-24 02:37:17', '2019-03-24 02:37:17', '2020-03-24 09:37:17'),
('9abe2186bc52447c029296e4954725dea293a0a1cf14741378374561c7de6bcded54fe6441c497cb', 4, 1, 'MyApp', '[]', 0, '2019-03-03 05:50:01', '2019-03-03 05:50:01', '2020-03-03 12:50:01'),
('a835be9cd66ce5cf77581b101df57b5fcb511c35140f3462674530497a572af057f0baf95bccc240', 3, 1, 'MyApp', '[]', 0, '2019-03-13 20:40:42', '2019-03-13 20:40:42', '2020-03-14 03:40:42'),
('b0d4d373f83c97698e5e761ccff585060c622360d579083526db4f1fc6a9ed27022ff4a53ca708ab', 10, 1, 'MyApp', '[]', 0, '2019-03-13 21:42:12', '2019-03-13 21:42:12', '2020-03-14 04:42:12'),
('b1ce6d993516fa8a0f09f48f6571cdc0f85249e1ff08f363cbf2f2d345ffce4becd548ede0d4ed36', 3, 1, 'MyApp', '[]', 0, '2019-03-13 21:48:14', '2019-03-13 21:48:14', '2020-03-14 04:48:14'),
('bf704c204b4e646b0153fc729a46338ea8b31cd74653eeb0bee1685e0a479aac01c92a74bed44098', 3, 1, 'MyApp', '[]', 0, '2019-03-17 22:54:24', '2019-03-17 22:54:24', '2020-03-18 05:54:24'),
('bf9251f13a72f3760cb5225d4869b655b168a9538d538bb3676fe500c50a0af605b4782b3c722c85', 10, 1, 'MyApp', '[]', 0, '2019-03-13 20:53:48', '2019-03-13 20:53:48', '2020-03-14 03:53:48'),
('debe91b09af533e132a770ae9c9d92324cbdeac78454035ffc0e61c4321daf8e097a2bdde155a903', 12, 1, 'MyApp', '[]', 0, '2019-03-21 02:07:42', '2019-03-21 02:07:42', '2020-03-21 09:07:42'),
('e1126c777a18cfe9399c1cecda3172fea1b91b8ce862c516f3338363c928ba9ecc6bd5271cb415f7', 13, 1, 'MyApp', '[]', 0, '2019-03-13 20:41:38', '2019-03-13 20:41:38', '2020-03-14 03:41:38'),
('e678c6512b59f2a2fa36783908255deee48507047f0590fa3032be938037b3c4ee0ccba5f1fac4f7', 13, 1, 'MyApp', '[]', 0, '2019-03-24 07:00:59', '2019-03-24 07:00:59', '2020-03-24 14:00:59'),
('ec366166ee50c12edfef5ebcb70a22513eba8221e803b0acf1b1ca970a7f61735ca2b05e19db3033', 12, 1, 'MyApp', '[]', 0, '2019-03-03 05:53:05', '2019-03-03 05:53:05', '2020-03-03 12:53:05'),
('f029a845c2fe944c8a050748e537112d5b68ae2bc011a29b2128a30df6317ab452d4be9cacee0981', 3, 1, 'MyApp', '[]', 0, '2019-04-14 19:49:42', '2019-04-14 19:49:42', '2020-04-15 02:49:42');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'zt7SCPU1ArbcZip2nw51XxXzbe0R7F9J0oDboMYn', 'http://localhost', 1, 0, 0, '2019-03-03 01:18:57', '2019-03-03 01:18:57'),
(2, NULL, 'Laravel Password Grant Client', 'mJp9KPflJLZd5tSmzxWapol8SX7QZGx6p5PvBtj6', 'http://localhost', 0, 1, 0, '2019-03-03 01:18:58', '2019-03-03 01:18:58');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-03-03 01:18:58', '2019-03-03 01:18:58');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli`
--

CREATE TABLE `pembeli` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member` enum('Ya','Tidak') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pembeli`
--

INSERT INTO `pembeli` (`id`, `user_id`, `nama`, `member`, `created_at`, `updated_at`) VALUES
(1, 3, 'Layndo', 'Ya', '2019-01-16 17:00:00', NULL),
(2, 10, 'Prayeka', 'Ya', '2019-02-25 23:33:14', '2019-03-09 07:30:09'),
(3, 13, 'Pratomo', 'Ya', '2019-02-27 20:14:13', '2019-04-07 06:58:58'),
(7, 14, 'Susilo', 'Ya', '2019-02-28 04:39:14', '2019-02-28 04:39:14'),
(8, 26, 'Imam', 'Ya', '2019-03-05 06:27:34', '2019-03-11 17:22:09'),
(9, 19, 'lita', 'Ya', '2019-03-09 05:40:02', '2019-03-09 05:40:02'),
(10, 20, 'paijo', 'Ya', '2019-03-11 01:18:41', '2019-03-11 01:18:41'),
(11, 14, 'Yuma', 'Tidak', '2019-03-15 10:49:03', '2019-03-15 10:49:03'),
(12, 14, 'Robert', 'Tidak', '2019-03-15 10:51:04', '2019-03-15 10:51:04'),
(13, 14, 'Dhea', 'Tidak', '2019-03-20 08:25:51', '2019-03-20 08:25:51'),
(14, 14, 'Titi', 'Tidak', '2019-03-20 08:44:28', '2019-03-20 08:44:28'),
(15, 14, 'Steve', 'Tidak', '2019-03-24 17:30:06', '2019-03-24 17:30:06'),
(401012, 401001, 'Nurul Meika', 'Ya', '2019-04-07 06:27:51', '2019-04-07 06:27:51'),
(401013, 401002, 'Sri Rejeki', 'Ya', '2019-04-07 06:27:51', '2019-04-07 06:27:51'),
(401014, 401003, 'Siti', 'Ya', '2019-04-07 06:27:52', '2019-04-07 06:27:52'),
(401015, 401004, 'Jimmy', 'Ya', '2019-04-07 06:27:52', '2019-04-07 06:27:52'),
(401016, 401005, 'Afif Rahmat', 'Ya', '2019-04-07 06:27:53', '2019-04-07 06:27:53'),
(401017, 401006, 'Dhea Areta', 'Ya', '2019-04-07 06:47:47', '2019-04-07 06:47:47'),
(401019, 401009, 'Fatimah Putri', 'Ya', '2019-04-07 08:17:10', '2019-04-07 08:17:10'),
(401020, 401123, 'Fikri Ahsan', 'Ya', '2019-04-07 08:20:43', '2019-04-07 08:20:43'),
(401021, 401125, 'joko', 'Ya', '2019-04-08 04:28:47', '2019-04-08 04:28:47');

-- --------------------------------------------------------

--
-- Table structure for table `pencairan`
--

CREATE TABLE `pencairan` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `total` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `bank_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pencairan`
--

INSERT INTO `pencairan` (`id`, `user_id`, `total`, `bank_id`, `created_at`, `updated_at`) VALUES
(2, 12, 10000, 5, '2019-04-07 09:06:43', '2019-04-07 09:06:43');

-- --------------------------------------------------------

--
-- Table structure for table `penjual_konven`
--

CREATE TABLE `penjual_konven` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `nama_toko` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_rek` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `penjual_konven`
--

INSERT INTO `penjual_konven` (`id`, `user_id`, `nama_toko`, `no_rek`, `bank_id`, `created_at`, `updated_at`) VALUES
(21, 4, 'Toko Asli', '8899776655', 5, '2019-01-10 01:00:00', '2019-04-22 02:23:42'),
(22, 12, 'Tenda Biru', '9900887766', 5, '2019-02-26 21:23:22', '2019-04-22 02:12:41'),
(23, 15, 'Mbak Rum', '', 5, '2019-03-09 02:52:52', '2019-03-09 02:52:52'),
(24, 29, 'Lalala Chicken', '', 5, '2019-03-15 02:37:18', '2019-03-15 02:37:18'),
(25, 30, 'ayamamin', '', 5, '2019-03-20 01:15:50', '2019-03-20 01:15:50'),
(26, 31, 'Kantin Bu Esti', '', 5, '2019-03-21 00:44:07', '2019-03-21 00:44:07'),
(27, 32, 'Mie Ayam Santika', '', 5, '2019-03-21 01:27:25', '2019-03-21 01:27:25'),
(28, 33, 'Kasir GAMAPAY', '', 5, '2019-04-01 17:00:00', '2019-04-01 17:00:00'),
(30, 401007, 'Warung Inyoeng', '', 5, '2019-04-07 05:24:39', '2019-04-07 05:24:39'),
(31, 401124, 'Kantin Guntur', '', 5, '2019-04-08 03:57:20', '2019-04-08 03:57:20');

-- --------------------------------------------------------

--
-- Table structure for table `topup`
--

CREATE TABLE `topup` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `total` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `topup`
--

INSERT INTO `topup` (`id`, `user_id`, `total`, `created_at`, `updated_at`) VALUES
(1, 3, 10000, '2019-03-03 05:59:11', '2019-03-03 05:59:11'),
(2, 10, 25000, '2019-03-03 06:15:28', '2019-03-03 06:15:28'),
(5, 10, 10000, '2019-03-04 18:35:52', '2019-03-04 18:35:52'),
(6, 10, 5000, '2019-03-04 18:37:11', '2019-03-04 18:37:11'),
(7, 10, 1000, '2019-03-04 18:37:46', '2019-03-04 18:37:46'),
(8, 10, 1000, '2019-03-09 08:12:55', '2019-03-09 08:12:55'),
(9, 10, 200000, '2019-03-24 06:56:43', '2019-03-24 06:56:43'),
(10, 13, 150000, '2019-03-24 07:01:59', '2019-03-24 07:01:59'),
(11, 3, 100000, '2019-03-28 00:17:35', '2019-03-28 00:17:35'),
(12, 10, 50000, '2019-04-07 16:21:07', '2019-04-07 16:21:07');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_konven`
--

CREATE TABLE `transaksi_konven` (
  `id` int(10) UNSIGNED NOT NULL,
  `penjual_konven_id` int(10) UNSIGNED NOT NULL,
  `pembeli_id` int(10) UNSIGNED NOT NULL,
  `jenis_pembayaran_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diskon` int(10) DEFAULT NULL,
  `total` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaksi_konven`
--

INSERT INTO `transaksi_konven` (`id`, `penjual_konven_id`, `pembeli_id`, `jenis_pembayaran_id`, `status`, `diskon`, `total`, `created_at`, `updated_at`) VALUES
(1, 26, 3, 1, 'Lunas', 0, 17000, '2019-04-02 05:24:28', '2019-04-02 05:24:28'),
(2, 23, 1, 1, 'Lunas', 0, 44500, '2019-04-02 05:35:42', '2019-04-02 05:35:42'),
(3, 23, 8, 1, 'Lunas', 0, 23000, '2019-04-02 05:36:52', '2019-04-02 05:36:52'),
(4, 28, 2, 1, 'Lunas', 0, 52500, '2019-04-02 05:49:59', '2019-04-02 05:49:59'),
(5, 28, 9, 1, 'Lunas', 0, 31000, '2019-04-04 20:58:59', '2019-04-04 20:58:59'),
(6, 23, 15, 1, 'Lunas', 0, 17500, '2019-04-04 21:49:25', '2019-04-04 21:49:25'),
(7, 23, 3, 1, 'Lunas', 0, 17000, '2019-04-04 21:58:21', '2019-04-04 21:58:21'),
(8, 21, 1, 2, 'Lunas', 0, 2000, '2019-04-06 09:44:21', '2019-04-12 19:54:25'),
(9, 23, 15, 1, 'Lunas', 0, 8000, '2019-04-07 03:46:16', '2019-04-07 03:46:16'),
(10, 23, 2, 2, 'Menunggu', 0, 36500, '2019-04-07 17:19:10', '2019-04-07 17:19:10'),
(11, 23, 8, 1, 'Lunas', 0, 2500, '2019-04-07 17:30:53', '2019-04-07 17:30:53'),
(12, 31, 2, 1, 'Lunas', 0, 6000, '2019-04-08 04:08:45', '2019-04-08 04:08:45'),
(13, 28, 3, 1, 'Lunas', 0, 11500, '2019-04-09 23:31:28', '2019-04-09 23:31:28'),
(14, 23, 1, 2, 'Lunas', 0, 23000, '2019-04-11 08:20:27', '2019-04-11 01:26:50'),
(16, 28, 2, 2, 'Lunas', 0, 17500, '2019-04-13 03:24:48', '2019-04-12 20:34:43');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `level` enum('Admin','Penjual','Pembeli','Kasir') COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telepon` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `foto` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'avatar.png',
  `saldo` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `status_id` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `level`, `name`, `email`, `email_verified_at`, `password`, `no_telepon`, `tanggal_lahir`, `foto`, `saldo`, `status_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Admin', 'admin@gmail.com', '2018-11-25 20:06:10', '$2y$10$J98mh1FlCScDufzKK61u7eo2j7REi5EkX56SNaQXyQ8EYncCY3Rey', '823456781', '2018-11-01', 'avatar.png', 0, 2, '6KZLnxySD63D2umcZvu4MnPacVokzSApCej9YdssnHv4GtjEDWBog7Q1V2si', NULL, '2019-04-14 10:05:44'),
(2, 'Kasir', 'Kasir', 'kasir@gmail.com', '2019-01-13 19:12:12', '$2y$10$J98mh1FlCScDufzKK61u7eo2j7REi5EkX56SNaQXyQ8EYncCY3Rey', '812345668', '2017-11-15', 'avatar.png', 0, 2, 'ALkyuhTy5AhvuKYT4vEjIiu7YnVTXANY9Nl098gDjEk5788OMwDwU3oX7Xct', '2019-01-14 21:08:00', '2019-04-21 04:39:27'),
(3, 'Pembeli', 'Layndo', 'layndo@gmail.com', '2019-01-16 23:00:00', '$2y$10$HbcqqE8unVmXvAMYmKz/fuve0RI8cqWYhG6uDH8/xIIOdwrCPEvly', '898765', '2018-10-10', 'Layndo.jpg', 100000, 2, NULL, '2019-01-16 17:00:00', '2019-04-15 02:53:13'),
(4, 'Penjual', 'Toko Asli', 'asli@gmail.com', '2019-01-19 23:14:00', '$2y$10$fh3L7itkriEZNdWsPVCxj.P9EXLVXt8lIh.mBdHY4smMIjVX3Ij2m', '081903777654', '2019-01-01', 'avatar.png', 25500, 1, 'omnGzps9J5oCYiT5Ak2sy4PNwLCFhjipzKMtgS6TGcrFSuQeNiHpVeX1QzUq', '2019-01-18 23:00:00', '2019-04-22 02:24:53'),
(10, 'Pembeli', 'Prayeka', 'adel@gmail.com', NULL, '$2y$10$4sUOeRmTrRS2ceecPaPhke7mo6RQiSJC5PFlnf8azRXtbSoa92g3y', '89283716', '2019-01-27', 'avatar.png', 238000, 2, NULL, '2019-02-25 23:33:13', '2019-04-12 20:34:43'),
(12, 'Penjual', 'Tenda Biru', 'tendabiru@gmail.com', NULL, '$2y$10$gNG/2TvW4Xi9UBOD3gXyGeLHA9k12HIhxxX0Tu39BaOqQPGXIcP1i', '82345678', '2019-02-05', 'avatar.png', 140000, 2, 'nC7nyQeCChKjY9V0laOrlgsj2jzmD1P0AWbf4Vn6rlQz7GrottTsUl16m2cL', '2019-02-26 21:23:22', '2019-04-22 02:12:41'),
(13, 'Pembeli', 'Pratomo', 'pratomo@gmail.com', NULL, '$2y$10$jndPXoSPBfR/BBBf7IDGDuPef5cPVuBfN.8rvziLfUyM29ARe.7d6', '85672345', '2019-01-01', 'avatar.png', 123000, 2, NULL, '2019-02-27 20:14:13', '2019-04-11 01:26:50'),
(14, 'Pembeli', 'Pembeli', 'pembeli@gmail.com', NULL, '$2y$10$kseyC/vMjGTeP/NGVjhJvOHneVCqf2drrP.iGFOTkJVjHBdr1EgAO', '12345678', '2019-02-28', 'avatar.png', 0, 2, NULL, '2019-02-27 21:16:36', '2019-02-27 21:16:36'),
(15, 'Penjual', 'Mbak Rum', 'mbakrum@gmail.com', NULL, '$2y$10$J98mh1FlCScDufzKK61u7eo2j7REi5EkX56SNaQXyQ8EYncCY3Rey', '81234567', '2019-02-25', 'avatar.png', 23000, 2, 'kC0lRmQap9dLcO6D2zdz2eY7rguJt3atbGH0cMduUlZNVdVKMZbSvOE0oIMZ', '2019-03-09 02:52:52', '2019-04-11 01:26:50'),
(19, 'Pembeli', 'lita', 'lita@gmail.com', NULL, '$2y$10$OjiJb29.SnlEuHFIYq394OebOQO7soE5ePP16WmeDgKzyYSdxnlxq', '876578912', '2018-11-27', 'avatar.png', 35000, 2, NULL, '2019-03-09 05:40:02', '2019-03-09 08:14:25'),
(20, 'Pembeli', 'paijo', 'paijo@gmail.com', NULL, '$2y$10$RcY/IqYyVicR2Ap1wYEHX..uJrybqgqKD9TnIPhgKzo5vVFnQ3bMa', '8192837', '2019-03-03', 'avatar.png', 0, 2, NULL, '2019-03-11 01:18:40', '2019-03-11 01:18:40'),
(26, 'Pembeli', 'Imam', 'imam@gmail.com', NULL, '$2y$10$msFSBNJC4a0RkpQECr6h0O3rbokLppVk5w.tCkFAZ8Pz5K/CBdjxK', '89765', '2019-03-10', 'avatar.png', 1500, 2, NULL, '2019-03-11 17:22:08', '2019-03-11 17:22:09'),
(29, 'Penjual', 'Lalala Chicken', 'larum999@gmail.com', NULL, '$2y$10$LsuOnhdG.Bhm9EfH/svT2uZoT2XQCTHqQMFZJamcl5uSdWjU1vTfO', '08972571757', '1990-01-01', 'avatar.png', 0, 2, 'DlpORi0TJwIk4NmkwSTY0C67O3YT0g1IhkDmvFCa2BtoJ67agb6fff4ljgG3', '2019-03-15 02:37:18', '2019-03-15 02:37:19'),
(30, 'Penjual', 'ayamamin', 'ayamamin@gmail.com', NULL, '$2y$10$tffmYUSE75.pQgAsiHzKFufeVfeMJtCuvEZ0rK7i3OTJTeIMo1G/S', '087757679700', '1998-12-25', 'avatar.png', 950000, 2, 'UZy2sFLN2fozpPM2mm9DmWPa0BK14uGibpoKXxpCKl0HpEgIEI8UUTZV4cNt', '2019-03-20 01:15:50', '2019-03-21 00:16:42'),
(31, 'Penjual', 'Kantin Bu Esti', 'esti@gmail.com', NULL, '$2y$10$36Uj4sPA4ICI6YWnhf0Az.d8zENVZjr/U9e0w8cnfZR6/Y4gF5L5S', '087838701988', '1974-05-09', 'avatar.png', 100000, 2, 'UvtZtye5bMHhLJa8YRS6qLZceuHe0XfuccRbxv4a6qAJdNklPu5R4jm8HIAF', '2019-03-21 00:44:06', '2019-03-21 00:44:06'),
(32, 'Penjual', 'Mie Ayam Santika', 'santika@gmail.com', NULL, '$2y$10$XA/TC6XKREw2/.XA6BcGReayapgjZBKZgdk.WfcYf81Pd1IscivSe', '087738962323', '2019-02-05', 'avatar.png', 150000, 2, 'ycWg7uTbg7HigPSPcWV4Xl9OcrhZ3x03l0vzDPtgvcBlTZUcHgv0bmht5uDf', '2019-03-21 01:27:25', '2019-03-21 01:27:25'),
(33, 'Penjual', 'Kasir GAMAPAY', 'penjual@gmail.com', '2019-03-31 17:00:00', '12345678', '081234567123', '2019-04-02', 'avatar.png', 17500, 1, NULL, '2019-04-01 17:00:00', '2019-04-12 20:34:43'),
(401001, 'Pembeli', 'Nurul Meika', 'nurul@gmail.com', NULL, '$2y$10$N6TVGoTXcEYZC4okmcmF/O0WCq2oTRe.qKhDCvuLIo8yKs1ZeTO6K', '081876123456', '1997-12-10', 'avatar.png', 10000, 2, NULL, '2019-04-07 06:27:51', '2019-04-07 06:27:51'),
(401002, 'Pembeli', 'Sri Rejeki', 'sri@gmail.com', NULL, '$2y$10$OafimE6I0rI3kjHH2s7iuOB0zpQJv96MCFEAhiBHhBMEE1cERCIei', '08123567810', '1997-12-11', 'avatar.png', 20000, 2, NULL, '2019-04-07 06:27:51', '2019-04-07 06:27:51'),
(401003, 'Pembeli', 'Siti', 'siti@gmail.com', NULL, '$2y$10$17GrOu6GmGF2CPslRro6weqaY1CjGhtIEtCDO7bk2Bn..WO1RTnYi', '087888777666', '1997-12-12', 'avatar.png', 30000, 2, NULL, '2019-04-07 06:27:51', '2019-04-07 06:27:51'),
(401004, 'Pembeli', 'Jimmy', 'jimmy@gmail.com', NULL, '$2y$10$ClCOr.pQ8vvGdbgvYmrvSuF5mLLT/sQjidMxOD6/dNX1IsnYn3QWG', '089889977665', '1997-12-13', 'avatar.png', 40000, 2, NULL, '2019-04-07 06:27:52', '2019-04-07 06:27:52'),
(401005, 'Pembeli', 'Afif Rahmat', 'afif@gmail.com', NULL, '$2y$10$9pRalVA4NLH4Ekb81OduHuURDjWZmOnWE462nOzwE6fUHjcpL3rsi', '081234123456', '1997-12-14', 'avatar.png', 50000, 2, NULL, '2019-04-07 06:27:52', '2019-04-07 06:27:52'),
(401006, 'Pembeli', 'Dhea Areta', 'areta@gmail.com', NULL, '$2y$10$Kdw.QqIh/W8wsz5KJu52r.wEV2sKzzRiDkub4oMy/SZEBu5yZARkG', '085678312571', '1998-01-01', 'avatar.png', 20000, 2, NULL, '2019-04-07 06:47:47', '2019-04-07 06:47:47'),
(401007, 'Penjual', 'Warung Inyoeng', 'inyoeng@gmail.com', NULL, '$2y$10$6Gutw.UHkc4Le6wVef0mP.wI/jxvkcbXQJbZwXT5E0lm5/1BleNwy', '087234678912', '2019-04-07', 'avatar.png', 10000, 2, NULL, '2019-04-07 05:24:39', '2019-04-07 05:24:39'),
(401009, 'Pembeli', 'Fatimah Putri', 'putri@gmail.com', NULL, '$2y$10$QhKoQLvn5F5P5OQmFUEBCeIyG/aaa3pT145a0E.3bjuze5P9WMs0q', '085678912659', '1999-06-03', 'avatar.png', 10000, 2, NULL, '2019-04-07 08:17:09', '2019-04-07 08:17:09'),
(401123, 'Pembeli', 'Fikri Ahsan', 'ahsan@gmail.com', NULL, '$2y$10$Piwb9h5WudG90XFtM14yF.Z3yvrTvQcqWYqfM5HrMy0uuK06l.51W', '085448312571', '1997-05-12', 'avatar.png', 20000, 2, NULL, '2019-04-07 08:20:43', '2019-04-07 08:20:43'),
(401124, 'Penjual', 'Kantin Guntur', 'guntur@gmail.com', NULL, '$2y$10$5aapLKa9KDD46uydSP8zceJvM7UL2Y9dlJYweBzcbZ.ZLw.RT4Tl.', '081340997694', '2019-04-08', 'avatar.png', 100000, 2, 'jd9LkuSQzX4VELRVn2x6gvk5Vqnh5OipyrYQl4FBCybXxS3uCa4IRsbTnzub', '2019-04-08 03:57:19', '2019-04-08 03:57:19'),
(401125, 'Pembeli', 'joko', 'joko@mail.com', NULL, '$2y$10$/8GXnYuz2FqFOktnN6537.3SpXgqt6BlyD9XsI/RkldmqNI8jHzZ.', '081456789012345', '2009-01-01', 'avatar.png', 50000, 2, NULL, '2019-04-08 04:28:47', '2019-04-08 04:28:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barang_konven`
--
ALTER TABLE `barang_konven`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang_konven_kategori_id_foreign` (`kategori_id`),
  ADD KEY `penjual_konven_id` (`penjual_konven_id`);

--
-- Indexes for table `detail_transaksi_konven`
--
ALTER TABLE `detail_transaksi_konven`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detail_transaksi_konven_barang_konven_id_foreign` (`barang_konven_id`),
  ADD KEY `detail_transaksi_konven_transaksi_konven_id_foreign` (`transaksi_konven_id`);

--
-- Indexes for table `jenis_pembayaran`
--
ALTER TABLE `jenis_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `keranjang_penjual_konven_id_foreign` (`penjual_konven_id`),
  ADD KEY `keranjang_barang_konven_id_foreign` (`barang_konven_id`);

--
-- Indexes for table `log_saldo`
--
ALTER TABLE `log_saldo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `log_saldo_user_id_foreign` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `pembeli`
--
ALTER TABLE `pembeli`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pembeli_user_id_foreign` (`user_id`);

--
-- Indexes for table `pencairan`
--
ALTER TABLE `pencairan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pencairan_user_id_foreign` (`user_id`),
  ADD KEY `pencairan_bank_id_foreign` (`bank_id`) USING BTREE;

--
-- Indexes for table `penjual_konven`
--
ALTER TABLE `penjual_konven`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `foreign_key_penjual_konven_bank` (`bank_id`) USING BTREE;

--
-- Indexes for table `topup`
--
ALTER TABLE `topup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `topup_user_id_foreign` (`user_id`);

--
-- Indexes for table `transaksi_konven`
--
ALTER TABLE `transaksi_konven`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transaksi_konven_pembeli_id_foreign` (`pembeli_id`),
  ADD KEY `transaksi_konven_jenis_pembayaran_id_foreign` (`jenis_pembayaran_id`),
  ADD KEY `penjual_konven_id` (`penjual_konven_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `barang_konven`
--
ALTER TABLE `barang_konven`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `detail_transaksi_konven`
--
ALTER TABLE `detail_transaksi_konven`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `jenis_pembayaran`
--
ALTER TABLE `jenis_pembayaran`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `keranjang`
--
ALTER TABLE `keranjang`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log_saldo`
--
ALTER TABLE `log_saldo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=297;
--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pembeli`
--
ALTER TABLE `pembeli`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=401022;
--
-- AUTO_INCREMENT for table `pencairan`
--
ALTER TABLE `pencairan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `penjual_konven`
--
ALTER TABLE `penjual_konven`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `topup`
--
ALTER TABLE `topup`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `transaksi_konven`
--
ALTER TABLE `transaksi_konven`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=401126;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `barang_konven`
--
ALTER TABLE `barang_konven`
  ADD CONSTRAINT `barang_konven_ibfk_1` FOREIGN KEY (`penjual_konven_id`) REFERENCES `penjual_konven` (`id`),
  ADD CONSTRAINT `barang_konven_kategori_id_foreign` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`),
  ADD CONSTRAINT `barang_konven_penjual_konven_id_foreign` FOREIGN KEY (`penjual_konven_id`) REFERENCES `penjual_konven` (`id`);

--
-- Constraints for table `detail_transaksi_konven`
--
ALTER TABLE `detail_transaksi_konven`
  ADD CONSTRAINT `detail_transaksi_konven_barang_konven_id_foreign` FOREIGN KEY (`barang_konven_id`) REFERENCES `barang_konven` (`id`),
  ADD CONSTRAINT `detail_transaksi_konven_transaksi_konven_id_foreign` FOREIGN KEY (`transaksi_konven_id`) REFERENCES `transaksi_konven` (`id`);

--
-- Constraints for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD CONSTRAINT `keranjang_barang_konven_id_foreign` FOREIGN KEY (`barang_konven_id`) REFERENCES `barang_konven` (`id`),
  ADD CONSTRAINT `keranjang_penjual_konven_id_foreign` FOREIGN KEY (`penjual_konven_id`) REFERENCES `penjual_konven` (`id`);

--
-- Constraints for table `log_saldo`
--
ALTER TABLE `log_saldo`
  ADD CONSTRAINT `log_saldo_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `pembeli`
--
ALTER TABLE `pembeli`
  ADD CONSTRAINT `pembeli_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `pencairan`
--
ALTER TABLE `pencairan`
  ADD CONSTRAINT `pencairan_ibfk_1` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`),
  ADD CONSTRAINT `pencairan_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `penjual_konven`
--
ALTER TABLE `penjual_konven`
  ADD CONSTRAINT `penjual_konven_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `penjual_konven_ibfk_2` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`);

--
-- Constraints for table `topup`
--
ALTER TABLE `topup`
  ADD CONSTRAINT `topup_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `transaksi_konven`
--
ALTER TABLE `transaksi_konven`
  ADD CONSTRAINT `transaksi_konven_ibfk_1` FOREIGN KEY (`penjual_konven_id`) REFERENCES `penjual_konven` (`id`),
  ADD CONSTRAINT `transaksi_konven_jenis_pembayaran_id_foreign` FOREIGN KEY (`jenis_pembayaran_id`) REFERENCES `jenis_pembayaran` (`id`),
  ADD CONSTRAINT `transaksi_konven_pembeli_id_foreign` FOREIGN KEY (`pembeli_id`) REFERENCES `pembeli` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
