@extends('master')

@section('css')
@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Dashboard</a> /
  <a href="#">Data Master</a> /
  <a href="{{url('/master/barang')}}">Produk</a> /
  <a href="{{url('#')}}">Ubah</a>
@stop

@section('title')
  <h3>Data Master</h3>
@stop

@section('content')
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Ubah Produk</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">

            {!! Form::open(array('url'=>'master/barang_konven/edit/'.$data->id.'', 'method'=>'POST', 'class'=>'form-horizontal form-label-left', 'novalidate',  'enctype'=>'multipart/form-data', 'files'=>'true'))!!}

            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <img class="img-responsive avatar-view" src="{{asset('images/'.$data->foto.'')}}" alt="Avatar" title="Change Photos">
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Gambar
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="file" class="form-control" name="foto" placeholder="" value="{{old('foto')}}">
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Kategori <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <select class="form-control" name="kategori" class="form-control col-md-7 col-xs-12">
                    <option value="">Pilih Kategori</option>
                    @foreach($kategori as $value)
                        <option value="{{$value->id}}" {{collect(old('kategori'))->contains($value->id) ? 'selected':''}} @if($value->id == $data['kategori_id']) selected='selected' @endif>{{$value->kategori}}</option>
                    @endforeach
                </select>
                 @if ($errors->has('kategori'))
                  <span class="invalid-feedback" role="alert" style="color: red">
                      <strong>{{ $errors->first('kategori') }}</strong>
                  </span>
                  @endif
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Nama Produk <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control" placeholder="item name" name="nama" value="{{old('nama') ? old('nama') : $data->nama}}" type="text" class="form-control col-md-7 col-xs-12">
                 @if ($errors->has('nama'))
                  <span class="invalid-feedback" role="alert" style="color: red">
                      <strong>{{ $errors->first('nama') }}</strong>
                  </span>
                  @endif
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Deskripsi <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea class="form-control" placeholder="tulis deskripsi" name="deskripsi" value="" type="text" class="form-control col-md-7 col-xs-12">{{old('deskripsi') ? old('deskripsi') : $data->deskripsi}}</textarea>
                @if ($errors->has('deskripsi'))
                  <span class="invalid-feedback" role="alert" style="color: red">
                      <strong>{{ $errors->first('deskripsi') }}</strong>
                  </span>
                  @endif
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Harga <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control" placeholder="item name" name="harga" value="{{old('harga') ? old('harga') : $data->harga}}" type="text" class="form-control col-md-7 col-xs-12">
                @if ($errors->has('harga'))
                  <span class="invalid-feedback" role="alert" style="color: red">
                      <strong>{{ $errors->first('harga') }}</strong>
                  </span>
                  @endif
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Harga Beli<span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control" placeholder="item name" name="harga_beli" value="{{old('harga_beli') ? old('harga_beli') : $data->harga_beli}}" type="text" class="form-control col-md-7 col-xs-12">
                @if ($errors->has('harga_beli'))
                  <span class="invalid-feedback" role="alert" style="color: red">
                      <strong>{{ $errors->first('harga_beli') }}</strong>
                  </span>
                  @endif
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Status<span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <select class="form-control" name="stok" class="form-control col-md-7 col-xs-12">
                    <option value="Tersedia"  @if ($data->status=='Tersedia') selected="selected" @endif>Tersedia</option>
                    <option value="Habis" @if ($data->status=='Habis') selected="selected" @endif>Habis</option>
                </select>
                @if ($errors->has('stok'))
                  <span class="invalid-feedback" role="alert" style="color: red">
                      <strong>{{ $errors->first('stok') }}</strong>
                  </span>
                  @endif
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Diskon <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control" placeholder="item name" name="diskon" value="{{old('diskon') ? old('diskon') : $data->diskon}}" type="text" class="form-control col-md-7 col-xs-12">
                @if ($errors->has('diskon'))
                  <span class="invalid-feedback" role="alert" style="color: red">
                      <strong>{{ $errors->first('diskon') }}</strong>
                  </span>
                  @endif
              </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-md-offset-3">
                <a class="btn btn-primary" onclick="location.href='{{url('master/barang_konven')}}'">Batal</a>
                <button id="send" type="submit" class="btn btn-success">Simpan</button>
              </div>
            </div>
            {!!Form::close()!!}
          </form>
        </div>
      </div>
    </div>
  </div>
  
        <!-- row -->
@endsection

@section('javascript')
  <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>

@endsection
