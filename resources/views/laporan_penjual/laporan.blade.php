@section('js')

@stop
@extends('layout_pos')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Daftar Detail Transaksi
        <small>Gamapay</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-pencil"></i> Dartar Detail Transaksi</a></li>
        <li class="active">Gamapay</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div><br>
          
          @if(Session::has('success'))
            <div id="app" class="alert alert-success">
              {{ Session::get('success') }}
            </div>
          @elseif(Session::has('error'))
            <div class="alert alert-error">
              {{ Session::get('error') }}
            </div>
          @endif
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Daftar Detail Transaksi</h3>
                <div class="exportexcel pull-right"> 
                  <form action="{{ url('laporan_transaksi_konven') }}" method="get" class="form-inline" enctype="multipart/form-data">
                                
                          <div class="form-group">
                              <div class="input-group date">
                                <div class="input-group-addon">
                                  <span class="glyphicon glyphicon-th"></span>
                                </div>
                                  <input placeholder="Masukan tanggal awal" class="form-control datepicker" name="tgl_awal" value="{{\Carbon\Carbon::parse($requested->tgl_awal)->format('Y-m-d')}}">
                              </div>
                          </div>
                         <div class="form-group">
                              <div class="input-group date">
                                <div class="input-group-addon">
                                  <span class="glyphicon glyphicon-th"></span>
                                </div>
                                  <input placeholder="Masukan tanggal akhir" class="form-control datepicker" name="tgl_akhir" value="{{\Carbon\Carbon::parse($requested->tgl_akhir)->format('Y-m-d')}}">
                              </div>
                        </div>

                        <button type="submit" class="btn btn-info">Cari</button>
                 
                  </form>
                </div><br><br>
                <div class="exportexcel pull-right"> 
                  <form action="{{ url('laporan_transaksi_konven/export') }}" method="get" class="form-inline" enctype="multipart/form-data">
                    <input type="hidden" value="{{\Carbon\Carbon::parse($requested->tgl_awal)->format('Y-m-d')}}" name="tgl_awal">
                    <input type="hidden" value="{{\Carbon\Carbon::parse($requested->tgl_akhir)->format('Y-m-d')}}" name="tgl_akhir">
                    <button type="submit" class="fa fa-file-excel-o btn btn-success">Export Excel</button>            
                  </form>
                </div><br>
                
                <!-- <h3>{{$nama_toko}}</h3> -->
                <ul class="list-unstyled user_data">
                  <li><i class="fa fa-money user-profile-icon"> Total Pemasukan</i> Rp {{number_format($incomeTotal,0,".",".")}},-
                  </li>
                  <li><i class="fa fa-shopping-cart user-profile-icon"> Total Pengeluaran</i> Rp {{number_format($outcomeTotal,0,".",".")}},-
                  </li>
                  <li><i class="fa fa-money user-profile-icon"> Total Laba</i> Rp {{number_format($laba,0,".",".")}},-
                  </li>
                  <li><i class="fa fa-star user-profile-icon"> Produk Terjual</i> {{round($itemTotal,3)}} Produk
                  </li>
                </ul>

              </div>
              <!-- /.box-header -->
              <div class="box-body">                  
              

          <table id="example2" class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th class="col-md-1">No</th>
                <th>Barang</th>
                <th>Pembeli</th>
                <th>Harga</th>
                <th>Harga Beli</th>
                <th>Diskon</th>
                <th>Jumlah</th>
                <th class="col-md-2">Total</th>
                <th class="col-md-2">Total Harga Beli</th>
              </tr>
            </thead>
            <tbody>
              @foreach($list as $value => $pembeli)
              <tr class="item-{{$pembeli->id}}">
                  <td align="center">{{$value+1}}</td>
                  <td>{{$pembeli->nama_barang}}</td>
                  <td>{{$pembeli->nama_pembeli}}</td>
                  <td>{{$pembeli->harga}}</td>
                  <td>{{$pembeli->harga_beli}}</td>
                  <td>{{$pembeli->diskon}}</td>
                  <td>{{$pembeli->jumlah}}</td>
                  <td>{{$pembeli->total}}</td>
                  <td>{{$pembeli->total_beli}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
              </div>
              <!-- ./box-body -->
              <div class="box-footer" align="center">
                
              </div>
              <!-- /.box-footer -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>    
    
@endsection
