@extends('master')

@section('css')
<!-- Datatables -->
    <link href="{{asset('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Home</a> /
  <a href="{{url('#')}}">Pay-Jur</a> /
  <a href="{{url('#')}}">Withdrawing</a>
@stop

@section('title')
  <h3>Pay-Jur</h3>
  {{-- <h3>Withdrawing</h3> --}}
@stop

@section('content')
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        @if ($request->status == 'all')
          <h2>All Withdrawing</h2>
        @elseif ($request->status == 'waiting')
          <h2>Waiting Withdrawing</h2>
        @elseif ($request->status == 'process')
          <h2>Process Withdrawing</h2>
        @elseif ($request->status == 'failed')
          <h2>Failed Withdrawing</h2>
        @elseif ($request->status == 'success')
          <h2>Success Withdrawing</h2>
        @endif
          {!! Form::open(array('url'=>'pencairan/find', 'method'=>'GET', 'class'=>'form-horizontal form-label-left pull-right', 'novalidate'))!!}
                <div class="item form-group pull-right">
                  <button class="btn btn-primary pull-right" type="submit"> GO! </button>
                  <div class="col-md-3 col-sm-3 col-xs-12 pull-right">
                    <select class="form-control select2" name="status" class="form-control col-md-7 col-xs-12">
                      <option value="all">All</option>
                      <option value="waiting">Waiting</option>
                      <option value="process">Process</option>
                      <option value="success">Success</a></option>
                      <option value="failed">Failed</option>
                    </select>
                  </div>
                </div>
          {!!Form::close()!!}
        {{-- <div class="input-group-btn">
          <button  style="margin-bottom:10px" type="button" class="btn btn-default btn-sm dropdown-toggle pull-right" data-toggle="dropdown" aria-expanded="false">Filter <span class="caret"></span>
          </button>
          <ul class="dropdown-menu pull-right" role="menu">
            <li><a href="{{url('pencairan/')}}">All</a>
            </li>
            <li><a href="{{url('pencairan/menunggu')}}">Waiting</a>
            </li>
            <li><a href="{{url('pencairan/proses')}}">Process</a>
            </li>
            <li><a href="{{url('pencairan/sukses')}}">Success</a>
            </li>
            <li><a href="{{url('pencairan/gagal')}}">Failed</a>
            </li>
          </ul>
        </div> --}}
        <div class="clearfix">
        </div>
      </div>
      <div class="x_content">
        {{-- <a href="{{url('master/lokasi/add')}}" class="btn btn-primary btn-sm"><i class="fa fa-plus mg-r-10"></i> Add Location</a> --}}
        <table id="datatable1" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="col-md-1">No</th>
              <th class="col-md-3">Date (Time)</th>
              <th class="col-md-2">Seller</th>
              <th class="col-md-2">Withdraw</th>
              {{-- <th class="col-md-2">Method</th> --}}
              <th class="col-md-2">Status</th>
              <th class="col-md-2">Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($data as $value => $withdraw)
            <tr>
              <td class="col-md-1" align="center">{{$value +1}}</td>
              <td>
                {{\Carbon\Carbon::parse($withdraw->created_at)->format('l, j F Y')}} (
                {{\Carbon\Carbon::parse($withdraw->created_at)->format('H:i')}})
              </td>
              <td>{{$withdraw->user['name']}}</td>
              {{-- <td>{{$withdraw->penjual['id']}}</td> --}}
              <td align="right">Rp {{number_format($withdraw->total,0,".",".")}},-</td>
              {{-- <td>{{$withdraw->metode}}</td> --}}
              <td>{{$withdraw->status}}</td>
              <td>
                <a href="{{url('pencairan/'.$withdraw->id.'')}}" class="btn btn-warning btn-success btn-xs" title="Process">
                  <div><i class="fa fa-paper-plane-o"> Process</i></div>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        <div class="form-group">
          <div>
            <a class="btn btn-primary" onclick="location.href='{{url('/pencairan')}}'">Back</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="deleteModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body text-center">
                <i class="fa fa-4x fa-trash"></i>
                <h5>Sure to delete data?</h5>
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                  Cancel
                </button>
                <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                  Delete
                </button>
            </div>
        </div>
    </div>
  </div>
@endsection

@section('javascript')

    <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>

    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

        });
    </script>
    <script type="text/javascript">
        var id_to_delete;
        $(document).on('click', '.delete-modal', function() {
            $('.modal-title').text('Delete');
            $('#deleteModal').modal('show');
            id_to_delete = $(this).data('id');
            // console.log(id_to_delete);
        });
        $('.modal-body').on('click', '.delete', function() {
          // console.log("click");
            $.ajax({
                type: 'GET',
                headers: {
                  'Accept': 'application/json'
                },
                url: '{{ env('APP_URL') }}/api/master/lokasi/delete/' + id_to_delete,
                success: function(data) {
                  console.log("coba"+data);
                    // toastr.success('Successfully deleted Post!', 'Success Alert', {timeOut: 5000});
                    $('.item-' + id_to_delete).remove();
                    $('.col1').each(function (index) {
                        $(this).html(index+1);
                    });
                }
            });
        });
    </script>

@endsection
