@extends('master')

@section('css')
<!-- Datatables -->
    <link href="{{asset('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Dashboard</a> /
  <a href="#">User</a> /
  <a href="{{url('#')}}">Penjual</a>
@stop

@section('title')
  <h3>Penjual</h3>
@stop

@section('content')
  <div id="app">  
    @if(Session::has('success'))
      <div class="alert alert-success">
        {{ Session::get('success') }}
      </div>
    @elseif(Session::has('error'))
      <div class="alert alert-error">
        {{ Session::get('error') }}
      </div>
    @endif
  </div>

  <div class="exportexcel">    
    <a href="{{url('master/penjual_konven/add')}}" class="btn btn-primary"><i class="fa fa-plus mg-r-10"></i> Tambah Penjual</a> 
  </div><br>
        
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        @if (Request::url() == url('master/penjual/aktif'))
          <div class="x_title">
            <h2>Penjual Aktif<small>dari Data Master</small></h2>
            <div class="input-group-btn">
              <button type="button" style="margin-bottom:10px" class="btn btn-default btn-sm dropdown-toggle pull-right" data-toggle="dropdown" aria-expanded="false">Filter <span class="caret"></span>
              </button>
              <ul class="dropdown-menu pull-right" style="align=right" role="menu">
                <li><a href="{{url('master/penjual_konven/')}}">Semua Penjual</a>
                </li>
                <li><a href="{{url('master/penjual_konven/aktif')}}">Penjual Aktif</a>
                </li>
                <li><a href="{{url('master/penjual_konven/non-aktif')}}">Penjual Tidak Aktif</a>
                </li>
              </ul>
            </div>
            <div class="clearfix">
            </div>
          </div>
        @elseif (Request::url() == url('master/penjual/non-aktif'))
          <div class="x_title">
            <h2>Penjual Tidak Aktif<small>dari Data Master</small></h2>
            <div class="input-group-btn">
              <button type="button" style="margin-bottom:10px" class="btn btn-default btn-sm dropdown-toggle pull-right" data-toggle="dropdown" aria-expanded="false">Filter <span class="caret"></span>
              </button>
              <ul class="dropdown-menu pull-right" style="align=right" role="menu">
                <li><a href="{{url('master/penjual_konven/')}}">Semua Penjual</a>
                </li>
                <li><a href="{{url('master/penjual_konven/aktif')}}">Penjual Aktif</a>
                </li>
                <li><a href="{{url('master/penjual_konven/non-aktif')}}">Penjual Tidak Aktif</a>
                </li>
              </ul>
            </div>
            <div class="clearfix">
            </div>
          </div>
        @else
          <div class="x_title">
            <h2>Semua Penjual<small>dari Data Master</small></h2>
            <div class="input-group-btn">
              <button type="button" style="margin-bottom:10px" class="btn btn-default btn-sm dropdown-toggle pull-right" data-toggle="dropdown" aria-expanded="false">Filter <span class="caret"></span>
              </button>
              <ul class="dropdown-menu pull-right" style="align=right" role="menu">
                <li><a href="{{url('master/penjual_konven/')}}">Semua Penjual</a>
                </li>
                <li><a href="{{url('master/penjual_konven/aktif')}}">Penjual Aktif</a>
                </li>
                <li><a href="{{url('master/penjual_konven/non-aktif')}}">Penjual Tidak Aktif</a>
                </li>
              </ul>
            </div>
            <div class="clearfix">
            </div>
          </div>
        @endif
        <div class="x_content">
          <table id="datatable1" class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th class="col-md-1">No</th>
                <th>Id</th>
                <th>Nama Toko</th>
                <th>Email</th>
                <th>No HP</th>
                <th>Status</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach($list as $value => $penjual)
              <tr class="item-{{$penjual->id}}">
                  <td align="center">{{$value+1}}</td>
                  <td>{{$penjual->id}}</td>
                  <td><a href="#" title="View {{$penjual->name}}"> {{$penjual->name}} </a></td>
                  <td>{{$penjual->email}}</td>
                  <td>{{$penjual->no_telepon}}</td>
                  <td>
                      @if($penjual->status_id==1)
                      <a style="width:70px" href="javascript:if(confirm('Are you sure change status to Inactive?')){window.location.href='{{ url('master/penjual_konven/status/'.$penjual->user_id.'')}}'};" onclick="" class="btn btn-primary btn-xs rounded-circle mg-r-5 mg-b-10" title="Aktive">
                          <div><i class="fa fa-toggle-on"></i> Aktif</div>
                      </a>
                      @else
                        <a style="width:70px" href="javascript:if(confirm('Are you sure change status to Active?')){window.location.href='{{ url('master/penjual_konven/status/'.$penjual->user_id.'')}}'};" onclick="" class="btn btn-default btn-xs rounded-circle mg-r-5 mg-b-10" title="Inaktive">
                          <div><i class="fa fa-toggle-off"></i> Off</div>
                      </a>
                      @endif
                  </td>
                  <td>
                      <a href="{{url('master/penjual_konven/'.$penjual->id.'')}}" class="btn btn-warning btn-icon" title="Edit">
                          <div><i class="fa fa-pencil"></i></div>
                      </a>
<!--                       <a href="{{url('master/penjual_konven/'.$penjual->id.'')}}" class="btn btn-info btn-icon" title="Lihat Produk">
                          <div><i class="fa fa-shopping-bag"></i></div>
                      </a> -->
                      <a href="javascript:if(confirm('Yakin ingin hapus data?')){window.location.href='{{ url('master/penjual_konven/delete/'.$penjual->id.'')}}'};" onclick="" class="btn btn-danger btn-icon" title="Hapus">
                          <div><i class="fa fa-trash"></i></div>
                      </a>
                  </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('javascript')

      <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
      <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
      <script src="{{asset('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>

    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>
    <script type="text/javascript">$('#app').fadeTo(300,1).fadeOut(3000);</script>

@endsection
