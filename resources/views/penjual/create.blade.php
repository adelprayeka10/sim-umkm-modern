@extends('layout')

@section('css')
    <link href="{{asset('lib/highlightjs/github.css')}}" rel="stylesheet">
    <link href="{{asset('lib/datatables/jquery.dataTables.css')}}" rel="stylesheet">
    <link href="{{asset('lib/select2/css/select2.min.css')}}" rel="stylesheet">
@endsection

@section('breadcrumbs')
    <nav class="breadcrumb sl-breadcrumb">
        <a class="breadcrumb-item" href="{{url('/')}}">Home</a>
        <a class="breadcrumb-item" href="{{url('penjual')}}">Penjual</a>
        <a class="breadcrumb-item" href="{{url('penjual/add')}}">Tambah Penjual</a>
    </nav>
@stop

@section('title')
    <div class="sl-page-title">
        <h5>Tambah Penjual</h5>
    </div><!-- sl-page-title -->
@stop

@section('content')
{!! Form::open(['url'=>'penjual/store'])!!}
    <div class="card pd-10 pd-sm-20">
        <div class="row">
          <div class="col-md-12 pl-5 pr-5">
              <div class="row pl-5 pr-5" style="margin-top: 5px;">
                  <div class="col-md-4">
                      <p>Username</p>
                  </div>
                  </label>
                  <div class="col-md-8">
                    <input class="form-control" placeholder="username" name="username" value="{{ old('username') }}" type="text">
                  </div>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 pl-5 pr-5">
              <div class="row pl-5 pr-5" style="margin-top: 5px;">
                  <div class="col-md-4">
                      <p>Email</p>
                  </div>
                  </label>
                  <div class="col-md-8">
                    <input class="form-control" placeholder="email" name="email" value="{{ old('email') }}" type="email">
                  </div>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 pl-5 pr-5">
              <div class="row pl-5 pr-5" style="margin-top: 5px;">
                  <div class="col-md-4">
                      <p>No HP</p>
                  </div>
                  </label>
                  <div class="col-md-8">
                    <input class="form-control" placeholder="no telephone" name="no_telephone" value="{{ old('no_telephone') }}" type="number">
                  </div>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 pl-5 pr-5">
              <div class="row pl-5 pr-5" style="margin-top: 5px;">
                  <div class="col-md-4">
                      <p>Password</p>
                  </div>
                  </label>
                  <div class="col-md-8">
                    <input class="form-control" placeholder="minimal 8 karakter" name="password" value="{{ old('password') }}" type="password">
                  </div>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 pl-5 pr-5">
              <div class="row pl-5 pr-5" style="margin-top: 5px;">
                  <div class="col-md-4">
                      <p>Tanggal Lahir</p>
                  </div>
                  </label>
                  <div class="col-md-8">
                    <input name="tanggal_lahir" type="text" class="form-control fc-datepicker" placeholder="YYYY-MM-DD" value="{{old('tanggal_lahir')}}" >
                  </div>
              </div>
          </div>
        </div>
        <div class="row cl-md-12" style="float:right ; margin-top: 10px;">
            <div class="col-md-12" style="text-align: center; ">
                <button type="submit" class="btn btn-success">Save</button>
                <button type="button" class="btn btn-danger" onclick="location.href='{{url('penjual')}}'">Cancel</button>
            </div>
        </div>
    </div>
{!!Form::close()!!}

        <!-- row -->
@endsection

@section('javascript')
    <script src="{{asset('lib/highlightjs/highlight.pack.js')}}"></script>
    <script src="{{asset('lib/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('lib/datatables-responsive/dataTables.responsive.js')}}"></script>
    <script src="{{asset('lib/select2/js/select2.min.js')}}"></script>

    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

            // Datepicker
            $('.fc-datepicker').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                dateFormat: 'yy-mm-dd'
            });

        });
    </script>

@endsection
