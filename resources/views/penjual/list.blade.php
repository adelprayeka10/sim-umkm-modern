@extends('layout')

@section('css')
<link href="{{asset('lib/highlightjs/github.css')}}" rel="stylesheet">
<link href="{{asset('lib/datatables/jquery.dataTables.css')}}" rel="stylesheet">
<link href="{{asset('lib/select2/css/select2.min.css')}}" rel="stylesheet">
@endsection

@section('breadcrumbs')
<nav class="breadcrumb sl-breadcrumb">
    <a class="breadcrumb-item" href="{{url('/')}}">Home</a>
    <a class="breadcrumb-item" href="#">Penjual</a>
</nav>
@stop

@section('title')
<div class="sl-page-title">
    <h5>Penjual</h5>
</div><!-- sl-page-title -->
@stop

@section('content')
	<div class="card pd-10 pd-sm-20">
    <div class="row">
        <div class="col-sm-6 col-md-3">
            <a href="{{url('penjual/add')}}" class="btn btn-primary btn-block mg-b-10"><i class="fa fa-plus mg-r-10"></i> Tambah Penjual</a>
        </div><!-- col-sm -->
    </div>

    <div class="table-wrapper mg-t-10">
        <table id="datatable1" class="table display responsive nowrap">
            <thead>
              <tr>
                <th>No</th>
                <th>Id</th>
                <th>Username</th>
                <th>Email</th>
                <th>No Telepon</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($list as $value => $penjual)
              <tr>
                  <td align="center">{{$value+1}}</td>
                  <td>{{$penjual->id}}</td>
                  <td>{{$penjual->name}}</td>
                  <td>{{$penjual->email}}</td>
                  <td>{{$penjual->no_telepon}}</td>
                  <td align="center">
                      {{--<div class="col-md-6">--}}
                      <a href="{{url('penjual/edit/'.$penjual->id.'')}}" class="btn btn-warning btn-icon rounded-circle mg-r-5 mg-b-10" title="Edit">
                          <div><i class="fa fa-pencil"></i></div>
                      </a>
                      <a href="javascript:if(confirm('Yakin ingin hapus data?')){window.location.href='{{ url('penjual/delete/'.$penjual->id.'')}}'};" onclick="" class="btn btn-danger btn-icon rounded-circle mg-r-5 mg-b-10" title="Delete">
                          <div><i class="fa fa-trash"></i></div>
                      </a>
                      {{--</div>--}}
                  </td>
              </tr>
              @endforeach
            </tbody>
        </table>
      </div><!-- table-wrapper -->
    </div><!-- card -->

@endsection

@section('javascript')
    <script src="{{asset('lib/highlightjs/highlight.pack.js')}}"></script>
    <script src="{{asset('lib/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('lib/datatables-responsive/dataTables.responsive.js')}}"></script>
    <script src="{{asset('lib/select2/js/select2.min.js')}}"></script>

    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>

@endsection
