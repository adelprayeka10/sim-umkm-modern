@extends('layout_pos')

@section('css')
  <link href="{{asset('assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">

@endsection

@section('content')
<section class="content-header">
      <h1>
        Ubah Data Profil
        <small>Kasir</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-pencil"></i> Profil</a></li>
        <li class="active">Ubah Password</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

    <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Ubah Password</h3>
            </div>

            <div class="box-body">
            {!! Form::open(['url'=>'profil_pos/'.Auth::user()->id.'/edit-password', 'role'=>'form', 'files'=>true, 'class'=>'form-horizontal form-label-left', 'novalidate'])!!}
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Password Lama <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="password" id="password-current" class="form-control" placeholder="Masukan password lama" name="password_current" value="" >
                  @if ($errors->has('password_current'))
                      <span class="invalid-feedback" role="alert" style="color: red">
                          <strong>{{ $errors->first('password_current') }}</strong>
                      </span>
                  @endif
                  @if (session('error'))
                    <span class="invalid-feedback" role="alert" style="color: red">
                        <strong>{{ session('error') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                  <span for="password" class="label-input100">Password Baru</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="password" placeholder="Masukan password baru lagi" type="password" class="input100 form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                  {{-- <input type="password" id="password" placeholder="" name="password_new" value=""  class="form-control col-md-7 col-xs-12"> --}}
                  @if ($errors->has('password'))
                      <span class="invalid-feedback" role="alert" style="color: red">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                  <span for="password" class="label-input100">Konfirmasi Password Baru</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="password-confirm" type="password" placeholder="Masukan ulang password baru" class="input100 form-control" name="password_confirmation" required>
                </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                  <a class="btn btn-primary" onclick="location.href='{{url('profil_pos/'.Auth::user()->id.'')}}'">Batal</a>
                  <button id="send" type="submit" class="btn btn-success">Simpan</button>
                </div>
              </div>
            {!!Form::close()!!}
          </div>
        </div>
      </div>
    </div>
    

        <!-- row -->
@endsection

@section('javascript')
    <script src="{{asset('lib/highlightjs/highlight.pack.js')}}"></script>
    <script src="{{asset('lib/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('lib/datatables-responsive/dataTables.responsive.js')}}"></script>
    <script src="{{asset('lib/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>


    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

            // Datepicker
            $('.fc-datepicker').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                dateFormat: 'dd-mm-yy'
            });

        });
    </script>
    <script type="text/javascript">
     $(function(){
      $(".datepicker").datepicker({
        // format: "dd/mm/yyyy",
        endDate: "dd",
        autoclose: true,
        todayHighlight: true,
      });
     });
    </script>

@endsection
