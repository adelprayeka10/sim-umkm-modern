@extends('master')

@section('css')
<!-- Datatables -->
    <link href="{{asset('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet">
@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Dashboard</a> /
  <a href="#">Profil</a>

@stop

@section('title')
  <h3>Profil Amin</h3>
@stop

@section('content')
  <div id="app">
    @if(Session::has('success'))
      <div class="alert alert-success">
        {{ Session::get('success') }}
      </div>
    @elseif(Session::has('error'))
      <div class="alert alert-error">
        {{ Session::get('error') }}
      </div>
    @endif
  </div>

  <div class="x_panel">
    <div class="x_title">
      <h2>Profil</h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <div class="col-md-2 col-sm-2 col-xs-12 profile_left" style="margin-bottom:10px">
        <div class="profile_img">
          <div id="crop-avatar" class="text-center">
            <img class="img-responsive img-circle avatar-view" src="{{asset('images/'.$user->foto.'')}}" alt="Avatar" title="Change the avatar">
            <div class="" style="margin-top:10px">
              <a  href="{{url('profile/'.Auth::user()->id.'/edit-avatar')}}" class="" title="Edit">
                <div>Ganti Foto</i></div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-9 col-sm-9 col-xs-12 profile_left">
        <label>DATA UMUM</label>
        <table class="table display responsive nowrap">
          <tbody>
            <tr>
              <td class="col-md-2" style="text-align:right"> Email:</td>
              <td>{{$user->email}}</td>
            </tr>
            <tr>
              <td class="col-md-2" style="text-align:right"> Nama:</td>
              <td>{{$user->name}}</td>
            </tr>
            <tr>
              <td class="col-md-2" style="text-align:right"> Nomor Telepon:</td>
              <td>{{$user->no_telepon}}</td>
            </tr>
            <tr>
              <td class="col-md-2" style="text-align:right"> Tanggal Lahir:</td>
              <td>{{\Carbon\Carbon::parse($user->tanggal_lahir)->format('j F Y')}}</td>
            </tr>
            <tr>
              <td></td>
              <td>
                <a style="width:70px" onclick="location.href='{{url('profile/'.Auth::user()->id.'/edit')}}'" class="btn btn-primary btn-xs rounded-circle mg-r-5 mg-b-10" title="Top Up">
                    <div><i class="fa fa-pencil"> Ubah</i></div>
                </a>
              </td>
            </tr>
          </tbody>
        </table>
        <label>PASSWORD </label>
        <table class="table display responsive nowrap">
          <tbody>
            <tr>
              <td class="col-md-2" style="text-align:right"> Password:</td>
              <td>********</td>
            </tr>
            <tr>
              <td></td>
              <td>
                <a style="width:70px" onclick="location.href='{{url('profile/'.Auth::user()->id.'/edit-password')}}'" class="btn btn-primary btn-xs rounded-circle mg-r-5 mg-b-10" title="Top Up">
                    <div><i class="fa fa-gear"> Ubah</i></div>
                </a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
@endsection

@section('javascript')
      <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
      <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
      <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
      <script src="{{asset('assets/vendors/select2/dist/js/select2.min.js')}}"></script>
      <script src="{{asset('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
      <script type="text/javascript">$('#app').fadeTo(300,1).fadeOut(3000);</script>
@endsection
