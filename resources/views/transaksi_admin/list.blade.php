@extends('master')

@section('css')
<!-- Datatables -->
    <link href="{{asset('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Home</a> /
  <a href="{{url('/transaksi_admin')}}">Transaction</a>
@stop

@section('title')
  <h3>Transaction</h3>
@stop

@section('content')
            <div id="app" >
              @if(Session::has('success'))
                <div class="alert alert-success">
                  {{ Session::get('success') }}
                </div>
              @elseif(Session::has('error'))
                <div class="alert alert-error">
                  {{ Session::get('error') }}
                </div>
              @endif
            </div>
            <form action="{{ url('transaksi_admin/export') }}" method="get" class="form-inline" enctype="multipart/form-data">
                <div class="exportexcel">           
                  
                    <input type="hidden" name="tgl_awal" value="{{$request->from}}">
                    <input type="hidden" name="tgl_akhir" value="{{$request->until}}">

                  <button type="submit" class="btn btn-success fa fa-download">Export Excel</button>

                </div>
            </form>
            <div class="row">
              @if (count($errors)>0)
                <div id="myError" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body text-center">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <i class="fa fa-4x fa-exclamation-triangle" style="color:red"></i>
                                <h4 style="color:red">Ooops...</h4>
                                @foreach($errors->all() as $error)
                                  <a>{{$error}}<br></a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
              @endif
              @if (session('error'))
                <div id="myError" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body text-center">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <i class="fa fa-4x fa-exclamation-triangle" style="color:red"></i>
                                <h4 style="color:red">Ooops...</h4>
                                  <a>{{ session('error') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
              @endif
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2>Transaction<small></small></h2>
                        {!! Form::open(array('url'=>'transaksi_admin/find', 'method'=>'GET', 'class'=>'form-horizontal form-label-left pull-right', 'novalidate'))!!}
                              <div class="item form-group pull-right">
                                <button class="btn btn-primary pull-right" type="submit"> GO! </button>
                                <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                  <input type="text" name="until" class="form-control datepicker" value="{{\Carbon\Carbon::parse($request->until)->addDay('-1')->format('m/d/Y')}}">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                  <input type="text" name="from" class="form-control datepicker" value="{{\Carbon\Carbon::parse($request->from)->format('m/d/Y')}}">
                                </div>
                              </div>
                        {!!Form::close()!!}
                        <div class="clearfix">
                        </div>
                      </div>
                      <div class="x_content">
                        <table id="datatable1" class="table table-hover">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Tanggal</th>
                              <th>Waktu</th>
                              <th>Toko</th>
                              <th>Pembeli</th>
                              <th>Total</th>
                              <th>Status</th>
                              <th>Aksi</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($data as $value => $transaksi)
                            <tr>
                              <td align="center">{{$value+1}}</td>
                              <td>
                                {{\Carbon\Carbon::parse($transaksi->updated_at)->formatLocalized('%A, %d %B %Y')}}<br>
                              </td>
                              <td>
                                {{\Carbon\Carbon::parse($transaksi->updated_at)->format('H:i:s')}}
                              </td>
                              <td>{{$transaksi->nama_toko}}</td>
                              <td>{{$transaksi->nama}}</td>
                              <td>Rp {{number_format($transaksi->total,0,".",".")}},-</td>
                              <td>{{$transaksi->status}}</td>
                              <td>
                                <a href="{{url('transaksi_admin/'.$transaksi->id.'')}}" class="btn btn-success btn-xs rounded-circle mg-r-5 mg-b-10" title="Detail">
                                  <div><i class="fa fa-eye"></i> Detail</div>
                                </a>
                              </td>
                              {{-- <td>{{$report['remain']}}</td>
                              <td>{{$report['income']}}</td> --}}
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                        <div class="form-group">
                          <div>
                            <a class="btn btn-primary" onclick="location.href='{{url('/transaksi_admin')}}'">Back</a>
                          </div>
                        </div>
                        <div class="ln_solid"></div>
                      </div>
                    </div>
                  </div>
                </div>
@endsection

@section('javascript')

    <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{asset('assets/vendors/select2/dist/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>

    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        });
    </script>
    <script type="text/javascript">
     $(function(){
      $(".datepicker").datepicker({
        // format: "dd/mm/yyyy",
        endDate: "dd",
        autoclose: true,
        todayHighlight: true,
      });
     });
    </script>
    <script>$("#myModalError").modal("show");</script>
    <script>$("#myError").modal("show");</script>
@endsection
