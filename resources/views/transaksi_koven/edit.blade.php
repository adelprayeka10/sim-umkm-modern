@section('js')
@stop
@extends('layout_pos')

@section('content')
  <section class="content-header">
    <h1>
      Edit Produk
      <small>{{Auth::user()->name}}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-pencil"></i> Transaksi</a></li>
      <li class="active"> Edit Detail Transaksi</li>
    </ol>
  </section>
  <section class="content">
    <div><br>
      @if(Session::has('success'))
        <div id="app" class="alert alert-success">
          {{ Session::get('success') }}
        </div>
      @elseif(Session::has('error'))
        <div class="alert alert-error">
          {{ Session::get('error') }}
        </div>
      @endif
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Detail Transaksi</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <form action="{{url('transaksi_konven/update_detail_pos/'.$transaksi->id.'')}}" method="post" enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="modal-header">
                <a class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></a>
                <center><h2 class="modal-title" id="myModalLabel"><i class="fa fa-exclamation-circle"></i> Edit Data</h2></center>
              </div>                  
              <div class="modal-body">        
                <div class="form-group">
                  <label for="jumlah">Jumlah Barang<span class="required" style="color:red">*</span></label>
                  <input class="form-control" placeholder="Jumlah Barang" name="jumlah" id="jumlah{{ $transaksi->id }}" value="{{(isset($transaksi->jumlah))?$transaksi->jumlah:''}} " type="text">
                  @if ($errors->has('jumlah'))
                  <span class="invalid-feedback" role="alert" style="color: red">
                    <strong>{{ $errors->first('jumlah') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="harga">Harga<span class="required" style="color:red">*</span></label>
                  <input class="form-control" placeholder="Harga" name="harga" id="harga{{ $transaksi->id }}" value="{{(isset($transaksi->harga))?$transaksi->harga:''}} " type="text" readonly>
                </div>
                <div class="form-group">
                  <label for="harga_beli">Harga Beli<span class="required" style="color:red">*</span></label>
                  <input class="form-control" placeholder="Harga Beli" name="harga_beli" id="harga_beli{{ $transaksi->id }}" value="{{(isset($transaksi->harga_beli))?$transaksi->harga_beli:''}} " type="text" readonly>
                </div>
                <div class="form-group">
                  <label for="total">Total<span class="required" style="color:red">*</span></label>
                  <input class="form-control" placeholder="Total" name="total" id="total{{ $transaksi->id }}" value="{{(isset($transaksi->total))?$transaksi->total:''}} " type="text" readonly>
                </div>
                <div class="form-group">
                  <label for="thb">THB<span class="required" style="color:red">*</span></label>
                  <input class="form-control" placeholder="THB" name="total_beli" id="total_beli{{ $transaksi->id }}" value="{{(isset($transaksi->total_beli))?$transaksi->total_beli:''}} " type="text" readonly>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-success">Edit</button>
              </div>
            </form>
          </div>
          <!-- ./box-body -->
          <div class="box-footer" align="center">
          </div>
          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@endsection

@section('javascript')
    <script src="{{asset('lib/highlightjs/highlight.pack.js')}}"></script>
    <script src="{{asset('lib/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('lib/datatables-responsive/dataTables.responsive.js')}}"></script>
    <script src="{{asset('lib/select2/js/select2.min.js')}}"></script>

    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>
    <script type="text/javascript">$('#app').fadeTo(300,1).fadeOut(1000);</script>

@endsection
