@section('js')
  <script>$("#myModalError").modal("show");</script>
  <script>$("#myError").modal("show");</script>

@stop
@extends('layout_pos')

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Daftar Transaksi
      <small>Gamapay</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-pencil"></i> Dartar Transaksi</a></li>
      <li class="active">Gamapay</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Masukan Rentang Tanggal</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body"> 
            @if (count($errors)>0)
            <div id="myError" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body text-center">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <i class="fa fa-4x fa-exclamation-triangle" style="color:red"></i>
                            <h4 style="color:red">Ooops...</h4>
                            @foreach($errors->all() as $error)
                              <a>{{$error}}<br></a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
          @endif
          <div class="row">
            <div class="col-md-12">
                {!! Form::open(array('url'=>'transaksi_konven/list', 'method'=>'GET', 'class'=>'form-horizontal form-label-left pull-right', 'novalidate'))!!}
                  <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tanggal Mulai<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <div class="input-group date">
                          <div class="input-group-addon">
                              <span class="glyphicon glyphicon-th"></span>
                          </div>
                          <input type="text" placeholder="MM/DD/YYYY" name="tgl_awal" class="form-control datepicker" value="{{old('tgl_awal')}}">
                      </div>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tanggal Selesai<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <div class="input-group date">
                          <div class="input-group-addon">
                              <span class="glyphicon glyphicon-th"></span>
                          </div>
                          <input type="text" placeholder="MM/DD/YYYY" name="tgl_akhir" class="form-control datepicker" value="{{old('tgl_akhir')}}">
                      </div>
                    </div>
                  </div>
                <div class="form-group">
                  <div class="col-md-6 col-md-offset-3">
                    <button class="btn btn-primary" type="submit"> Cari! </button>
                  </div>
                </div>
                {!!Form::close()!!}
          </div>
          @if (session('error'))
            <div id="myError" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body text-center">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <i class="fa fa-4x fa-exclamation-triangle" style="color:red"></i>
                            <h4 style="color:red">Ooops...</h4>
                              <a>{{ session('error') }}</a>
                        </div>
                    </div>
                </div>
            </div>
          @endif                 
          </div>
          <!-- ./box-body -->
          <div class="box-footer" align="center">
          </div>
          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>    
    
@endsection
