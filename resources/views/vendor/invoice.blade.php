<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Nota Belanja Kantin UGM</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }
        body {
            margin: 0px;
        }
        * {
            font-family: Verdana, Arial, sans-serif;
        }
        a {
            color: #fff;
            text-decoration: none;
        }
        table {
            font-size: x-small;
        }
        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }
        .invoice table {
            margin: 15px;
        }
        .invoice h3 {
            margin-left: 15px;
        }
        .information {
            background-color: #FFFFFF;
            color: #000000;
        }
        .information .logo {
            margin: 5px;
        }
        .information table {
            padding: 10px;
        }
    </style>

</head>
<body>

<div class="information">
    <table width="100%">
        <tr>
            <!-- <td align="left" style="width: 40%;">
                <pre>
                    Tanggal: 2018-01-01
                    No Transaksi: 34455
                    User: Karyawan
                </pre>
            </td> -->
            <td align="center" >
                <img src="image/logo-ugm.png" alt="Logo" width="64" class="logo"/>
                <h3>Kantin UGM</h3>
                <span>{{$value->penjual_konven->nama_toko}}</span><br>
            </td>
            <!-- <td align="right" style="width: 40%;">

                <h3>CompanyName</h3>
                <pre>
                    https://company.com

                    Street 26
                    123456 City
                    United Kingdom
                </pre>
            </td> -->
        </tr>
        <tr>
            <td align="left" style="width: 40%;">
                <pre>

                    Tanggal: {{ date('d:m:y') }}
                    No Transaksi: {{$value->id}}
                    Penjual: {{$value->penjual_konven->nama_toko}}
                    Pembeli : {{$value->pembeli->nama}}
                </pre>
            </td>
        </tr>
    </table>
</div>
        ========================================================================================
<div class="invoice">
    <h3>Detail Transaksi</h3>
    <table width="100%">
        <thead>
        <tr>
            <th>Barang</th>
            <th>Jmlh</th>
            <th>Harga</th>
            <th>Diskon</th>
            <th>Sub Total</th>
        </tr>
        </thead>
        <tbody>
            @foreach($data as $item)
        <tr>
            <td>{{$item['barang_konven']['nama']}}</td>
            <td>{{$item['jumlah']}}</td>
            <td>{{$item['harga']}}</td>
            <td>{{$item['diskon']}}</td>
            <td align="left">{{$item['total']}}</td>
        </tr>
            @endforeach
        </tbody>
        <tfoot>

        <tr>
            <td colspan="1"></td>
            <td colspan="1"></td>
            <td colspan="1"></td>
            <td align="left">Total</td>
            <td align="left" class="gray">{{$item['transaksi_konven']['total']}}</td>
        </tr>
            
        </tfoot>
    </table>
</div>


<div class="information" style="position: absolute; bottom: 0;">
    <table width="100%">
        <tr>
            <td align="left" style="width: 50%;">
                &copy; {{ date('Y') }} {{ config('app.url') }} - All rights reserved.
            </td>
            <td align="right" style="width: 50%;">
                GamaPay UGM
            </td>
        </tr>

    </table>
</div>
</body>
</html>