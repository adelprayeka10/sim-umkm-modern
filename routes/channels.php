<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

// API FOR ANDROID
Route::middleware('auth:api')->get('logout', 'UserController@api_logout');
// PEMBELI
Route::group(['prefix' => 'pembeli'], function()
{
  Route::middleware('api')->post('/register', 'UserController@api_registerPembeli');
  Route::middleware('api')->post('/login','UserController@api_loginPembeli');
  Route::middleware('auth:api')->put('/edit','UserController@api_updatePembeli');
  Route::middleware('auth:api')->put('/upload-ava','UserController@api_uploadAva');
  Route::middleware('auth:api')->put('/edit-password','UserController@api_updatePassword');
  Route::middleware('auth:api')->get('/detail', 'UserController@api_detail');
  Route::middleware('auth:api')->get('/saldo/riwayat', 'LogSaldoController@riwayat');
  Route::middleware('auth:api')->get('/lokasi', 'LokasiController@api_indexPembeli');
  Route::middleware('auth:api')->get('/stok-barang/{id}', 'StokBarangController@api_indexPembeli');
  Route::middleware('auth:api')->get('/barang/detail/{id}', 'StokBarangController@api_detailBarang');
  Route::middleware('auth:api')->get('/keranjang', 'KeranjangController@api_index');
  Route::middleware('auth:api')->post('/keranjang/create/{id}', 'KeranjangController@api_store');
  Route::middleware('auth:api')->put('/keranjang/edit/{id}', 'KeranjangController@api_update');
  Route::middleware('auth:api')->delete('/keranjang/delete/{id}', 'KeranjangController@api_destroy');
  Route::middleware('auth:api')->delete('/keranjang/delete/', 'KeranjangController@api_destroyAll');
  Route::middleware('auth:api')->get('/jenis-pembayaran', 'JenisPembayaranController@api_index');
  Route::middleware('auth:api')->post('/transaksi/create', 'TransaksiController@api_store');
  Route::middleware('auth:api')->get('/transaksi/pilih-metode', 'TransaksiController@api_pilihMetode');
  Route::middleware('auth:api')->get('/transaksi/waiting', 'TransaksiController@api_waiting');
  Route::middleware('auth:api')->put('/transaksi/pay/{id}', 'TransaksiController@api_pay');
  Route::middleware('auth:api')->get('/transaksi/success/{id}', 'TransaksiController@api_transactionSuccess');
  Route::middleware('auth:api')->get('/transaksi/failed/{id}', 'TransaksiController@api_transactionFailed');
  Route::middleware('auth:api')->get('/transaksi/riwayat', 'TransaksiController@api_riwayat');
  Route::middleware('auth:api')->get('/transaksi/{id}', 'TransaksiController@api_detail');
});