<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index')->middleware('isAdmin');

Auth::routes();
Auth::routes(['verify' => true]);

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('dashboard', 'DashboardController@index')->middleware('isAdmin');
Route::get('/transaksi_admin', 'TransaksiController@index')->middleware('isAdmin');
Route::post('/transaksi_admin', 'TransaksiController@index')->middleware('isAdmin');
Route::get('/transaksi_admin/find', 'TransaksiController@find')->middleware('isAdmin');
Route::get('transaksi_admin/export', 'TransaksiController@exportExcel')->middleware('isAdmin');
Route::get('/transaksi_admin/{id}', 'TransaksiController@detail')->middleware('isAdmin');
Route::get('invoice_admin/{id}', 'TransaksiController@getPdf')->middleware('isAdmin');

// ====================== PROFILE ADMIN =========================
Route::get('/profile/{id}', 'ProfilController@index')->middleware('isAdmin');
Route::get('/profile/{id}/edit', 'ProfilController@edit')->middleware('isAdmin');
Route::post('/profile/{id}/edit', 'ProfilController@update')->middleware('isAdmin');
Route::get('/profile/{id}/edit-password', 'ProfilController@editPassword')->middleware('isAdmin');
Route::post('/profile/{id}/edit-password', 'ProfilController@updatePassword')->middleware('isAdmin');
Route::get('/profile/{id}/edit-avatar', 'ProfilController@editAvatar')->middleware('isAdmin');
Route::post('/profile/{id}/edit-avatar', 'ProfilController@updateAvatar')->middleware('isAdmin');

// ====================== PROFILE PENJUAL =========================
Route::get('/profil_pos/{id}', 'ProfilPosController@index')->middleware('isPenjual');
Route::get('/profil_pos/{id}/edit', 'ProfilPosController@edit')->middleware('isPenjual');
Route::post('/profil_pos/{id}/edit', 'ProfilPosController@update')->middleware('isPenjual');
Route::get('/profil_pos/{id}/edit-password', 'ProfilPosController@editPassword')->middleware('isPenjual');
Route::post('/profil_pos/{id}/edit-password', 'ProfilPosController@updatePassword')->middleware('isPenjual');
Route::get('/profil_pos/{id}/edit-avatar', 'ProfilPosController@editAvatar')->middleware('isPenjual');
Route::post('/profil_pos/{id}/edit-avatar', 'ProfilPosController@updateAvatar')->middleware('isPenjual');
Route::post('/profil_pos/{id}/mutasi-saldo', 'ProfilPosController@mutasiSaldo')->middleware('isPenjual');

// ====================== PROFILE KASIR =========================
Route::get('/profil_kasir/{id}', 'ProfilKasirController@index')->middleware('isKasir');
Route::get('/profil_kasir/{id}/edit', 'ProfilKasirController@edit')->middleware('isKasir');
Route::post('/profil_kasir/{id}/edit', 'ProfilKasirController@update')->middleware('isKasir');
Route::get('/profil_kasir/{id}/edit-password', 'ProfilKasirController@editPassword')->middleware('isKasir');
Route::post('/profil_kasir/{id}/edit-password', 'ProfilKasirController@updatePassword')->middleware('isKasir');
Route::get('/profil_kasir/{id}/edit-avatar', 'ProfilKasirController@editAvatar')->middleware('isKasir');
Route::post('/profil_kasir/{id}/edit-avatar', 'ProfilKasirController@updateAvatar')->middleware('isKasir');
Route::post('/profil_kasir/{id}/mutasi-saldo', 'ProfilKasirController@mutasiSaldo')->middleware('isKasir');

// ====================== START OF PENJUAL =========================
Route::group(['middleware' => ['isAdmin','verified'],'prefix' => 'master/penjual_konven'], function()
{
  Route::get('/', 'PenjualKonvenController@index');
  Route::get('/aktif', 'PenjualKonvenController@aktif');
  Route::get('/non-aktif', 'PenjualKonvenController@nonAktif');
  Route::get('/add', 'PenjualKonvenController@create');
  Route::post('/store', 'PenjualKonvenController@store');
  Route::get('/edit/{id}', 'PenjualKonvenController@edit');
  Route::post('/edit/{id}', 'PenjualKonvenController@update');
  Route::get('/edit/{id}/password', 'PenjualKonvenController@editPassword');
  Route::post('/edit/{id}/password', 'PenjualKonvenController@updatePassword');
  Route::get('/edit/{id}/avatar', 'PenjualKonvenController@editAvatar');
  Route::post('/edit/{id}/avatar', 'PenjualKonvenController@updateAvatar');
  Route::get('/{id}','PenjualKonvenController@detail');
  Route::get('/status/{id}', 'PenjualKonvenController@status');
  Route::get('/delete/{id}', 'PenjualKonvenController@delete');
});

// ====================== START OF PEMBELI =========================

Route::get('master/pembeli', 'PembeliMasterController@index')->middleware('isAdmin');
Route::get('master/pembeli_bukan_member', 'PembeliMasterController@index2')->middleware('isAdmin');
Route::get('master/pembeli/aktif', 'PembeliMasterController@aktif')->middleware('isAdmin');
Route::get('master/pembeli/non-aktif', 'PembeliMasterController@nonAktif')->middleware('isAdmin');
Route::get('master/pembeli/add', 'PembeliMasterController@create')->middleware('isAdmin');
Route::get('master/pembeli/add/nonmember/{id}', 'PembeliMasterController@create2')->middleware('isAdmin');
Route::post('master/pembeli/store', 'PembeliMasterController@store')->middleware('isAdmin');
Route::post('master/pembeli/store2/{id}', 'PembeliMasterController@store2')->middleware('isAdmin');
Route::get('master/pembeli/edit/{id}', 'PembeliMasterController@edit')->middleware('isAdmin');
Route::post('master/pembeli/edit/{id}', 'PembeliMasterController@update')->middleware('isAdmin');
Route::get('master/pembeli/export','PembeliMasterController@downloadExcel')->middleware('isAdmin');
Route::get('master/pembeli/downloadTemplateExcel','PembeliMasterController@downloadTemplateExcel')->middleware('isAdmin');
Route::post('master/pembeli/import','PembeliMasterController@import')->middleware('isAdmin');
Route::get('master/pembeli/edit/{id}/password', 'PembeliMasterController@editPassword')->middleware('isAdmin');
Route::post('master/pembeli/edit/{id}/password', 'PembeliMasterController@updatePassword')->middleware('isAdmin');
Route::get('master/pembeli/edit/{id}/tambah-saldo', 'TopupController@create')->middleware('isAdmin');
Route::get('master/pembeli/edit/{id}/avatar', 'PembeliMasterController@editAvatar')->middleware('isAdmin');
Route::post('master/pembeli/edit/{id}/avatar', 'PembeliMasterController@updateAvatar')->middleware('isAdmin');
Route::get('master/pembeli/status/{id}', 'PembeliMasterController@status')->middleware('isAdmin');
Route::get('master/pembeli/{id}','PembeliMasterController@detail')->middleware('isAdmin');
Route::get('master/pembeli/delete/{id}','PembeliMasterController@delete')->middleware('isAdmin');

// ====================== START OF LAPORAN =========================

Route::group(['middleware' => ['isAdmin','verified'], 'prefix' => 'laporan'], function()
{
  // START OF lAPORAN PENJUAL
  Route::get('/penjual', 'PenjualReportController@index');
  Route::post('/penjual', 'PenjualReportController@index');
  Route::get('/penjual/find', 'PenjualReportController@laporan');
  Route::get('/penjual/downloadPDF/','PenjualReportController@downloadPDF');
  Route::get('/penjual/downloadExcel', 'PenjualReportController@downloadExcel');
  Route::get('/penjual/stok-barang/{id}', 'PenjualReportController@detailStok');
  // START OF lAPORAN PEMBELI
  Route::get('/pembeli', 'PembeliReportController@index');
  Route::post('/pembeli', 'PembeliReportController@index');
  Route::get('/pembeli/find', 'PembeliReportController@laporan');
  Route::get('/pembeli/downloadPDF/','PembeliReportController@downloadPDF');
  Route::get('/pembeli/downloadExcel', 'PembeliReportController@downloadExcel');
  // START OF lAPORAN BARANG
  Route::get('/barang', 'BarangReportController@index');
  Route::post('/barang', 'BarangReportController@index');
  Route::get('/barang/find', 'BarangReportController@laporan');
  Route::get('/barang/downloadPDF/','BarangReportController@downloadPDF');
  Route::get('/barang/downloadExcel', 'BarangReportController@downloadExcel');
});

// ====================== TOP UP =========================
Route::group(['middleware' => ['isAdmin','verified'], 'prefix' => 'topup'], function()
{
  Route::get('/', 'TopupController@index');
  Route::get('/detail-user/{id}', 'TopupController@detail');
  Route::post('/{id}/tambah-saldo', 'TopupController@store');
});

Route::group(['middleware' => ['isAdmin','verified'], 'prefix' => 'pencairan'], function()
{
  // Route::get('/', 'PencairanController@index');
  Route::get('/', 'PencairanController@find');
  Route::get('/find', 'PencairanController@index');
  Route::get('/detail-user/{id}', 'PencairanController@detail');
  Route::post('/{id}/tambah-saldo', 'PencairanController@store');
  Route::get('/{id}', 'PencairanController@detail');
  Route::post('/{id}', 'PencairanController@update');
});

// ====================== START OF MASTER DATA =========================

// START OF MASTER BARANG
Route::get('master/jenis-pembayaran', 'JenisPembayaranController@index')->middleware('isAdmin');
Route::post('master/jenis-pembayaran', 'JenisPembayaranController@index')->middleware('isAdmin');
Route::get('master/jenis-pembayaran/add', 'JenisPembayaranController@create')->middleware('isAdmin');
Route::post('master/jenis-pembayaran/store', 'JenisPembayaranController@store')->middleware('isAdmin');
Route::get('master/jenis-pembayaran/edit/{id}', 'JenisPembayaranController@edit')->middleware('isAdmin');
Route::post('master/jenis-pembayaran/edit/{id}', 'JenisPembayaranController@update')->middleware('isAdmin');
Route::get('master/jenis-pembayaran/delete/{id}', 'JenisPembayaranController@destroy')->middleware('isAdmin');


// START OF MASTER KATEGORI BARANG
Route::get('master/kategori', 'KategoriController@index')->middleware('isAdmin');
Route::post('master/kategori', 'KategoriController@index')->middleware('isAdmin');
Route::get('master/kategori/add', 'KategoriController@create')->middleware('isAdmin');
Route::post('master/kategori/store', 'KategoriController@store')->middleware('isAdmin');
Route::get('master/kategori/edit/{id}', 'KategoriController@edit')->middleware('isAdmin');
Route::post('master/kategori/edit/{id}', 'KategoriController@update')->middleware('isAdmin');
Route::get('master/kategori/delete/{id}', 'KategoriController@destroy')->middleware('isAdmin');

// START OF MASTER BANK
Route::group(['middleware' => ['isAdmin','verified'],'prefix' => 'master/bank'], function()
{
  Route::get('/', 'BankController@index');
  Route::post('/', 'BankController@index');
  Route::get('/add', 'BankController@create');
  Route::post('/store', 'BankController@store');
  Route::get('/edit/{id}', 'BankController@edit');
  Route::post('/edit/{id}', 'BankController@update');
  Route::get('/delete/{id}', 'BankController@destroy');
  Route::get('/restore/{id}', 'BankController@restore');
  Route::get('/force-delete/{id}', 'BankController@forceDelete');
});


// START OF BARANG KONVEN
Route::group(['middleware' => ['isAdmin','verified'],'prefix' => 'master/barang_konven'], function()
{
  Route::get('/', 'BarangKonvenController@index');
  Route::get('/export', 'BarangKonvenController@exportFileMaster');
  Route::get('/downloadTemplateExcel', 'BarangKonvenController@downloadTemplateExcelAdmin');
  Route::post('/import', 'BarangKonvenController@import');
  Route::get('/add', 'BarangKonvenController@create');
  Route::post('/store', 'BarangKonvenController@store');
  Route::get('/edit/{id}', 'BarangKonvenController@edit');
  Route::post('/edit/{id}', 'BarangKonvenController@update');
  Route::get('/delete/{id}', 'BarangKonvenController@delete');
  Route::get('/restore/{id}', 'BarangKonvenController@restore');
  Route::get('/force-delete/{id}', 'BarangKonvenController@forceDelete');
});


Route::get('dashboard_konven', 'DashboardKonvenController@index')->middleware('isPenjual');
Route::get('transaksi_konven', 'DetailTransaksiKonvenController@index')->middleware('isPenjual');
Route::get('transaksi_konven/find','TransaksiKonvenController@index')->middleware('isPenjual');
Route::post('transaksi_konven/update_pos/{id}','TransaksiKonvenController@update')->middleware('isPenjual');
Route::post('transaksi_konven/update_detail_pos/{id}','TransaksiKonvenController@updateDetail')->middleware('isPenjual');
Route::get('transaksi_konven/list','TransaksiKonvenController@find')->middleware('isPenjual');
Route::get('transaksi_konven/backToList','TransaksiKonvenController@backToList')->middleware('isPenjual');
Route::get('transaksi_konven/detail_transaksi_konven/{id}','TransaksiKonvenController@showDetail')->middleware('isPenjual');
Route::get('transaksi_konven/export', 'TransaksiKonvenController@exportFile')->middleware('isPenjual');
Route::get('transaksi_konven/detail/delete/{id}', 'TransaksiKonvenController@hapus')->middleware('isPenjual');
Route::resource('transaksi_konven', 'DetailTransaksiKonvenController')->middleware('isPenjual');
Route::get('barang_konven/list_pos', 'BarangKonvenController@index2')->middleware('isPenjual');
Route::get('barang_konven/tambah_barang_pos', 'BarangKonvenController@create2')->middleware('isPenjual');
Route::get('barang_konven/edit_pos/{id}', 'BarangKonvenController@edit2')->middleware('isPenjual');
Route::post('barang_konven/edit_pos/{id}', 'BarangKonvenController@update2')->middleware('isPenjual');
Route::post('barang_konven/store_pos', 'BarangKonvenController@store2')->middleware('isPenjual');
Route::get('barang_konven/delete/{id}', 'BarangKonvenController@destroy')->middleware('isPenjual');
Route::get('barang_konven/export', 'BarangKonvenController@exportFile')->middleware('isPenjual');
Route::get('barang_konven/downloadTemplateExcelPenjual', 'BarangKonvenController@downloadTemplateExcelPenjual')->middleware('isPenjual');
Route::post('barang_konven/import', 'BarangKonvenController@importFile')->middleware('isPenjual');
Route::get('pembeli_konven/list_pos', 'PembeliController@index2')->middleware('isPenjual');
Route::get ('addItem', 'DetailTransaksiKonvenController@tampilKeranjang')->middleware('isPenjual');
Route::post ('addItem', 'DetailTransaksiKonvenController@addItem')->middleware('isPenjual');
Route::post ('tambahTransaksi', 'DetailTransaksiKonvenController@tambahTransaksi')->middleware('isPenjual');
Route::get('transaksi_konven/{id_produk}/getData', 'DetailTransaksiKonvenController@getData')->middleware('isPenjual');
Route::get('transaksi_konven/{nama}/getDataProduk', 'DetailTransaksiKonvenController@getDataProduk')->middleware('isPenjual');
Route::get('transaksi_konven/{id_pembeli}/getDataPembeli', 'DetailTransaksiKonvenController@getDataPembeli')->middleware('isPenjual');
Route::get('transaksi_konven/{nama_pembeli}/getNamaPembeli', 'DetailTransaksiKonvenController@getNamaPembeli')->middleware('isPenjual');
Route::get('searchajax',array('as'=>'searchajax','uses'=>'DetailTransaksiKonvenController@autoComplete'))->middleware('isPenjual');
Route::post('/autocomplete/fetch', 'DetailTransaksiKonvenController@fetch')->name('autocomplete.fetch')->middleware('isPenjual');
Route::get('qrcode', 'DetailTransaksiKonvenController@generateQrcode')->middleware('isPenjual');
Route::get('invoice/{id}', 'DetailTransaksiKonvenController@getPdf')->middleware('isPenjual');
Route::get('laporan_transaksi_konven/cari', 'TransaksiKonvenController@cari')->middleware('isPenjual');
Route::get('laporan_transaksi_konven', 'TransaksiKonvenController@laporan')->middleware('isPenjual');
Route::get('laporan_transaksi_konven/export', 'TransaksiKonvenController@exportLaporan')->middleware('isPenjual');


//START OF KASIR UTAMA/////
Route::get('dashboard_konven_kasir', 'DashboardKonvenKasirController@index')->middleware('isKasir');
Route::get('transaksi_konven_kasir', 'DetailTransaksiKonvenKasirController@index')->middleware('isKasir');
Route::get('transaksi_konven_kasir/pilih_toko', 'DetailTransaksiKonvenKasirController@pilih_toko')->middleware('isKasir');
Route::post('transaksi_konven_kasir/pilih_toko', 'DetailTransaksiKonvenKasirController@pilih_toko')->middleware('isKasir');
Route::get('transaksi_konven_kasir/find','TransaksiKonvenKasirController@index')->middleware('isKasir');
Route::get('transaksi_konven_kasir/list','TransaksiKonvenKasirController@find')->middleware('isKasir');
Route::get('transaksi_konven_kasir/detail_transaksi_konven/{id}','TransaksiKonvenKasirController@showDetail')->middleware('isKasir');
Route::get('transaksi_konven_kasir/export', 'TransaksiKonvenKasirController@exportFile')->middleware('isKasir');
Route::resource('transaksi_konven_kasir', 'DetailTransaksiKonvenKasirController')->middleware('isKasir');
Route::get('barang_konven_kasir/list_pos', 'BarangKonvenKasirController@index2')->middleware('isKasir');
Route::get('barang_konven_kasir/tambah_barang_pos', 'BarangKonvenKasirController@create2')->middleware('isKasir');
Route::get('barang_konven_kasir/edit_pos/{id}', 'BarangKonvenKasirController@edit2')->middleware('isKasir');
Route::post('barang_konven_kasir/edit_pos/{id}', 'BarangKonvenKasirController@update2')->middleware('isKasir');
Route::post('barang_konven_kasir/store_pos', 'BarangKonvenKasirController@store2')->middleware('isKasir');
Route::get('barang_konven_kasir/delete/{id}', 'BarangKonvenKasirController@destroy')->middleware('isKasir');
Route::get('barang_konven_kasir/export', 'BarangKonvenKasirController@exportFile')->middleware('isKasir');
Route::get('pembeli_konven_kasir/list_pos', 'PembeliController@index2')->middleware('isKasir');
Route::get ('addItem_kasir', 'DetailTransaksiKonvenKasirController@tampilKeranjang')->middleware('isKasir');
Route::post ('addItem_kasir', 'DetailTransaksiKonvenKasirController@addItem')->middleware('isKasir');
Route::post ('tambahTransaksi_kasir', 'DetailTransaksiKonvenKasirController@tambahTransaksi')->middleware('isKasir');
Route::get('transaksi_konven_kasir/{id_produk}/getData', 'DetailTransaksiKonvenKasirController@getData')->middleware('isKasir');
Route::get('transaksi_konven_kasir/{nama}/getDataProduk', 'DetailTransaksiKonvenKasirController@getDataProduk')->middleware('isKasir');
Route::get('transaksi_konven_kasir/{id_pembeli}/getDataPembeli', 'DetailTransaksiKasirKonvenController@getDataPembeli')->middleware('isKasir');
Route::get('transaksi_konven_kasir/{nama_pembeli}/getNamaPembeli', 'DetailTransaksiKonvenKasirController@getNamaPembeli')->middleware('isKasir');
Route::get('searchajax_kasir',array('as'=>'searchajax_kasir','uses'=>'DetailTransaksiKonvenKasirController@autoComplete'))->middleware('isKasir');
Route::post('/autocomplete_kasir/fetch', 'DetailTransaksiKonvenKasirController@fetch')->name('autocomplete_kasir.fetch')->middleware('isKasir');
Route::get('qrcode_kasir', 'DetailTransaksiKonvenKasirController@generateQrcode')->middleware('isKasir');
Route::get('invoice_kasir/{id}', 'DetailTransaksiKonvenKasirController@getPdf')->middleware('isKasir');
